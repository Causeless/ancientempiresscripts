
-- this line needs to be present - due to the way this script file is loaded by the main script it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.script_env);


-------------------------------------------------------
--	Intro cutscene construction
--	This function declares and configures the cutscene 
--	and loads it with actions.
--	Customise it to suit.
-------------------------------------------------------

bool_intro_cutscene_advice_played = false;

function play_intro_cutscene_advice()
	if bool_intro_cutscene_advice_played then
		return;
	end;
	
	bool_intro_cutscene_advice_played = true;
	
	cm:show_advice("ae_advice_intro_flyby_macedon", true)	
end;


function cutscene_intro_construct()

	cutscene_intro = campaign_cutscene:new(
		local_faction .. "_intro",							-- string name for this cutscene
		55,													-- length of cutscene in seconds
		function() start_faction() end						-- end callback
	);


	--cutscene_intro:set_debug();
	cutscene_intro:set_skippable(true, function() cutscene_intro_skipped() end);
	cutscene_intro:set_disable_settlement_labels(false);
	
	
	cutscene_intro:action(function() play_intro_cutscene_advice() end, 1.5);	
	
	cutscene_intro:set_skip_camera(cam_start_x, cam_start_y, cam_start_h, cam_start_r);

	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
				5, 
				{296.8, 279.4, 1.1, cam_start_r}, 
				{296.8, 279.4, 0.7, cam_start_r}
			) 
		end, 
		0

	);

	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_macedon", "att_reg_italia_roma") end, 0);
	
	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
			                9, 									-- Speed of camera 
				{296.8, 279.4, 0.7, cam_start_r}, --pella
				{293.46, 247.39, 0.7, -0.3}		--corinth

			) 
		end, 
		7 	--Time of the camera action in seconds
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				7,
				{293.46, 247.39, 0.7, -0.3},		--corinth
				{289, 251, 0.95, -0.1}	--aet
						
			)
		end,
		22
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				7,
                                                                {289, 251, 0.95, -0.1},             --aet
				{209, 292, 0.95, 0.85}	--rome
						
			)
		end,
		31
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				14,
				{209, 292, 0.95, 0.85},	--rome
				{269, 285, 1, 0.1},	--dyrach
				{296, 279.4, 1.1, cam_start_r}	--pella
						
			)
		end,
		40
	);
end;


function cutscene_intro_skipped()
	play_intro_cutscene_advice()
end;


-------------------------------------------------------
--	Construct and then start the cutscene
-------------------------------------------------------

function cutscene_intro_play()
	cutscene_intro_construct();
	cutscene_intro:start();
end;




