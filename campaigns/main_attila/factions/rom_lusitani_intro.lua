
-- this line needs to be present - due to the way this script file is loaded by the main script it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.script_env);


-------------------------------------------------------
--	Intro cutscene construction
--	This function declares and configures the cutscene 
--	and loads it with actions.
--	Customise it to suit.
-------------------------------------------------------

bool_intro_cutscene_advice_played = false;

function play_intro_cutscene_advice()
	if bool_intro_cutscene_advice_played then
		return;
	end;
	
	bool_intro_cutscene_advice_played = true;
	
	cm:show_advice("ae_advice_intro_flyby_lusitani", true)	
end;


function cutscene_intro_construct()

	cutscene_intro = campaign_cutscene:new(
		local_faction .. "_intro",							-- string name for this cutscene
		43,													-- length of cutscene in seconds
		function() start_faction() end						-- end callback
	);


	--cutscene_intro:set_debug();
	cutscene_intro:set_skippable(true, function() cutscene_intro_skipped() end);
	cutscene_intro:set_disable_settlement_labels(false);
	
	
	cutscene_intro:action(function() play_intro_cutscene_advice() end, 1.5);	
	
	cutscene_intro:set_skip_camera(cam_start_x, cam_start_y, cam_start_h, cam_start_r);

	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
				7, 
				{18, 247.4, 1.1, cam_start_r}, 
				{18, 247.4, 0.7, cam_start_r}
			) 
		end, 
		0

	);

	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_lusitani", "att_reg_baetica_hispalis") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_lusitani", "att_reg_baetica_malaca") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_lusitani", "att_reg_carthaginensis_carthago_nova") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_lusitani", "att_reg_lusitania_pax_augusta") end, 0);

	
	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
			                15, 									-- Speed of camera 
				{18, 247.4, 0.7, cam_start_r}, --olisipo
				{28, 233, 0.6, cam_start_r}, --olisipo
				{65, 220, 0.6, 0.5},		--New Carthage
				{85, 220, 0.6, 0.5}			--New Carthage

			) 
		end, 
		10 	--Time of the camera action in seconds
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				7,
				{85, 220, 0.6, 0.5},			--Hispaniola Settlements
				{18, 247.4, 0.7, cam_start_r} --olisipo
						
			)
		end,
		29
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				5,
				{18, 247.4, 0.7, cam_start_r}, --olisipo
				{18, 247.4, 1, cam_start_r} --olisipo
						
			)
		end,
		37
	);

end;


function cutscene_intro_skipped()
	play_intro_cutscene_advice()
end;


-------------------------------------------------------
--	Construct and then start the cutscene
-------------------------------------------------------

function cutscene_intro_play()
	cutscene_intro_construct();
	cutscene_intro:start();
end;





