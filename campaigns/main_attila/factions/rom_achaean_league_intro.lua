
-- this line needs to be present - due to the way this script file is loaded by the main script it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.script_env);


-------------------------------------------------------
--	Intro cutscene construction
--	This function declares and configures the cutscene 
--	and loads it with actions.
--	Customise it to suit.
-------------------------------------------------------

bool_intro_cutscene_advice_played = false;

function play_intro_cutscene_advice()
	if bool_intro_cutscene_advice_played then
		return;
	end;
	
	bool_intro_cutscene_advice_played = true;
	
	cm:show_advice("ae_advice_intro_flyby_achaean_league", true)	
end;


function cutscene_intro_construct()

	cutscene_intro = campaign_cutscene:new(
		local_faction .. "_intro",							-- string name for this cutscene
		68,													-- length of cutscene in seconds
		function() start_faction() end						-- end callback
	);


	--cutscene_intro:set_debug();
	cutscene_intro:set_skippable(true, function() cutscene_intro_skipped() end);
	cutscene_intro:set_disable_settlement_labels(false);
	
	
	cutscene_intro:action(function() play_intro_cutscene_advice() end, 1.5);	
	
	cutscene_intro:set_skip_camera(cam_start_x, cam_start_y, cam_start_h, cam_start_r);

	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
				5, 
				{293.45, 247.38, 1.1, cam_start_r}, 
				{293.45, 247.38, 0.7, cam_start_r}
			) 
		end, 
		0

	);

	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_achaean_league", "att_reg_italia_roma") end, 0);

	
	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
			                5, 									-- Speed of camera 
				{293.45, 247.38, 0.7, cam_start_r}, --corinth
				{289, 251, 0.95, 0.3}	--aet

			) 
		end, 
		5 	--Time of the camera action in seconds
	);
	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
			                5, 									-- Speed of camera 
				{289, 251, 0.95, 0.3},	--aet
				{289, 251, 0.95, 0.1}	--aet

			) 
		end, 
		10 	--Time of the camera action in seconds
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				9,
				{289, 251, 0.95, 0.1},	--aet
				{296.80, 279.42, 0.7, -0.1} --pella
						
			)
		end,
		15
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				10,
				{296.80, 279.42, 0.7, -0.1}, --pella
				{209, 292, 0.6, 0.55}	--rome
						
			)
		end,
		28
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				12,
				{209, 292, 0.6, 0.55},	--rome
				{270, 252, 0.6, 0.3},	--sea
				{290.45, 240, 0.9, 0.1} --spartan horde
						
			)
		end,
		39
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				5,
				{290.45, 240, 0.9, 0.1}, --spartan horde
				{290.45, 240, 0.9, 0.5} --spartan horde
						
			)
		end,
		51
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				9,
				{290.45, 240, 0.9, 0.5}, --spartan horde
				{293.45, 247.38, 1, cam_start_r} --corinth
						
			)
		end,
		56
	);

end;


function cutscene_intro_skipped()
	play_intro_cutscene_advice()
end;


-------------------------------------------------------
--	Construct and then start the cutscene
-------------------------------------------------------

function cutscene_intro_play()
	cutscene_intro_construct();
	cutscene_intro:start();
end;







