-- this line needs to be present - due to the way this script file is loaded by the main script it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.script_env);


-------------------------------------------------------
--	Intro cutscene construction
--	This function declares and configures the cutscene 
--	and loads it with actions.
--	Customise it to suit.
-------------------------------------------------------

bool_intro_cutscene_advice_played = false;

function play_intro_cutscene_advice()
	if bool_intro_cutscene_advice_played then
		return;
	end;
	
	bool_intro_cutscene_advice_played = true;
	
	cm:show_advice("ae_advice_intro_flyby_carthage", true)	
end;


function cutscene_intro_construct()

	cutscene_intro = campaign_cutscene:new(
		local_faction .. "_intro",	-- string name for this cutscene
		9,							-- length of cutscene in seconds
		start_faction				-- end callback
	);

	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_seleucid", "att_reg_khwarasan_merv") end, 0);

	--cutscene_intro:set_debug();
	cutscene_intro:set_skippable(true, cutscene_skipped);
	cutscene_intro:set_disable_settlement_labels(false);
	
	cutscene_intro:action(play_intro_cutscene_advice, 1.5);	
	
	cutscene_intro:set_skip_camera(cam_start_x, cam_start_y, cam_start_h, cam_start_r);

	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
				9, 
				{170, 203, 0.4, cam_start_r}, 
				{171, 203, 1.1, 1.5}
			) 
		end, 
		0
	);

	cutscene_intro:action(
		cutscene_intro_end, 
		9
	);

end;

function cutscene_skipped()
	-- As esc cancels advice
	bool_intro_cutscene_advice_played = false

	play_intro_cutscene_advice()
	cutscene_intro_end()
end

function cutscene_intro_end()
	if cm:is_multiplayer() == false and m_scripted_zama_enabled then
		cm:lock_ui()
		blockRetreatForNextBattle()
		sendHannibalToAttackScipio()
	end
end

-------------------------------------------------------
--	Construct and then start the cutscene
-------------------------------------------------------

function cutscene_intro_play()
	cutscene_intro_construct();
	cutscene_intro:start();
end;

