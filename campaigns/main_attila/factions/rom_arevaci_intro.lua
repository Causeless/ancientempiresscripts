
-- this line needs to be present - due to the way this script file is loaded by the main script it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.script_env);


-------------------------------------------------------
--	Intro cutscene construction
--	This function declares and configures the cutscene 
--	and loads it with actions.
--	Customise it to suit.
-------------------------------------------------------

bool_intro_cutscene_advice_played = false;

function play_intro_cutscene_advice()
	if bool_intro_cutscene_advice_played then
		return;
	end;
	
	bool_intro_cutscene_advice_played = true;
	
	cm:show_advice("ae_advice_intro_flyby_arevaci", true)	
end;


function cutscene_intro_construct()

	cutscene_intro = campaign_cutscene:new(
		local_faction .. "_intro",							-- string name for this cutscene
		73,													-- length of cutscene in seconds
		function() start_faction() end						-- end callback
	);


	--cutscene_intro:set_debug();
	cutscene_intro:set_skippable(true, function() cutscene_intro_skipped() end);
	cutscene_intro:set_disable_settlement_labels(false);
	
	
	cutscene_intro:action(function() play_intro_cutscene_advice() end, 1.5);	
	
	cutscene_intro:set_skip_camera(cam_start_x, cam_start_y, cam_start_h, cam_start_r);

	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
				7, 
				{95.6, 279.8, 1.1, cam_start_r}, 
				{95.6, 279.8, 0.7, cam_start_r}
			) 
		end, 
		0

	);

	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_baetica_hispalis") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_baetica_malaca") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_carthaginensis_carthago_nova") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_lusitania_pax_augusta") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_carthaginensis_toletum") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_lusitania_emerita_augusta") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_gallaecia_bracara") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_gallaecia_brigantium") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_gallaecia_asturica") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_tarraconensis_pompaelo") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_baetica_corduba") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_baetica_corduba") end, 0);
	cutscene_intro:action(function() cm:make_region_visible_in_shroud("rom_arevaci", "att_reg_lusitania_olisipo") end, 0);

	
	cutscene_intro:action(
		function() 
			cm:scroll_camera_with_direction(
			                15, 									-- Speed of camera 
				{95.6, 279.8, 0.7, cam_start_r}, --tarraconensis
				{115.64, 265.91, 0.6,-0.4}, --tarraco
				{85, 220, 0.6, 0.5},		--New Carthage
				{65, 220, 0.6, 0.5}			--New Carthage

			) 
		end, 
		10 	--Time of the camera action in seconds
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				17,
				{65, 220, 0.6, 0.5},			--Hispaniola Settlements
				{67, 245, 0.7, cam_start_r}, --toletum
				{52, 255.45, 0.7, -0.3}, --emerita
				{30, 280.19, 0.7, 0.2}, --bracara
				{32.75, 307.59, 0.7, 0.2} --brigantium
						
			)
		end,
		29
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				10,
				{32.75, 307.59, 0.7, 0.2}, --brigantium
				{50.8, 294.08, 0.7, 0.2}, --asturica
				{87.57, 296.02, 0.7, 0.1} --pompaelo
						
			)
		end,
		48
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				7,
				{87.57, 296.02, 0.7, 0.1}, --pompaelo
				{95.6, 279.8, 0.7, cam_start_r} --tarraconensis
						
			)
		end,
		60
	);
	
	cutscene_intro:action(
		function()
			cm:scroll_camera_with_direction(
				5,
				{95.6, 279.8, 0.7, cam_start_r}, --tarraconensis
				{95.6, 279.8, 1, cam_start_r} --tarraconensis
						
			)
		end,
		67
	);

end;


function cutscene_intro_skipped()
	play_intro_cutscene_advice()
end;


-------------------------------------------------------
--	Construct and then start the cutscene
-------------------------------------------------------

function cutscene_intro_play()
	cutscene_intro_construct();
	cutscene_intro:start();
end;






