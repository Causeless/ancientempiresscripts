-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--
--    CAMPAIGN SCRIPT
--
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

-- Determine whether scripted zama battle is enabled
m_scripted_zama_enabled = true;

-- Load m_queued for whether we need to show mission messages
m_prelude_message_queued = {}

intro_cinematic_str = false;

function set_intro_cinematic(str)
    intro_cinematic_str = str;
end;

-- play intro movie
cm:register_ui_created_callback(
    function()
        if cm:is_new_game() then
            -- if one of the faction scripts has set an intro movie string, play it
            if is_string(intro_cinematic_str) then
                --output("Playing intro movie, key is [" .. intro_cinematic_str .. "]");
                --cm:register_movies(intro_cinematic_str);
            end;
        end;
    end
);


-- camera extents
--cm:set_default_zoom_limit(1.00, 0.8);

function zero_army_action_points()
    local hannibalStr = char_lookup_str(getHannibalChar())
    local scipioStr = char_lookup_str(getScipioChar())

    -- Scipio army can't move during the first turn
    cm:zero_action_points(scipioStr)

    -- Zero hannibal points too so AI doesn't make any plans
    cm:zero_action_points(hannibalStr)

    output("zeroed hannibal, scipio action points")
end

function init_hannibal_attack()
    cm:add_turn_start_callback_for_faction(
        "CarthageTurnListener", 
        "rom_carthage", 
        function()
            -- We want to allow scipio to retreat if he wants, thus why this is commented out
            --[[cm:add_listener(
                "pre_battle_ui_listener",
                "PanelOpenedCampaign",
                true,
                preBattleScreen,
                false)]]
            --output("speedup active?: " .. cm:speedup_active())
            sendHannibalToAttackScipio()
        end,
        false
    )

    output("hannibal set to attack on carthage turn start")
end

-- if we go into the actual battle scene the campaign is unloaded, so we must also save a value
cm:register_saving_game_callback(
	function(context)
		local util = require "lua_scripts.util"
		util.deepSaveTable(context, m_prelude_message_queued, "m_prelude_message_queued")
	end 
)

cm:register_loading_game_callback(
    function(context)
        local util = require "lua_scripts.util"
		m_prelude_message_queued = util.deepLoadTable(context, "m_prelude_message_queued")
        if m_prelude_message_queued.faction ~= nil and m_prelude_message_queued.mission ~= nil then
			cm:add_listener(
				"post_battle_messages",
				"PanelClosedCampaign",
				true,
				queue_messages,
				true
			)
		end
	end 
)

-- Need to show messages after a delay to the battle finish otherwise they don't automatically pop-up
function queue_messages(context)
	if context.string == "popup_battle_results" then
        cm:remove_listener("post_battle_messages")
        cm:lock_ui()
        scripting.game_interface:add_time_trigger("show_prelude_messages", 5.0)
	end
end

function show_prelude_messages(context)
    output("triggering rome/carthage mission")
    cm:unlock_ui()
    cm:trigger_custom_mission(m_prelude_message_queued.faction, m_prelude_message_queued.mission);
    m_prelude_message_queued = {}
end

local function TimeTrigger(context)
    if context.string == "show_prelude_messages" then
        -- Zero action points only after the turn has ended, so they aren't blanked out during the intro cutscene
        zero_army_action_points()
        show_prelude_messages()
    end
end
scripting.AddEventCallBack("TimeTrigger", TimeTrigger)

-- put stuff that needs to be started regardless of the faction being played here
function start_game_all_factions()
    output("start_game_all_factions() called")

    if cm:is_new_game() and cm:is_multiplayer() == false and m_scripted_zama_enabled then
        -- Rome and Carthage manually set up the battle in their intros
        if cm:get_local_faction() ~= "rom_carthage" and cm:get_local_faction() ~= "rom_rome" then
            -- This forces the battle to occur if you're playing as another faction
            zero_army_action_points()
            init_hannibal_attack()
        end
    end
    
    --[[
    if cm:is_multiplayer() == false then
        -----------------------------
        ------- SINGLE PLAYER -------
        -----------------------------

    else
        -----------------------------
        ------- MULTI PLAYER --------
        -----------------------------
    
    end]]
end;
