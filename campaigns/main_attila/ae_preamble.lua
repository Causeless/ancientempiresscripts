--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--
--    PREAMBLE SCRIPT
--    handles details relating to the battle of zama at the start of the campaign
--
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

local zama_pos_x = 261
local zama_pos_y = 259

function getHannibalChar()
    -- get the closest carthaginian force to zama
    local hannibal_char = get_closest_commander_to_position_from_faction(
        get_faction("rom_carthage"), 
        zama_pos_x, 
        zama_pos_y, 
        true
    )

    return hannibal_char
end

function getScipioChar()
    -- get the closest roman force to zama
    local scipio_char = get_closest_commander_to_position_from_faction(
        get_faction("rom_rome"), 
        zama_pos_x, 
        zama_pos_y, 
        true
    )

    return scipio_char
end

function sendHannibalToAttackScipio()
    local hannibalStr = char_lookup_str(getHannibalChar())
    local scipioStr = char_lookup_str(getScipioChar())

    sendToAttack(hannibalStr, scipioStr)
    output("sending hannibal to attack scipio")
end

function sendScipioToAttackHannibal()
    local hannibalStr = char_lookup_str(getHannibalChar())
    local scipioStr = char_lookup_str(getScipioChar())

    sendToAttack(scipioStr, hannibalStr)
    output("sending scipio to attack hannibal")
end

function preBattleScreen(context)
    if context.string == "popup_pre_battle" then
        cm:remove_listener("pre_battle_ui_listener")
        cm:unlock_ui()
        output("disallowing retreat...")
        ui_state.retreat:lock(true, true)
    end
end

function postBattleScreen(context)
    if context.string == "popup_battle_results" then
        cm:remove_listener("post_battle_ui_listener")
        output("re-allowing retreat...")
        ui_state.retreat:unlock(true, true)
    end
end


function blockRetreatForNextBattle()
    -- setup hiding retreat button for this battle
    output("hiding retreat for next battle")
    cm:add_listener(
        "pre_battle_ui_listener",
        "PanelOpenedCampaign",
        true,
        preBattleScreen,
        true
    )

    -- and allowing it again afterwards
    cm:add_listener(
        "post_battle_ui_listener",
        "PanelOpenedCampaign",
        true,
        postBattleScreen,
        true
    )
end

function moveTo(char_str, log_x, log_y)    
    cm:enable_movement_for_character(char_str)
    cm:force_character_force_into_stance(char_str, "MILITARY_FORCE_ACTIVE_STANCE_TYPE_DEFAULT")
    cm:replenish_action_points(char_str)

    cm:move_to(char_str, log_x, log_y, true)
    
    -- listen for the army running out of movement points
    cm:add_listener(
        "movement_points_exhausted_" .. char_str,
        "MovementPointsExhausted",
        true,
        function()
            output("MovementPointsExhausted event has happened for " .. char_str)
            moveTo(char_str, log_x, log_y)
        end,
        true
    );

end

function finishedAttack(attacker_str)
    output(attacker_str .. " finished attack")
    cm:remove_listener("move_attack_finished_" .. attacker_str)
    cm:remove_listener("movement_points_exhausted_" .. attacker_str)
end

function sendToAttack(attacker_str, target_str)
    -- do the attack
    cm:force_character_force_into_stance(attacker_str, "MILITARY_FORCE_ACTIVE_STANCE_TYPE_DEFAULT")
    cm:replenish_action_points(attacker_str)

    cm:attack(attacker_str, target_str, true)
    
    cm:add_listener(
        "move_attack_finished_" .. attacker_str,
        "PendingBattle",
        true,
        function() cm:zero_action_points(attacker_str); finishedAttack(attacker_str) end,
        false
    )

    -- listen for movement points running out, if they do, restart this
    cm:add_listener(
        "movement_points_exhausted_" .. attacker_str,
        "MovementPointsExhausted",
        true,
        function()
            output("MovementPointsExhausted event has happened for "  .. attacker_str)
            sendToAttack(attacker_str, target_str)
        end,
        false
    )
end

-- as of yet unused
function activateZamaBattlefield()
    output("adding custom battlefield")

    -- launch Zama battle
    cm:add_custom_battlefield(
        "zama",                                     -- string identifier
        0,                                          -- x co-ord
        0,                                          -- y co-ord
        5000,                                       -- radius around position
        false,                                      -- will campaign be dumped
        "",                                         -- loading override
        "campaigns/main_attila/battle_zama.lua",    -- script override
        "",                                         -- entire battle override
        0,                                          -- human alliance when battle override
        false,                                      -- launch battle immediately
        true                                        -- is land battle (only for launch battle immediately)
    )
end