-- Written by niklas

--General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"

function remove_fog()
	scripting.game_interface:show_shroud(false)
end

events.AddEventCallBack("FactionTurnStart", remove_fog)

function avoid_rebellion(context)
	cm:set_public_order_of_province_for_region(context:region():name(), 0)
end

events.AddEventCallBack("RegionTurnStart", avoid_rebellion)

dev.log("ae debug loaded")