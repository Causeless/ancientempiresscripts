--[[
Automatically generated via export from C:/Users/Stelios.Avramidis\DaVE_local\branches/attila/charlemagne/rome2/raw_data/db
Edit manually at your own risk
--]]

module(..., package.seeall)

events = get_events()
require "data.lua_scripts.lib_export_triggers"

-- Trigger declarations



--[[ Faction_Leader ]]--

function Faction_Leader_impl (context)
        return char_is_general(context:character()) and context:character():is_faction_leader() and not (context:character():has_trait("Faction_Leader") or context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_impl(context) then
        effect.trait("Faction_Leader", "agent", 1, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Usurper ]]--

function Faction_Leader_Usurper_impl (context)
        return char_is_general(context:character()) and not context:character():has_trait("Faction_Leader_Usurper") and not (context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Infirm") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous") or context:character():has_trait("Faction_Leader_Weary")) and context:character():has_father() and not context:character():father():has_trait("Faction_Leader")
end 

events.CharacterBecomesFactionLeader[#events.CharacterBecomesFactionLeader+1] =
function (context)
    if Faction_Leader_Usurper_impl(context) then
        effect.trait("Faction_Leader_Usurper", "agent", 8, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir ]]--

function Faction_Heir_impl (context)
        return context:character():has_father() and context:character():father():has_trait("Faction_Leader") and context:character():is_male() and not (context:character():has_trait("Faction_Heir") or context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_impl(context) then
        effect.trait("Faction_Heir", "agent", 1, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Beloved ]]--

function Faction_Heir_Beloved_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():has_trait("African_Hero") or context:character():has_trait("Arab_Hero") or context:character():has_trait("Carthaginian_Hero") or context:character():has_trait("Eastern_Hero") or context:character():has_trait("Hellenic_Hero") or context:character():has_trait("Roman_Hero")) and (context:character():has_trait("Victor") or context:character():has_trait("Victor_Others") or context:character():has_trait("Charismatic") or context:character():has_trait("Civic_Crown") or context:character():has_trait("Grass_Crown") or context:character():has_trait("Courageous") or context:character():has_trait("Generous") or context:character():has_trait("Handsome") or context:character():has_trait("Noble_Patron") or context:character():has_trait("Optimist") or context:character():has_trait("Philanthropist") or context:character():has_trait("Witty"))) and not (context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():culture() ~= "rom_Barbarian"
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Beloved_impl(context) then
        effect.trait("Faction_Heir_Beloved", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Craves_for_Power ]]--

function Faction_Heir_Craves_for_Power_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():has_trait("Envious") and (context:character():has_trait("Changeable") or context:character():has_trait("Corrupt") or context:character():has_trait("Devious") or context:character():has_trait("Disloyal") or context:character():has_trait("Secretive"))) or (context:character():has_trait("Changeable") and (context:character():has_trait("Corrupt") or context:character():has_trait("Devious") or context:character():has_trait("Disloyal") or context:character():has_trait("Secretive"))) or (context:character():has_trait("Corrupt") and (context:character():has_trait("Devious") or context:character():has_trait("Disloyal") or context:character():has_trait("Secretive"))) or (context:character():has_trait("Devious") and (context:character():has_trait("Disloyal") or context:character():has_trait("Secretive"))) or (context:character():has_trait("Disloyal") and context:character():has_trait("Secretive")) or context:character():trait_level("Disloyal") == 3) and not (context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():culture() ~= "rom_Barbarian"
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Craves_for_Power_impl(context) then
        effect.trait("Faction_Heir_Beloved", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Despised ]]--

function Faction_Heir_Despised_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():has_trait("Brutal") and (context:character():has_trait("Coward") or context:character():has_trait("Harsh") or context:character():has_trait("Ignorant") or context:character():has_trait("Unjust"))) or (context:character():has_trait("Coward") and (context:character():has_trait("Harsh") or context:character():has_trait("Ignorant") or context:character():has_trait("Unjust"))) or (context:character():has_trait("Harsh") and (context:character():has_trait("Ignorant") or context:character():has_trait("Unjust"))) or (context:character():has_trait("Ignorant") and context:character():has_trait("Unjust"))) and not (context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Despised_impl(context) then
        effect.trait("Faction_Heir_Despised", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Feckless ]]--

function Faction_Heir_Feckless_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():has_trait("Retiring") and (context:character():has_trait("Passive") or context:character():has_trait("Socially_Awkward") or context:character():has_trait("Slothful"))) or (context:character():has_trait("Passive") and (context:character():has_trait("Socially_Awkward") or context:character():has_trait("Slothful"))) or (context:character():trait_level("Passive") == 2) or (context:character():trait_level("Retiring") == 3)) and not (context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Feckless_impl(context) then
        effect.trait("Faction_Heir_Feckless", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Halfwit ]]--

function Faction_Heir_Halfwit_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():has_trait("Ignorant") and (context:character():has_trait("Eccentric") or context:character():has_trait("Insane") or context:character():has_trait("Inconsistent") or context:character():trait_level("Unmotivated") >= 2)) or (context:character():trait_level("Eccentric") == 2 and (context:character():has_trait("Insane") or context:character():has_trait("Inconsistent") or context:character():trait_level("Unmotivated") >= 2)) or (context:character():has_trait("Insane") and (context:character():has_trait("Inconsistent") or context:character():trait_level("Unmotivated") >=2)) or (context:character():trait_level("Inconsistent") >= 2 and (context:character():trait_level("Unmotivated") >= 2)) or (context:character():trait_level("Insane") >= 3)) and not (context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Halfwit")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Halfwit_impl(context) then
        effect.trait("Faction_Heir_Halfwit", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Popular ]]--

function Faction_Heir_Popular_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():trait_level("Analytical") >= 2 and (context:character():has_trait("Austere") or context:character():has_trait("Prudent") or context:character():has_trait("Courageous") or context:character():has_trait("Friendly") or context:character():has_trait("Generous") or context:character():has_trait("Humble") or context:character():has_trait("Just") or context:character():has_trait("Kind") or context:character():has_trait("Victor") or context:character():has_trait("Victor_Others"))) or ((context:character():has_trait("Austere") or context:character():has_trait("Prudent"))  and (context:character():has_trait("Courageous") or context:character():trait_level("Friendly") >= 2 or context:character():has_trait("Generous") or context:character():has_trait("Humble") or context:character():has_trait("Just") or context:character():has_trait("Kind") or context:character():has_trait("Victor") or context:character():has_trait("Victor_Others"))) or (context:character():has_trait("Courageous") and (context:character():has_trait("Friendly") or context:character():has_trait("Generous") or context:character():has_trait("Humble") or context:character():has_trait("Just") or context:character():has_trait("Kind") or context:character():has_trait("Victor") or context:character():has_trait("Victor_Others"))) or (context:character():trait_level("Friendly") >= 2 and (context:character():has_trait("Generous") or context:character():has_trait("Humble") or context:character():has_trait("Just") or context:character():has_trait("Kind") or context:character():has_trait("Victor") or context:character():has_trait("Victor_Others"))) or (context:character():has_trait("Generous") and (context:character():has_trait("Humble") or context:character():has_trait("Just") or context:character():has_trait("Kind") or context:character():has_trait("Victor") or context:character():has_trait("Victor_Others"))) or (context:character():has_trait("Humble") and (context:character():has_trait("Just") or context:character():has_trait("Kind") or context:character():has_trait("Victor") or context:character():has_trait("Victor_Others"))) or (context:character():has_trait("Just") and context:character():has_trait("Kind")) or (context:character():has_trait("Just") and (context:character():has_trait("Victor") or context:character():has_trait("Victor_Others")))) and not (context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Popular_impl(context) then
        effect.trait("Faction_Heir_Popular", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Unfit_to_Rule ]]--

function Faction_Heir_Unfit_to_Rule_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():has_trait("Drinker") or context:character():has_trait("Womanizer") and (context:character():has_trait("Discriminatory") or context:character():has_trait("Coward") or context:character():has_trait("Dark_Humor"))) or context:character():trait_level("Drinker") >= 5) and not (context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Unfit_to_Rule_impl(context) then
        effect.trait("Faction_Heir_Unfit_to_Rule", "agent", 2, 1, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Usurped_Content ]]--

function Faction_Heir_Usurped_Content_impl (context)
        return char_is_general(context:character()) and (context:character():has_trait("Faction_Heir") or context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():faction_leader():has_trait("Faction_Leader_Usurper")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Usurped_Content_impl(context) then
        effect.trait("Usurped_Content_Heir", "agent", 2, 20, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Usurped_Discontent ]]--

function Faction_Heir_Usurped_Discontent_impl (context)
        return char_is_general(context:character()) and (context:character():has_trait("Faction_Heir") or context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():faction_leader():has_trait("Faction_Leader_Usurper")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Usurped_Discontent_impl(context) then
        effect.trait("Usurped_Discontent_Heir", "agent", 2, 40, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Usurped_Rebellious ]]--

function Faction_Heir_Usurped_Rebellious_impl (context)
        return char_is_general(context:character()) and (context:character():has_trait("Faction_Heir") or context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():faction_leader():has_trait("Faction_Leader_Usurper")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Usurped_Rebellious_impl(context) then
        effect.trait("Usurped_Rebellious_Heir", "agent", 2, 25, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Usurped_Supportive ]]--

function Faction_Heir_Usurped_Supportive_impl (context)
        return char_is_general(context:character()) and (context:character():has_trait("Faction_Heir") or context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():faction_leader():has_trait("Faction_Leader_Usurper")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Usurped_Supportive_impl(context) then
        effect.trait("Usurped_Supportive_Heir", "agent", 2, 15, context)
        return true
    end
    return false
end

--[[ Faction_Heir_Worthy_Successor ]]--

function Faction_Heir_Worthy_Successor_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Heir") and ((context:character():has_trait("Upright") and (context:character():has_trait("Stoic") or context:character():has_trait("Sober") or context:character():has_trait("Prudent") or context:character():has_trait("Austere") or context:character():has_trait("Pragmatic") or context:character():has_trait("Political_Talent") or context:character():has_trait("Pious"))) or (context:character():has_trait("Stoic") and (context:character():has_trait("Prudent") or context:character():has_trait("Austere") or context:character():has_trait("Pious")))) and not (context:character():has_trait("Faction_Heir_Beloved") or context:character():has_trait("Faction_Heir_Craves_for_Power") or context:character():has_trait("Faction_Heir_Despised") or context:character():has_trait("Faction_Heir_Feckless") or context:character():has_trait("Faction_Heir_Halfwit") or context:character():has_trait("Faction_Heir_Popular") or context:character():has_trait("Faction_Heir_Worthy_Successor") or context:character():has_trait("Faction_Heir_Unfit_to_Rule")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Heir_Worthy_Successor_impl(context) then
        effect.trait("Faction_Heir_Worthy_Successor", "agent", 2, 1, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Bloodless]]--

function Faction_Leader_Bloodless_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Diplomatic") and (context:character():has_trait("Affectionate") or context:character():has_trait("Non_Authoritarian") or context:character():has_trait("Friendly") or context:character():has_trait("Open_Minded"))) or (context:character():has_trait("Affectionate") and (context:character():has_trait("Non_Authoritarian") or context:character():has_trait("Friendly") or context:character():has_trait("Open_Minded"))) or (context:character():has_trait("Non_Authoritarian") and (context:character():has_trait("Friendly") or context:character():has_trait("Open_Minded"))) or (context:character():has_trait("Friendly") and context:character():has_trait("Open_Minded"))) and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Bloodless_impl(context) then
        effect.trait("Faction_Leader_Bloodless", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Bloodless2 ]]--

function Faction_Leader_Bloodless2_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and context:character():faction():num_allies() >= 3 and not context:character():faction():at_war() and context:character():model():turn_number() > 10 and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Faction_Leader_Bloodless2_impl(context) then
		effect.trait("Faction_Leader_Bloodless", "agent", 2, 4, context)
		return true
	end
	return false
end

--[[ Faction_Leader_Bloodless3 ]]--

function Faction_Leader_Bloodless3_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and not context:character():faction():at_war() and context:character():model():turn_number() > 30 and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Faction_Leader_Bloodless3_impl(context) then
		effect.trait("Faction_Leader_Bloodless", "agent", 2, 15, context)
		return true
	end
	return false
end

--[[ Faction_Leader_Conqueror]]--

function Faction_Leader_Conqueror_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and (((context:character():has_trait("Victor") or context:character():has_trait("Natural_Military_Skill")) and (context:character():has_trait("African_Hero") or context:character():has_trait("Arab_Hero") or context:character():has_trait("Carthaginian_Hero") or context:character():has_trait("Eastern_Hero") or context:character():has_trait("Hellenic_Hero") or context:character():has_trait("Roman_Hero"))) or (context:character():trait_level("Victor") == 3 and (context:character():has_trait("African_Hero") or context:character():has_trait("Arab_Hero") or context:character():has_trait("Carthaginian_Hero") or context:character():has_trait("Eastern_Hero") or context:character():has_trait("Hellenic_Hero") or context:character():has_trait("Roman_Hero")))) and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Conqueror_impl(context) then
        effect.trait("Faction_Leader_Conqueror", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Feckless]]--

function Faction_Leader_Feckless_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Passive") and (context:character():has_trait("Retiring") or context:character():trait_level("Socially_Awkward") >= 3 or context:character():trait_level("Unamusing") >= 2)) or (context:character():has_trait("Retiring") and (context:character():has_trait("Socially_Awkward") or context:character():has_trait("Unamusing")))) and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Feckless_impl(context) then
        effect.trait("Faction_Leader_Feckless", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Feckless2 ]]--

function Faction_Leader_Feckless2_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and context:character():faction():tax_level() < 20 and not context:character():faction():at_war() and context:character():model():turn_number() > 30 and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Faction_Leader_Feckless2_impl(context) then
		effect.trait("Faction_Leader_Feckless", "agent", 2, 15, context)
		return true
	end
	return false
end

--[[ Faction_Leader_Glorious]]--

function Faction_Leader_Glorious_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and (((context:character():has_trait("Rash") or context:character():trait_level("Courageous") >= 3) and (context:character():trait_level("Upright") >= 3 or context:character():has_trait("African_Hero") or context:character():has_trait("Arab_Hero") or context:character():has_trait("Carthaginian_Hero") or context:character():has_trait("Eastern_Hero") or context:character():has_trait("Hellenic_Hero")))) and (context:character():faction():culture() == "rom_Hellenistic" or context:character():faction():culture() == "rom_Eastern") and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous"))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Glorious_impl(context) then
        effect.trait("Faction_Leader_Glorious", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Gorgeous]]--

function Faction_Leader_Gorgeous_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and (context:character():has_trait("Handsome") and ((context:character():has_trait("Energetic") or context:character():has_trait("Fertile") or context:character():has_trait("Healthy") or context:character():has_trait("Womanizer")) or context:character():trait_level("Handsome") >= 3)) and not context:character():has_trait("Leper") and context:character():faction():culture() == "rom_Eastern" and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous"))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Gorgeous_impl(context) then
        effect.trait("Faction_Leader_Gorgeous", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Immoral]]--

function Faction_Leader_Immoral_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():trait_level("Gambler") >= 2 and (context:character():trait_level("Skeptic") >= 4 or context:character():trait_level("Drinker") >= 3 or context:character():trait_level("Eye_for_Women") >= 2 or context:character():has_trait("Persecutor") or context:character():trait_level("Womanizer") >= 3 or context:character():trait_level("Foulmouthed") >= 3)) or (context:character():trait_level("Skeptic") >= 2 and (context:character():has_trait("Drinker") or context:character():trait_level("Eye_for_Women") >= 2 or context:character():has_trait("Persecutor") or context:character():trait_level("Womanizer") >= 3)) or (context:character():trait_level("Drinker") == 5) or (context:character():trait_level("Drinker") >= 3 and (context:character():trait_level("Eye_for_Women") >= 2 or context:character():trait_level("Womanizer") >= 2)) or (context:character():trait_level("Drinker") >= 3 and context:character():trait_level("Gambler") >= 3)) and not (context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Immoral_impl(context) then
        effect.trait("Faction_Leader_Immoral", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Infirm]]--

function Faction_Leader_Infirm_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Gout") and (context:character():has_trait("Leper") or context:character():trait_level("Ill") >= 2)) or (context:character():trait_level("Ill") >= 3) or (context:character():has_trait("Leper") and context:character():trait_level("Ill") >= 1) or context:character():trait_level("Scarred") >= 3) and not (context:character():has_trait("Faction_Leader_Infirm")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Infirm_impl(context) then
        effect.trait("Faction_Leader_Infirm", "agent", 10, 50, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Insane]]--

function Faction_Leader_Insane_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():trait_level("Insane") >= 3 and (context:character():trait_level("Inconsistent") >= 2 or context:character():trait_level("Ignorant") >= 3 or context:character():trait_level("Eccentric") == 2)) or (context:character():trait_level("Insane") >= 4)) and not (context:character():has_trait("Faction_Leader_Insane")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Insane_impl(context) then
        effect.trait("Faction_Leader_Insane", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Noble]]--

function Faction_Leader_Noble_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and (((context:character():has_trait("Just") or context:character():has_trait("Charismatic")) and (context:character():has_trait("Noble_Patron") or context:character():has_trait("Philantrophist") or context:character():has_trait("Populares"))) or (context:character():trait_level("Just") >= 2 and context:character():has_trait("Charismatic"))) and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Noble_impl(context) then
        effect.trait("Faction_Leader_Noble", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Noble2 ]]--

function Faction_Leader_Noble2_impl (context)
		return context:character():is_faction_leader() and context:character():faction():tax_level() < 20 and context:character():model():turn_number() > 5 and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Faction_Leader_Noble2_impl(context) then
		effect.trait("Faction_Leader_Noble", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Faction_Leader_One_eyed]]--

function Faction_Leader_One_eyed_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and (context:character():has_trait("Scarred") and (context:character():has_trait("Good_Attacker") or context:character():has_trait("African_Hero") or context:character():has_trait("Arab_Hero") or context:character():has_trait("Carthaginian_Hero") or context:character():has_trait("Eastern_Hero") or context:character():has_trait("Hellenic_Hero") or context:character():has_trait("Roman_Hero") or context:character():has_trait("Victor") or context:character():has_trait("Victor_Others"))) and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_One_eyed_impl(context) then
        effect.trait("Faction_Leader_One_eyed", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Paranoid]]--

function Faction_Leader_Paranoid_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Good_Personal_Security") and (context:character():has_trait("Inconsistent") or context:character():has_trait("Drinker") or context:character():has_trait("Close_Minded") or context:character():has_trait("Secretive"))) or (context:character():trait_level("Good_Personal_Security") >= 3)) and not (context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Treacherous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Paranoid_impl(context) then
        effect.trait("Faction_Leader_Paranoid", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Paranoid2 ]]--

function Faction_Leader_Paranoid2_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and not (context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Treacherous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterFamilyRelationDied[#events.CharacterFamilyRelationDied+1] =
function (context)
	if Faction_Leader_Paranoid2_impl(context) then
		effect.trait("Faction_Leader_Paranoid", "agent", 2, 6, context)
		return true
	end
	return false
end

--[[ Faction_Leader_Pompous]]--

function Faction_Leader_Pompous_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Epicurean") and (context:character():has_trait("Proud") or context:character():has_trait("Narcissist") or context:character():has_trait("Envious") or context:character():has_trait("Possessive") or context:character():has_trait("Optimates") or context:character():has_trait("Superficial") or context:character():trait_level("Gourmand") >= 3 or context:character():trait_level("Generous") >= 3)) or (context:character():has_trait("Narcissist") and (context:character():has_trait("Proud") or context:character():has_trait("Envious") or context:character():has_trait("Possessive") or context:character():has_trait("Optimates") or context:character():has_trait("Superficial") or context:character():trait_level("Gourmand") >= 3 or context:character():trait_level("Generous") >= 3)) or (context:character():has_trait("Superficial") and (context:character():has_trait("Proud") or context:character():has_trait("Possessive") or context:character():has_trait("Optimates"))) or (context:character():has_trait("Playwright") and (context:character():has_trait("Narcissist") or context:character():has_trait("Superficial") or context:character():trait_level("Possessive") >= 3)) or context:character():trait_level("Narcissist") == 3) and not (context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Pompous_impl(context) then
        effect.trait("Faction_Leader_Pompous", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Pompous2 ]]--

function Faction_Leader_Pompous2_impl (context)
		return context:character():is_faction_leader() and context:character():faction():losing_money() and context:character():faction():is_human() and context:character():model():turn_number() > 5 and (context:character():has_trait("Epicurean") or context:character():has_trait("Narcissist") or context:character():has_trait("Envious") or context:character():has_trait("Possessive") or context:character():has_trait("Superficial") or context:character():trait_level("Gourmand") >= 3)
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Faction_Leader_Pompous2_impl(context) then
		effect.trait("Faction_Leader_Pompous", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Faction_Leader_Praiseworthy]]--

function Faction_Leader_Praiseworthy_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():trait_level("Roman_Hero") >= 3 and (context:character():has_trait("Philantrophist") or context:character():has_trait("Humble") or context:character():has_trait("Witty") or context:character():trait_level("Prudent") == 3 or context:character():trait_level("Upright") == 3)) or (context:character():has_trait("Civic_Crown") and (context:character():has_trait("Philantrophist") or context:character():has_trait("Humble") or context:character():has_trait("Witty") or context:character():trait_level("Prudent") == 3 or context:character():trait_level("Upright") == 3)) or (context:character():has_trait("Grass_Crown") and (context:character():has_trait("Philantrophist") or context:character():has_trait("Humble") or context:character():has_trait("Witty") or context:character():trait_level("Prudent") == 3 or context:character():trait_level("Upright") == 3)) or (context:character():trait_level("Roman_Hero") >= 5)) and context:character():faction():culture() == "rom_Roman" and not (context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous"))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Praiseworthy_impl(context) then
        effect.trait("Faction_Leader_Praiseworthy", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Stoic]]--

function Faction_Leader_Stoic_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():trait_level("Stoic") >= 3 and (context:character():has_trait("Unhurried") or context:character():trait_level("Sober") == 2 or context:character():has_trait("Patient") or context:character():trait_level("Philosopher") >= 2 or context:character():has_trait("Pragmatic"))) or (context:character():trait_level("Sober") == 2 and (context:character():has_trait("Philosopher") or context:character():has_trait("Pragmatic") or context:character():has_trait("Patient"))) or (context:character():has_trait("Pragmatic") and (context:character():has_trait("Philosopher") or context:character():trait_level("Patient") == 2))) and (context:character():faction():culture() == "rom_Hellenistic" or context:character():faction():culture() == "rom_Roman" or context:character():faction():subculture() == "sc_rom_eastern" or context:character():faction():subculture() == "sc_rom_armenian" or context:character():faction():subculture() == "sc_rom_parthian") and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous"))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Stoic_impl(context) then
        effect.trait("Faction_Leader_Stoic", "agent", 2, 100, context)
        return true
    end
    return false
end


--[[ Faction_Leader_Tolerant]]--

function Faction_Leader_Tolerant_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Kind") and (context:character():has_trait("Non_Authoritarian") or context:character():has_trait("Open_Minded") or context:character():has_trait("Affectionate") or context:character():has_trait("Generous") or context:character():has_trait("Philanthrophist") or context:character():has_trait("Optimist"))) or (context:character():has_trait("Non_Authoritarian") and (context:character():has_trait("Open_Minded") or context:character():has_trait("Affectionate") or context:character():has_trait("Generous") or context:character():has_trait("Philanthrophist") or context:character():has_trait("Optimist"))) or (context:character():has_trait("Open_Minded") and (context:character():has_trait("Affectionate") or context:character():has_trait("Generous") or context:character():has_trait("Philanthrophist"))) or (context:character():trait_level("Generous") >= 2 and context:character():has_trait("Philanthrophist")) or (context:character():trait_level("Philanthropist") >= 3)) and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Tolerant_impl(context) then
        effect.trait("Faction_Leader_Tolerant", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Treacherous]]--

function Faction_Leader_Treacherous_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Brutal") and (context:character():has_trait("Devious") or context:character():has_trait("Corrupt") or context:character():has_trait("Liar") or context:character():has_trait("Political_Schemer") or context:character():has_trait("Vengeful"))) or (context:character():has_trait("Devious") and (context:character():has_trait("Corrupt") or context:character():has_trait("Liar") or context:character():has_trait("Political_Schemer") or context:character():has_trait("Vengeful"))) or (context:character():has_trait("Corrupt") and (context:character():has_trait("Liar") or context:character():has_trait("Political_Schemer")) or (context:character():trait_level("Liar") == 4) or (context:character():trait_level("Devious") == 3))) and not (context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Treacherous_impl(context) then
        effect.trait("Faction_Leader_Treacherous", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Tyrannous]]--

function Faction_Leader_Tyrannous_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and ((context:character():has_trait("Authoritarian") and (context:character():has_trait("Harsh") or context:character():trait_level("Sober") == 2 or context:character():trait_level("Political_Skill") == 5 or context:character():trait_level("Unjust") >= 2 or context:character():has_trait("Prudent") or context:character():has_trait("Austere") or context:character():has_trait("Predominant"))) or (context:character():has_trait("Harsh") and (context:character():trait_level("Unjust") >= 2 or context:character():trait_level("Political_Skill") == 5 or context:character():has_trait("Predominant"))) or (context:character():trait_level("Authoritarian") == 3)) and not (context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Tyrannous_impl(context) then
        effect.trait("Faction_Leader_Tyrannous", "agent", 2, 100, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Weary1]]--

function Faction_Leader_Weary1_impl (context)
        return char_is_general(context:character()) and (context:character():has_trait("Faction_Leader") or context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():age() > 60 and not context:character():has_trait("Faction_Leader_Infirm") and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Weary1_impl(context) then
        effect.trait("Faction_Leader_Weary", "agent", 2, 2, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Weary2]]--

function Faction_Leader_Weary2_impl (context)
        return char_is_general(context:character()) and (context:character():has_trait("Irritable") or context:character():has_trait("Depressed") or context:character():trait_level("Passive") == 2) and (context:character():has_trait("Faction_Leader") or context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():age() > 55 and not context:character():has_trait("Faction_Leader_Infirm") and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Weary2_impl(context) then
        effect.trait("Faction_Leader_Weary", "agent", 2, 10, context)
        return true
    end
    return false
end

--[[ Faction_Leader_Virtuous]]--

function Faction_Leader_Virtuous_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Faction_Leader") and (context:character():has_trait("Pious") and (context:character():has_trait("Religious_Mania") or context:character():has_trait("Austere") or context:character():has_trait("Philanthropist") or context:character():has_trait("Prudent") or context:character():has_trait("Upright")) or (context:character():trait_level("Pious") == 3 and (context:character():has_trait("Humble")))) and not (context:character():has_trait("Faction_Leader_Conqueror") or context:character():has_trait("Faction_Leader_Noble") or context:character():has_trait("Faction_Leader_Feckless") or context:character():has_trait("Faction_Leader_Glorious") or context:character():has_trait("Faction_Leader_Gorgeous") or context:character():has_trait("Faction_Leader_Immoral") or context:character():has_trait("Faction_Leader_Insane") or context:character():has_trait("Faction_Leader_One_eyed") or context:character():has_trait("Faction_Leader_Paranoid") or context:character():has_trait("Faction_Leader_Bloodless") or context:character():has_trait("Faction_Leader_Pompous") or context:character():has_trait("Faction_Leader_Praiseworthy") or context:character():has_trait("Faction_Leader_Stoic") or context:character():has_trait("Faction_Leader_Tolerant") or context:character():has_trait("Faction_Leader_Treacherous") or context:character():has_trait("Faction_Leader_Tyrannous") or context:character():has_trait("Faction_Leader_Virtuous")) and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Faction_Leader_Virtuous_impl(context) then
        effect.trait("Faction_Leader_Virtuous", "agent", 2, 100, context)
        return true
    end
    return false
end


--[[ Tribal_Chief]]--

function Tribal_Chief_impl (context)
        return context:character():is_faction_leader() and context:character():faction():culture() == "rom_Barbarian" and (context:character():faction():culture() == "rom_Barbarian" and not context:character():has_trait("Chieftain") or context:character():has_trait("Chieftain_Infirm") or context:character():has_trait("Chieftain_Mighty") or context:character():has_trait("Chieftain_Savage") or context:character():has_trait("Chieftain_Tolerant") or context:character():has_trait("Chieftain_Tyrannic") or context:character():has_trait("Chieftain_Warlike") or context:character():has_trait("Chieftain_Weak"))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Tribal_Chief_impl(context) then
        effect.trait("Chieftain", "agent", 1, 100, context)
        return true
    end
    return false
end

--[[ Chieftain_Infirm]]--

function Chieftain_Infirm_impl (context)
        return context:character():is_faction_leader() and context:character():has_trait("Chieftain") and ((context:character():has_trait("Gout") and (context:character():has_trait("Leper") or context:character():trait_level("Ill") >= 1)) or (context:character():trait_level("Ill") >= 3) or (context:character():has_trait("Leper") and context:character():trait_level("Ill") >= 1))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Chieftain_Infirm_impl(context) then
        effect.trait("Chieftain_Infirm", "agent", 2, 30, context)
        return true
    end
    return false
end

--[[ Chieftain_Mighty]]--

function Chieftain_Mighty_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Tribal_Leader")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Chieftain_Mighty_impl(context) then
        effect.trait("Chieftain_Mighty", "agent", 2, 65, context)
        return true
    end
    return false
end

--[[ FirstInitialTraits ]]--

function  FirstInitialTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  FirstInitialTraits_impl(context) then
		effect.trait("Aesthetic", "agent", 1,1, context)
		effect.trait("Analytical", "agent", 1,2, context)
		effect.trait("Approachable", "agent", 1,1, context)
		effect.trait("Austere", "agent", 1,2, context)
		effect.trait("Cautious", "agent", 1,2, context)
		effect.trait("Changeable", "agent", 1,1, context)
		effect.trait("Cheeky", "agent", 1,2, context)
		effect.trait("Complimenter", "agent", 1,1, context)
		effect.trait("Confrontational", "agent", 1,2, context)
		effect.trait("Courageous", "agent", 1,3, context)
		effect.trait("Dark_Humor", "agent", 1,3, context)
		effect.trait("Decisive", "agent", 1,2, context)
		effect.trait("Disorderly", "agent", 1,2, context)
		effect.trait("Drinker", "agent", 1,3, context)
		effect.trait("Energetic", "agent", 1,2, context)
		effect.trait("Envious", "agent", 1, 1, context)
		effect.trait("Eye_for_Women", "agent", 1,3, context)
		effect.trait("Friendly", "agent", 1,2, context)
		effect.trait("Gout", "agent", 1,1, context)
		effect.trait("Harsh", "agent", 1,3, context)
		effect.trait("Ill", "agent", 1,1, context)	
		effect.trait("Impassioned", "agent", 1,2, context)
		effect.trait("Impatient", "agent", 1,2, context)	
		effect.trait("Indecisive", "agent", 1,1, context)
		effect.trait("Insane", "agent", 1,3, context)	
		effect.trait("Kind", "agent", 1,2, context)	
		effect.trait("Leper", "agent", 1,2, context)
		effect.trait("Liar", "agent", 1,1, context)
		effect.trait("Motivated", "agent", 1,2, context)
		effect.trait("Nagger", "agent", 1,2, context)	
		effect.trait("Narcicisst", "agent", 1,2, context)	
		effect.trait("Optimist", "agent", 1,2, context)	
		effect.trait("Patient", "agent", 1,2, context)
		effect.trait("Persuasive", "agent", 1,2, context)
		effect.trait("Pious", "agent", 1,2, context)
		effect.trait("Pragmatic", "agent", 1,2, context)
		effect.trait("Proud", "agent", 1,2, context)
		effect.trait("Rational", "agent", 1,2, context)
		effect.trait("Self_Deprecating_Humor", "agent", 1,2, context)
		effect.trait("Sharp", "agent", 1,2, context)		
		effect.trait("Skeptic", "agent", 1,1, context)
		effect.trait("Straightforward", "agent", 1,2, context)
		effect.trait("Unfriendly", "agent", 1,2, context)
		effect.trait("Unmotivated", "agent", 1,1, context)
		effect.trait("Upright", "agent", 1,2, context)
		effect.trait("Harsh", "agent", 1,2, context)
		effect.trait("Witty", "agent", 1, 2, context)
		effect.trait("Womanizer", "agent", 1,3, context)
	end
	return false
end

--[[ SecondInitialTraits ]]--

function  SecondInitialTraits_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  SecondInitialTraits_impl(context) then
		effect.trait("Aesthetic", "agent", 1,1, context)
		effect.trait("Analytical", "agent", 1,2, context)
		effect.trait("Approachable", "agent", 1,1, context)
		effect.trait("Austere", "agent", 1,2, context)
		effect.trait("Cautious", "agent", 1,2, context)
		effect.trait("Changeable", "agent", 1,1, context)
		effect.trait("Cheeky", "agent", 1,2, context)
		effect.trait("Complimenter", "agent", 1,1, context)
		effect.trait("Confrontational", "agent", 1,2, context)
		effect.trait("Courageous", "agent", 1,3, context)
		effect.trait("Dark_Humor", "agent", 1,3, context)
		effect.trait("Decisive", "agent", 1,2, context)
		effect.trait("Disorderly", "agent", 1,2, context)
		effect.trait("Drinker", "agent", 1,3, context)
		effect.trait("Energetic", "agent", 1,2, context)
		effect.trait("Envious", "agent", 1, 1, context)
		effect.trait("Eye_for_Women", "agent", 1,3, context)
		effect.trait("Friendly", "agent", 1,2, context)
		effect.trait("Gout", "agent", 1,1, context)
		effect.trait("Harsh", "agent", 1,3, context)
		effect.trait("Ill", "agent", 1,1, context)	
		effect.trait("Impassioned", "agent", 1,2, context)
		effect.trait("Impatient", "agent", 1,2, context)	
		effect.trait("Indecisive", "agent", 1,1, context)
		effect.trait("Insane", "agent", 1,3, context)	
		effect.trait("Kind", "agent", 1,2, context)	
		effect.trait("Leper", "agent", 1,2, context)
		effect.trait("Liar", "agent", 1,1, context)
		effect.trait("Motivated", "agent", 1,2, context)
		effect.trait("Nagger", "agent", 1,2, context)	
		effect.trait("Narcicisst", "agent", 1,2, context)	
		effect.trait("Optimist", "agent", 1,2, context)	
		effect.trait("Patient", "agent", 1,2, context)
		effect.trait("Persuasive", "agent", 1,2, context)
		effect.trait("Pious", "agent", 1,2, context)
		effect.trait("Pragmatic", "agent", 1,2, context)
		effect.trait("Rational", "agent", 1,2, context)
		effect.trait("Self_Deprecating_Humor", "agent", 1,2, context)
		effect.trait("Sharp", "agent", 1,2, context)		
		effect.trait("Skeptic", "agent", 1,1, context)
		effect.trait("Straightforward", "agent", 1,2, context)
		effect.trait("Unfriendly", "agent", 1,2, context)
		effect.trait("Unmotivated", "agent", 1,1, context)
		effect.trait("Upright", "agent", 1,2, context)
		effect.trait("Harsh", "agent", 1,2, context)
		effect.trait("Witty", "agent", 1, 2, context)
		effect.trait("Womanizer", "agent", 1,3, context)
	end
	return false
end

--[[ ThirdInitialTraits ]]--

function  ThirdInitialTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  ThirdInitialTraits_impl(context) then
		effect.trait("Aesthetic", "agent", 1,1, context)
		effect.trait("Analytical", "agent", 1,2, context)
		effect.trait("Approachable", "agent", 1,1, context)
		effect.trait("Austere", "agent", 1,2, context)
		effect.trait("Cautious", "agent", 1,2, context)
		effect.trait("Changeable", "agent", 1,1, context)
		effect.trait("Cheeky", "agent", 1,2, context)
		effect.trait("Complimenter", "agent", 1,1, context)
		effect.trait("Confrontational", "agent", 1,2, context)
		effect.trait("Courageous", "agent", 1,3, context)
		effect.trait("Dark_Humor", "agent", 1,3, context)
		effect.trait("Decisive", "agent", 1,2, context)
		effect.trait("Disorderly", "agent", 1,2, context)
		effect.trait("Drinker", "agent", 1,3, context)
		effect.trait("Energetic", "agent", 1,2, context)
		effect.trait("Envious", "agent", 1, 1, context)
		effect.trait("Eye_for_Women", "agent", 1,3, context)
		effect.trait("Friendly", "agent", 1,2, context)
		effect.trait("Gout", "agent", 1,1, context)
		effect.trait("Harsh", "agent", 1,3, context)
		effect.trait("Ill", "agent", 1,1, context)	
		effect.trait("Impassioned", "agent", 1,2, context)
		effect.trait("Impatient", "agent", 1,2, context)	
		effect.trait("Indecisive", "agent", 1,2, context)
		effect.trait("Insane", "agent", 1,3, context)	
		effect.trait("Kind", "agent", 1,2, context)	
		effect.trait("Leper", "agent", 1,2, context)
		effect.trait("Liar", "agent", 1,1, context)
		effect.trait("Motivated", "agent", 1,2, context)
		effect.trait("Nagger", "agent", 1,2, context)	
		effect.trait("Narcicisst", "agent", 1,2, context)	
		effect.trait("Optimist", "agent", 1,2, context)	
		effect.trait("Patient", "agent", 1,2, context)
		effect.trait("Persuasive", "agent", 1,2, context)
		effect.trait("Pious", "agent", 1,2, context)
		effect.trait("Pragmatic", "agent", 1,2, context)
		effect.trait("Rational", "agent", 1,2, context)
		effect.trait("Self_Deprecating_Humor", "agent", 1,2, context)
		effect.trait("Sharp", "agent", 1,2, context)		
		effect.trait("Skeptic", "agent", 1,1, context)
		effect.trait("Straightforward", "agent", 1,2, context)
		effect.trait("Unfriendly", "agent", 1,2, context)
		effect.trait("Unmotivated", "agent", 1,1, context)
		effect.trait("Upright", "agent", 1,2, context)
		effect.trait("Harsh", "agent", 1,2, context)
		effect.trait("Witty", "agent", 1, 2, context)
		effect.trait("Womanizer", "agent", 1,3, context)
	end
	return false
end

--[[ AppearanceTraits ]]--

function  AppearanceTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and not (context:character():has_trait("Handsome") or context:character():has_trait("Ugly")) and context:character():age() > 16 
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  AppearanceTraits_impl(context) then
		effect.trait("Handsome", "agent", 1, 3, context)
		effect.trait("Handsome", "agent", 2, 3, context)
		effect.trait("Handsome", "agent", 3, 2, context)
		effect.trait("Ugly", "agent", 1, 3, context)
		effect.trait("Ugly", "agent", 2, 2, context)
		return true
	end
	return false
end	

--[[ AuthorityTraits ]]--

function AuthorityTraits_impl (context)
    return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 and context:character():faction():culture() ~= "rom_Barbarian"
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if AuthorityTraits_impl(context) then 
		effect.trait("Authoritarian", "agent", 1, 4, context)
		effect.trait("Authoritarian", "agent", 2, 3, context)
		effect.trait("Authoritarian", "agent", 3, 2, context)
		effect.trait("Non_Authoritarian", "agent", 1, 2, context)
		effect.trait("Non_Authoritarian", "agent", 2, 3, context)
		effect.trait("Non_Authoritarian", "agent", 3, 2, context)
	end
	return false
end

--[[ AuthorityTraits2 ]]--

function AuthorityTraits2_impl (context)
    return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 and context:character():faction():culture() ~= "rom_Barbarian"
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
    if AuthorityTraits2_impl(context) then 
		effect.trait("Authoritarian", "agent", 1, 4, context)
		effect.trait("Authoritarian", "agent", 2, 3, context)
		effect.trait("Authoritarian", "agent", 3, 2, context)
		effect.trait("Non_Authoritarian", "agent", 1, 2, context)
		effect.trait("Non_Authoritarian", "agent", 2, 3, context)
		effect.trait("Non_Authoritarian", "agent", 3, 2, context)
	end
	return false
end

--[[ CharismaTraits ]]--

function  CharismaTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 and not context:character():has_trait("Charismatic")
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  CharismaTraits_impl(context) then
		effect.trait("Charismatic", "agent", 1, 2, context)
		effect.trait("Charismatic", "agent", 2, 3, context)
		effect.trait("Charismatic", "agent", 3, 1, context)
		effect.trait("Socially_Awkward", "agent", 1, 3, context)
		effect.trait("Socially_Awkward", "agent", 2, 1, context)
		effect.trait("Socially_Awkward", "agent", 3, 2, context)
		effect.trait("Socially_Awkward", "agent", 4, 1, context)
		return true
	end
	return false
end	

--[[ CharismaTraits2 ]]--

function  CharismaTraits2_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 and not context:character():has_trait("Charismatic")
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  CharismaTraits2_impl(context) then
		effect.trait("Charismatic", "agent", 1, 2, context)
		effect.trait("Charismatic", "agent", 2, 3, context)
		effect.trait("Charismatic", "agent", 3, 1, context)
		effect.trait("Socially_Awkward", "agent", 1, 3, context)
		effect.trait("Socially_Awkward", "agent", 2, 1, context)
		effect.trait("Socially_Awkward", "agent", 3, 2, context)
		effect.trait("Socially_Awkward", "agent", 4, 1, context)
		return true
	end
	return false
end	

--[[ FertilityTraits ]]--

function  FertilityTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and not (context:character():has_trait("Fertile") or context:character():has_trait("Infertile")) and context:character():age() > 16 
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  FertilityTraits_impl(context) then
		effect.trait("Fertile", "agent", 1, 2, context)
		effect.trait("Fertile", "agent", 2, 1, context)
		effect.trait("Fertile", "agent", 3, 1, context)
		effect.trait("Infertile", "agent", 1, 3, context)
		effect.trait("Infertile", "agent", 2, 1, context)
		effect.trait("Infertile", "agent", 1, 1, context)
		return true
	end
	return false
end	

--[[ FertilityTraits2 ]]--

function  FertilityTraits2_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and not (context:character():has_trait("Fertile") or context:character():has_trait("Infertile")) and context:character():age() > 16 
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  FertilityTraits2_impl(context) then
		effect.trait("Fertile", "agent", 1, 2, context)
		effect.trait("Fertile", "agent", 2, 1, context)
		effect.trait("Fertile", "agent", 3, 1, context)
		effect.trait("Infertile", "agent", 1, 3, context)
		effect.trait("Infertile", "agent", 2, 1, context)
		effect.trait("Infertile", "agent", 1, 1, context)
		return true
	end
	return false
end	

--[[ IntelligenceTraits ]]--

function  IntelligenceTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and not (context:character():has_trait("Intelligent") or context:character():has_trait("Ignorant")) and context:character():age() > 16 
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  IntelligenceTraits_impl(context) then
		effect.trait("Intelligent", "agent", 1, 2, context)
		effect.trait("Intelligent", "agent", 2, 3, context)
		effect.trait("Intelligent", "agent", 3, 2, context)
		effect.trait("Ignorant", "agent", 1, 2, context)
		effect.trait("Ignorant", "agent", 3, 2, context)
		return true
	end
	return false
end

--[[ IntelligenceTraits2 ]]--

function  IntelligenceTraits2_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and not (context:character():has_trait("Intelligent") or context:character():has_trait("Ignorant")) and context:character():age() > 16 
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  IntelligenceTraits2_impl(context) then
		effect.trait("Intelligent", "agent", 1, 2, context)
		effect.trait("Intelligent", "agent", 2, 3, context)
		effect.trait("Intelligent", "agent", 3, 2, context)
		effect.trait("Ignorant", "agent", 1, 2, context)
		effect.trait("Ignorant", "agent", 3, 2, context)
		return true
	end
	return false
end	

--[[ LoyaltyTraits ]]--

function  LoyaltyTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 and not context:character():is_faction_leader()
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  LoyaltyTraits_impl(context) then
		effect.trait("Loyal", "agent", 1, 3, context)
		effect.trait("Loyal", "agent", 2, 2, context)
		effect.trait("Loyal", "agent", 3, 1, context)
		effect.trait("Disloyal", "agent", 1, 2, context)
		effect.trait("Disloyal", "agent", 2, 2, context)
		effect.trait("Devious", "agent", 1, 2, context)
		effect.trait("Devious", "agent", 2, 2, context)
		return true
	end
	return false
end	

--[[ LoyaltyTraits2 ]]--

function  LoyaltyTraits2_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16 and not context:character():is_faction_leader()
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  LoyaltyTraits2_impl(context) then
		effect.trait("Loyal", "agent", 1, 3, context)
		effect.trait("Loyal", "agent", 2, 2, context)
		effect.trait("Loyal", "agent", 3, 1, context)
		effect.trait("Disloyal", "agent", 1, 2, context)
		effect.trait("Disloyal", "agent", 2, 2, context)
		effect.trait("Devious", "agent", 1, 2, context)
		effect.trait("Devious", "agent", 2, 2, context)
		return true
	end
	return false
end	

--[[ MilitarySkillTraits ]]--

function  MilitarySkillTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and not (context:character():has_trait("Natural_Military_Skill") or context:character():has_trait("Fainthearted")) and context:character():age() > 16 
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  MilitarySkillTraits_impl(context) then
		effect.trait("Natural_Military_Skill", "agent", 1, 4, context)
		effect.trait("Natural_Military_Skill", "agent", 2, 4, context)
		effect.trait("Natural_Military_Skill", "agent", 3, 3, context)
		effect.trait("Fainthearted", "agent", 1, 3, context)
		effect.trait("Fainthearted", "agent", 2, 3, context)
        effect.trait("Fainthearted", "agent", 3, 2, context)		
		return true
	end
	return false
end

--[[ MilitarySkillTraits2 ]]--

function  MilitarySkillTraits2_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and not (context:character():has_trait("Natural_Military_Skill") or context:character():has_trait("Fainthearted")) and context:character():age() > 16 
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  MilitarySkillTraits2_impl(context) then
		effect.trait("Natural_Military_Skill", "agent", 1, 4, context)
		effect.trait("Natural_Military_Skill", "agent", 2, 4, context)
		effect.trait("Natural_Military_Skill", "agent", 3, 3, context)
		effect.trait("Fainthearted", "agent", 1, 3, context)
		effect.trait("Fainthearted", "agent", 2, 3, context)
        effect.trait("Fainthearted", "agent", 3, 2, context)		
		return true
	end
	return false
end


--[[ FirstPoliticalTraits ]]--

function  FirstPoliticalTraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  FirstPoliticalTraits_impl(context) then
		effect.trait("Political_Schemer", "agent", 1, 3, context)
		effect.trait("Political_Schemer", "agent", 2, 4, context)		
		effect.trait("Political_Talent", "agent", 1, 4, context)
		effect.trait("Political_Talent", "agent", 2, 5, context)
		effect.trait("Political_Talent", "agent", 3, 3, context)
		effect.trait("Political_Talent", "agent", 4, 1, context)
		effect.trait("Political_Underdog", "agent", 1, 4, context)
		effect.trait("Orator", "agent", 1, 5, context)
		effect.trait("Orator", "agent", 2, 3, context)
		effect.trait("Orator", "agent", 3, 2, context)
	end
	return false
end

--[[ FirstPoliticalTraits2 ]]--

function  FirstPoliticalTraits2_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  FirstPoliticalTraits2_impl(context) then
		effect.trait("Political_Schemer", "agent", 1, 3, context)
		effect.trait("Political_Schemer", "agent", 2, 4, context)		
		effect.trait("Political_Talent", "agent", 1, 4, context)
		effect.trait("Political_Talent", "agent", 2, 5, context)
		effect.trait("Political_Talent", "agent", 3, 3, context)
		effect.trait("Political_Talent", "agent", 4, 1, context)
		effect.trait("Political_Underdog", "agent", 1, 4, context)
		effect.trait("Orator", "agent", 1, 5, context)
		effect.trait("Orator", "agent", 2, 3, context)
		effect.trait("Orator", "agent", 3, 2, context)
	end
	return false
end

--[[ FirstPoliticalAntitraits ]]--

function  FirstPoliticalAntitraits_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <3 and context:character():age() > 16
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  FirstPoliticalAntitraits_impl(context) then
		effect.trait("Unamusing", "agent", 1, 5, context)
		effect.trait("Unamusing", "agent", 2, 3, context)
		effect.trait("Unamusing", "agent", 3, 2, context)
	end
	return false
end

--[[ GoodAdmin1 ]]--

function GoodAdmin1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():town_wealth_growth() > 0 and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if GoodAdmin1_impl(context) then
		effect.trait("Able_Administrator", "agent", 1, 3, context)
		effect.trait("Effective_Taxman", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ GoodAdmin2 ]]--

function GoodAdmin2_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_admin" and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if GoodAdmin2_impl(context) then
		effect.trait("Able_Administrator", "agent", 1, 8, context)
		return true
	end
	return false
end

--[[ UpgradeGoodAdmin1 ]]--

function UpgradeGoodAdmin1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character() and context:character():region():town_wealth_growth() > 0 and context:character():has_trait("Able_Administrator")) and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if UpgradeGoodAdmin1_impl(context) then
		effect.trait("Able_Administrator", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ UpgradeEffective_Taxman1 ]]--

function UpgradeEffective_Taxman1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():town_wealth_growth() > 0 and context:character():has_trait("Effective_Taxman") and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if UpgradeEffective_Taxman1_impl(context) then
		effect.trait("Effective_Taxman", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Aesthetic1 ]]--

function Adaptable1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "rom_sch_entertainment" or context:building():superchain() == "rom_sch_aqueduct") and context:character():age() > 16
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Adaptable1_impl(context) then
		effect.trait("Aesthetic", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Adaptable1 ]]--

function Adaptable1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if Adaptable1_impl(context) then
		effect.trait("Adaptable", "agent", 1, 25, context)
		effect.trait("Dark_Humor", "agent", 1, 5, context)
		effect.trait("Cautious", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ UpgradeAdaptable1 ]]--

function UpgradeAdaptable1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Adaptable") and context:character():age() > 16
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if UpgradeAdaptable1_impl(context) then
		effect.trait("Adaptable", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ UpgradeCautious1 ]]--

function UpgradeCautious1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Cautious") and context:character():age() > 16
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if UpgradeCautious1_impl(context) then
		effect.trait("Cautious", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ UpgradeDark_Humor1 ]]--

function UpgradeDark_Humor1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Dark_Humor") and context:character():age() > 16
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if UpgradeDark_Humor1_impl(context) then
		effect.trait("Dark_Humor", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Affectionate1 ]]--

function Affectionate1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():town_wealth_growth() < 0 and context:character():has_trait("Pious") and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Affectionate1_impl(context) then
		effect.trait("Affectionate", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ UpgradeAffectionate1 ]]--

function UpgradeAffectionate1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():town_wealth_growth() < 0 and context:character():has_trait("Affectionate") 
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if UpgradeAffectionate1_impl(context) then
		effect.trait("Affectionate", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ AfricanHero1 ]]--

function AfricanHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and (context:character():faction():subculture() == "sc_rom_african_arabian" or context:character():faction():subculture() == "sc_rom_berber"))
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if AfricanHero1_impl (context) then
	    effect.trait("African_Hero", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ UpgradeAfricanHero1 ]]--

function UpgradeAfricanHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():has_trait("African_Hero"))
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if UpgradeAfricanHero1_impl (context) then
	    effect.trait("African_Hero", "agent", 1, 50, context)
		return true
	end
	return false
end	

--[[ Animal_Friend1 ]]--

function Animal_Friend1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_land"  and context:character():number_of_traits() <10
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Animal_Friend1_impl(context) then
		effect.trait("Animal_Friend", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Upgrade_Animal_Friend1 ]]--

function Upgrade_Animal_Friend1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_land"  and context:character():has_trait("Animal_Friend")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Upgrade_Animal_Friend1_impl(context) then
		effect.trait("Animal_Friend", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ ArabHero1 ]]--

function ArabHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():faction():subculture() == "sc_rom_african_arabian")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if ArabHero1_impl (context) then
	    effect.trait("Arab_Hero", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ UpgradeArabHero1 ]]--

function UpgradeArabHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():has_trait("Arab_Hero"))
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if UpgradeArabHero1_impl (context) then
	    effect.trait("Arab_Hero", "agent", 1, 50, context)
		return true
	end
	return false
end	

--[[ ArabMerchant1 ]]--

function ArabMerchant1_impl (context)
        return context:character():has_trait("Good_Trader") and context:character():faction():subculture() == "sc_rom_african_arabian" and context:character():age() > 16
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if ArabMerchant1_impl(context) then
	    effect.trait("Arab_Merchant", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ Architect1 ]]--

function Architect1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:character():faction():culture() ~= "rom_Barbarian" and context:character():number_of_traits() <10
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Architect1_impl(context) then
		effect.trait("Architect", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Architect1 ]]--

function Upgrade_Architect1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:character():has_trait("Architect")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Upgrade_Architect1_impl(context) then
		effect.trait("Architect", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ Authoritarian1 ]]--

function Authoritarian1_impl (context)
		return context:character():has_region() and char_is_governor(context:character()) and (context:character():faction():tax_level() > 35 or context:character():region():public_order() <= 1.0) and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian" and not context:character():has_trait("Unpopular_Governor") and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Authoritarian1_impl(context) then
		effect.trait("Authoritarian", "agent", 1, 5, context)
		effect.trait("Unjust", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Authoritarian2 ]]--

function Authoritarian2_impl (context)
		return context:character():has_region() and char_is_governor(context:character()) and (context:character():faction():tax_level() > 35 or context:character():region():public_order() <= 1.0) and context:character():number_of_traits() <10 and context:character():faction():culture() == "rom_Barbarian" and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Authoritarian2_impl(context) then
		effect.trait("Unjust", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Authoritarian1 ]]--

function Upgrade_Authoritarian1_impl (context)
		return  context:character():has_region() and char_is_governor(context:character()) and context:character():region():public_order() <= 1.0 and context:character():has_trait("Authoritarian")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Authoritarian1_impl(context) then
		effect.trait("Authoritarian", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Upgrade_Unjust1 ]]--

function Upgrade_Unjust1_impl (context)
		return  context:character():has_region() and char_is_general(context:character()) and context:character():region():public_order() <= 1.0 and context:character():has_trait("Unjust")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Unjust1_impl(context) then
		effect.trait("Unjust", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ BadPlanner1 ]]--

function BadPlanner1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false and context:pending_battle():has_contested_garrison()
end

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if BadPlanner1_impl(context) then
	    effect.trait("Bad_Planner", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ Upgrade_BadPlanner1 ]]--

function Upgrade_BadPlanner1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Bad_Planner")
end

events.CharacterBesiegesSettlement[#events.CharacterBesiegesSettlement+1] =
function (context)
    if Upgrade_BadPlanner1_impl(context) then
	    effect.trait("Bad_Planner", "agent", 1, 20, context)
		return true
	end
	return false
end	

--[[ BadSiegeDefender1 ]]--

function BadSiegeDefender1_impl (context)
        return context:character():in_settlement() and context:character():model():turn_number() % 5 == 0 and context:character():age() > 16
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if BadSiegeDefender1_impl(context) then
	    effect.trait("Bad_Siege_Defender", "agent", 1, 4, context)
		return true
	end
	return false
end	

--[[ Upgrade_BadSiegeDefender1 ]]--

function Upgrade_BadSiegeDefender1_impl (context)
        return context:character():in_settlement() and context:character():model():turn_number() % 5 == 0 and context:character():has_trait("Bad_Siege_Defender")
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_BadSiegeDefender1_impl(context) then
	    effect.trait("Bad_Siege_Defender", "agent", 1, 6, context)
		return true
	end
	return false
end	

--[[ Beekeper1 ]]--

function Beekeper1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Beekeper1_impl(context) then
		effect.trait("Beekeeper", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Brutal1 ]]--

function Brutal1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10
end 

events.CharacterPostBattleSlaughter[#events.CharacterPostBattleSlaughter+1] =
function (context)
	if Brutal1_impl(context) then
		effect.trait("Devious", "agent", 1, 1, context)
		effect.trait("Harsh", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Brutal2 ]]--

function Brutal2_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterPostBattleSlaughter[#events.CharacterPostBattleSlaughter+1] =
function (context)
	if Brutal2_impl(context) then
		effect.trait("Brutal", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ Brutal3 ]]--

function Brutal3_impl (context)
		return (char_is_general(context:character()) and context:character():number_of_traits() <10 and not context:character():faction():culture() == "rom_Hellenistic" and not context:character():faction():culture() == "rom_Roman" and not context:character():faction():culture() == "rom_Eastern")
end 

events.CharacterPostBattleSlaughter[#events.CharacterPostBattleSlaughter+1] =
function (context)
	if Brutal3_impl(context) then
		effect.trait("Savage", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ Upgrade_Brutal1 ]]--

function Upgrade_Brutal1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Brutal")
end 

events.CharacterPostBattleSlaughter[#events.CharacterPostBattleSlaughter+1] =
function (context)
	if Upgrade_Brutal1_impl(context) then
		effect.trait("Brutal", "agent", 1, 40, context)
		return true
	end
	return false
end
--[[ Upgrade_Devious1 ]]--

function Upgrade_Devious1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Devious")
end 

events.CharacterPostBattleSlaughter[#events.CharacterPostBattleSlaughter+1] =
function (context)
	if Upgrade_Devious1_impl(context) then
		effect.trait("Devious", "agent", 1, 15, context)
		return true
	end
	return false
end
--[[ Upgrade_Harsh1 ]]--

function Upgrade_Harsh1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Harsh")
end 

events.CharacterPostBattleSlaughter[#events.CharacterPostBattleSlaughter+1] =
function (context)
	if Upgrade_Harsh1_impl(context) then
		effect.trait("Harsh", "agent", 1, 10, context)
		return true
	end
	return false
end
--[[ Upgrade_Savage1 ]]--

function Upgrade_Savage1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Savage")
end 

events.CharacterPostBattleSlaughter[#events.CharacterPostBattleSlaughter+1] =
function (context)
	if Upgrade_Savage1_impl(context) then
		effect.trait("Savage", "agent", 1, 40, context)
		return true
	end
	return false
end

--[[ CapturedEagle1 ]]--

function CapturedEagle1_impl (context)
		return ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:pending_battle():defender():faction():culture() == "rom_Roman") or (context:pending_battle():defender() == context:character() and context:character():won_battle() == true and context:pending_battle():attacker():faction():culture() == "rom_Roman")) and context:character():faction():culture() ~= "rom_Roman")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if CapturedEagle1_impl(context) then
	    effect.trait("Captured_Eagle", "agent", 1, 75, context)
		return true
	end
	return false
end		

--[[ CarthaginianHero1 ]]--

function CarthaginianHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():faction():subculture() == "sc_rom_carthaginian")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if CarthaginianHero1_impl (context) then
	    effect.trait("Carthaginian_Hero", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ UpgradeCarthaginianHero1 ]]--

function UpgradeCarthaginianHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():has_trait("Carthaginian_Hero"))
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if UpgradeCarthaginianHero1_impl (context) then
	    effect.trait("Carthaginian_Hero", "agent", 1, 50, context)
		return true
	end
	return false
end	

--[[ Cavalryman1 ]]--

function Cavalryman1_impl (context)
		return char_is_general(context:character()) and context:character():has_military_force() and character_get_percentage_of_unit_class_in_army("cav_mel", context:character():military_force():unit_list()) >= 0.40  and context:character():won_battle() == true and context:character():number_of_traits() <10
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Cavalryman1_impl(context) then
		effect.trait("Cavalryman", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Cavalryman1 ]]--

function Upgrade_Cavalryman1_impl (context)
		return char_is_general(context:character()) and context:character():has_military_force() and character_get_percentage_of_unit_class_in_army("cav_mel", context:character():military_force():unit_list()) >= 0.40  and context:character():won_battle() == true and context:character():has_trait("Cavalryman")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Cavalryman1_impl(context) then
		effect.trait("Cavalryman", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Civic_Crown1 ]]--

function Civic_Crown1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() and context:pending_battle():has_contested_garrison() and context:character():faction():culture() == "rom_Roman" and context:pending_battle():defender_battle_result() == "heroic_victory"
end 

events.CharacterParticipatedAsSecondaryGeneralInBattle[#events.CharacterParticipatedAsSecondaryGeneralInBattle+1] =
function (context)
	if Civic_Crown1_impl(context) then
		effect.trait("Civic_Crown", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ Close_Minded1 ]]--

function Close_Minded1_impl (context)
		return context:character():is_politician() and context:character():faction():at_war()
end 

events.CharacterRankUp[#events.CharacterRankUp+1] =
function (context)
	if Close_Minded1_impl(context) then
		effect.trait("Close_Minded", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Complimenter1 ]]--

function Complimenter1_impl (context)
        return char_is_general(context:character()) and context:character():is_politician() and context:character():age() > 16 
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if Complimenter1_impl(context) then
	    effect.trait("Complimenter", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Confederationist1 ]]--

function Confederationist1_impl (context)
        return char_is_general(context:character()) and context:character():is_politician() and context:character():faction():at_war() and context:character():faction():culture() == "rom_Barbarian" and context:character():age() > 16
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if Confederationist1_impl(context) then
	    effect.trait("Confederationist", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Upgrade_Confrontational1 ]]--

function Upgrade_Confrontational1_impl (context)
	    return context:character():has_trait("Confrontational")
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Confrontational1_impl(context) then
	    effect.trait("Confrontational", "agent", 1, 4, context)
		return true
	end
	return false
end	

--[[ Corrupt1 ]]--

function Corrupt1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():faction():treasury() > 50000 and context:character():faction():is_human() and not context:character():faction():is_horde() and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Corrupt1_impl(context) then
		effect.trait("Corrupt", "agent", 1, 5, context)
		effect.trait("Embezzler", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Corrupt2 ]]--

function Corrupt2_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():town_wealth_growth() > 0 and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Corrupt2_impl(context) then
		effect.trait("Corrupt", "agent", 1, 1, context)
		effect.trait("Embezzler", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Corrupt1 ]]--

function Upgrade_Corrupt1_impl (context)
		return char_is_governor(context:character()) and context:character():faction():treasury() > 25000 and context:character():faction():is_human() and not context:character():faction():is_horde()  and context:character():has_trait("Corrupt")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Corrupt1_impl(context) then
		effect.trait("Corrupt", "agent", 1, 8, context)
		return true
	end
	return false
end

--[[ Corrupt_Trader1 ]]--

function Corrupt_Trader1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "ai_sch_port_generic" and context:character():faction():treasury() > 25000 and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Corrupt_Trader1_impl(context) then
		effect.trait("Corrupt_Trader", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Corrupt_Trader1 ]]--

function Upgrade_Corrupt_Trader1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "ai_sch_port_generic" and context:character():faction():treasury() > 25000 and context:character():has_trait("Corrupt_Trader")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Corrupt_Trader1_impl(context) then
		effect.trait("Corrupt_Trader", "agent", 1, 8, context)
		return true
	end
	return false
end

--[[ Courageous1 ]]--

function Courageous1_impl (context)
		return char_is_general_with_army(context:character()) and  ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee())) and context:character():number_of_traits() <10
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Courageous1_impl(context) then
		effect.trait("Courageous", "agent", 1, 15, context)
		effect.trait("Scarred", "agent", 1, 5, context)
		effect.trait("Scarred_Cripple", "agent", 1, 2, context)
		return true
	end
	return false
end
--[[ Upgrade_Courageous1 ]]--

function Upgrade_Courageous1_impl (context)
		return char_is_general_with_army(context:character()) and  ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee()) ) and context:character():has_trait("Courageous")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Courageous1_impl(context) then
		effect.trait("Courageous", "agent", 1, 15, context)
		return true
	end
	return false
end
--[[ Upgrade_Scarred1 ]]--

function Upgrade_Scarred1_impl (context)
		return char_is_general_with_army(context:character()) and  ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee()) ) and context:character():has_trait("Scarred")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Scarred1_impl(context) then
		effect.trait("Scarred", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Coward1 ]]--

function Coward1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():faction():is_human() and ((context:pending_battle():attacker() == context:character() and not context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and not context:pending_battle():defender_commander_fought_in_melee())) and context:character():number_of_traits() <10
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Coward1_impl(context) then
		effect.trait("Coward", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Coward2 ]]--

function Coward2_impl (context)
		return char_is_general_with_army(context:character()) and context:character():faction():is_human() and ((context:pending_battle():attacker() == context:character() and not context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and not context:pending_battle():defender_commander_fought_in_melee())) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false and context:pending_battle():attacker_battle_result() == "decisive_defeat" and context:character():number_of_traits() <10
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Coward2_impl(context) then
		effect.trait("Coward", "agent", 1, 20, context)
		return true
	end
	return false
end

--[[ Upgrade_Coward1 ]]--

function Upgrade_Coward1_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():faction():is_human() and ((context:pending_battle():attacker() == context:character() and not context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and not context:pending_battle():defender_commander_fought_in_melee())) and context:character():has_trait("Coward")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Coward1_impl(context) then
		effect.trait("Coward", "agent", 1, 70, context)
		return true
	end
	return false
end

--[[ Cuckold1 ]]--

function Cuckold1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10
end 

events.CharacterMarriage[#events.CharacterMarriage+1] =
function (context)
	if Cuckold1_impl(context) then
		effect.trait("Cuckold", "agent", 1, 6, context)
		return true
	end
	return false
end

--[[ Upgrade_Cuckold1 ]]--

function Upgrade_Cuckold1_impl (context)
		return  char_is_general(context:character()) and context:character():gravitas() <= 25 and context:character():has_trait("Cuckold")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Cuckold1_impl(context) then
		effect.trait("Cuckold", "agent", 1, 3, context)
		return true
	end
	return false
end


--[[ Decisive1 ]]--

function Decisive1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():won_battle() and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Decisive1_impl(context) then
		effect.trait("Decisive", "agent", 1, 5, context)
		effect.trait("Proud", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Decisive2 ]]--

function Decisive2_impl (context)
		return char_is_general_with_army(context:character()) and context:character():action_points_remaining_percent() <= 5
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Decisive2_impl(context) then
		effect.trait("Decisive", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Decisive1 ]]--

function Upgrade_Decisive1_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():won_battle() and context:character():has_trait("Decisive")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Decisive1_impl(context) then
		effect.trait("Decisive", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Upgrade_Proud1 ]]--

function Upgrade_Proud1_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():won_battle() and context:character():has_trait("Proud")

end

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Proud1_impl(context) then
		effect.trait("Proud", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ Depressed1 ]]--

function Depressed1_impl (context)
		return char_is_general(context:character()) and context:character():age() > 16
end 

events.CharacterFamilyRelationDied[#events.CharacterFamilyRelationDied+1] =
function (context)
	if Depressed1_impl(context) then
		effect.trait("Depressed", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Depressed2 ]]--

function Depressed2_impl (context)
		return char_is_general(context:character()) and context:character():age() > 65
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Depressed2_impl(context) then
		effect.trait("Depressed", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Lost_Battle1 ]]--

function Lost_Battle1_impl (context)
		return char_is_general_with_army(context:character()) and (context:character():won_battle() == false and context:pending_battle():attacker() == context:character() and context:pending_battle():percentage_of_attacker_killed() >= 65) or (context:character():won_battle() == false and context:pending_battle():defender() == context:character() and context:pending_battle():percentage_of_defender_killed() >= 65)
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Lost_Battle1_impl(context) then
		effect.trait("Depressed", "agent", 1, 10, context)
		effect.trait("Dark_Humor", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Depressed_Removed1 ]]--

function Depressed_Removed1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Depressed") and context:character():model():turn_number() > 2 and context:character():age() < 65
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Depressed_Removed1_impl(context) then
		effect.trait("Depressed_Removed", "agent", 1, 8, context)
		return true
	end
	return false
end

--[[ Destined1 ]]--

function Destined_to_Rule1_impl (context)
		return context:character():is_faction_leader()
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Destined_to_Rule1_impl(context) then
		effect.trait("Destined_to_Rule", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Diplomatic1 ]]--

function Diplomatic1_impl (context)
		return char_is_general(context:character()) and context:character():is_politician() and char_rank_between(context:character(), 2, 9) and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Diplomatic1_impl(context) then
		effect.trait("Diplomatic", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Diplomatic1 ]]--

function Upgrade_Diplomatic1_impl (context)
		return  context:character():is_politician() and context:character():has_trait("Diplomatic")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Diplomatic1_impl(context) then
		effect.trait("Diplomatic", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Disciplinarian1 ]]--

function Disciplinarian1_impl (context)
		return char_is_general_with_army(context:character())  and context:character():model():turn_number() % 10 == 0 and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Disciplinarian1_impl(context) then
		effect.trait("Disciplinarian", "agent", 1, 5, context)
		effect.trait("Discriminatory", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Disciplinarian1 ]]--

function Upgrade_Disciplinarian1_impl (context)
		return  char_is_general_with_army(context:character())  and context:character():has_trait("Disciplinarian")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Disciplinarian1_impl(context) then
		effect.trait("Disciplinarian", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Discriminatory1 ]]--

function Upgrade_Discriminatory1_impl (context)
		return  char_is_general_with_army(context:character())  and context:character():has_trait("Discriminatory")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Discriminatory1_impl(context) then
		effect.trait("Discriminatory", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Disloyal1 ]]--

function Disloyal1_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_enemy_regions() >= 10 and context:character():model():turn_number() > 1 and context:character():model():turn_number() % 5 == 0 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Disloyal1_impl(context) then
		effect.trait("Disloyal", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Drinker1 ]]--

function Drinker1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 10 and context:character():in_settlement()  and context:character():model():turn_number() % 5 == 0 and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Drinker1_impl(context) then
		effect.trait("Drinker", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Drinker1 ]]--

function Upgrade_Drinker1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Drinker")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Drinker1_impl(context) then
		effect.trait("Drinker", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Dysentery1 ]]--

function Dysentery1_impl (context)
		return char_is_general_with_army(context:character())

end 

events.CharacterEntersAttritionalArea[#events.CharacterEntersAttritionalArea+1] =
function (context)
	if Dysentery1_impl(context) then
		effect.trait("Dysentery", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Dysentery_Removed1 ]]--

function Dysentery_Removed1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Dysentery")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Dysentery_Removed1_impl(context) then
		effect.trait("Dysentery_Removed", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ Dysentery_Removed2 ]]--

function Dysentery_Removed2_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Dysentery") and context:character():has_trait("Healthy")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Dysentery_Removed2_impl(context) then
		effect.trait("Dysentery_Removed", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ EasternHero1 ]]--

function EasternHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and (context:character():faction():subculture() == "sc_rom_african_armenian" or context:character():faction():subculture() == "sc_rom_eastern" or context:character():faction():subculture() == "sc_rom_parthian"))
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if EasternHero1_impl (context) then
	    effect.trait("Eastern_Hero", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ UpgradeEasternHero1 ]]--

function UpgradeEasternHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():has_trait("Eastern_Hero"))
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if UpgradeEasternHero1_impl (context) then
	    effect.trait("Eastern_Hero", "agent", 1, 50, context)
		return true
	end
	return false
end	

--[[ Eccentric1 ]]--

function Eccentric1_impl (context)
		return char_is_general_with_army(context:character()) and char_rank_between(context:character(), 5, 9) and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Eccentric1_impl(context) then
		effect.trait("Eccentric", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Upgrade_Eccentric1 ]]--

function Upgrade_Eccentric1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Eccentric")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Eccentric1_impl(context) then
		effect.trait("Eccentric", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Envious1 ]]--

function Envious1_impl (context)
		return char_is_general(context:character()) and context:character():is_politician() and context:character():has_father() and context:character():father():has_trait("Faction_Leader") and context:character():number_of_traits() <8 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Envious1_impl(context) then
		effect.trait("Envious", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Envious1 ]]--

function Upgrade_Envious1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Envious")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Envious1_impl(context) then
		effect.trait("Envious", "agent", 1, 5, context)
		return true
	end
	return false
end



--[[ Epicurean1 ]]--

function Epicurean1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Eye_for_Women") and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16 

end 

events.CharacterMarriage[#events.CharacterMarriage+1] =
function (context)
	if Epicurean1_impl(context) then
		effect.trait("Epicurean", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Epicurean2 ]]--

function Epicurean2_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():has_trait("Gourmand") and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16 

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Epicurean2_impl(context) then
		effect.trait("Epicurean", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Epicurean3 ]]--

function Epicurean3_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <4 and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16 

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Epicurean3_impl(context) then
		effect.trait("Epicurean", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Upgrade_Epicurean1 ]]--

function Upgrade_Epicurean1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Epicurean")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Epicurean1_impl(context) then
		effect.trait("Epicurean", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Eye_for_Women1 ]]--

function Eye_for_Women1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10 and not context:character():is_faction_leader()

end 

events.CharacterMarriage[#events.CharacterMarriage+1] =
function (context)
	if Eye_for_Women1_impl(context) then
		effect.trait("Eye_for_Women", "agent", 1, 5, context)
		effect.trait("Womanizer", "agent", 1, 5, context)
		effect.trait("Loyal", "agent", 1, 3, context)
		effect.trait("Disloyal", "agent", 1, 3, context)
		return true
	end
	return false
end


--[[ Eye_for_Women2 ]]--

function Eye_for_Women2_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Disloyal") and context:character():number_of_traits() <10

end 

events.CharacterEntersGarrison[#events.CharacterEntersGarrison+1] =
function (context)
	if Eye_for_Women2_impl(context) then
		effect.trait("Eye_for_Women", "agent", 1, 5, context)
		effect.trait("Womanizer", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Eye_for_Women3 ]]--

function Eye_for_Women3_impl (context)
		return char_is_general(context:character()) and context:character():is_faction_leader() and context:character():number_of_traits() <10

end 

events.CharacterMarriage[#events.CharacterMarriage+1] =
function (context)
	if Eye_for_Women3_impl(context) then
		effect.trait("Eye_for_Women", "agent", 1, 5, context)
		effect.trait("Womanizer", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Eye_for_Women1 ]]--

function Upgrade_Eye_for_Women1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Eye_for_Women")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Eye_for_Women1_impl(context) then
		effect.trait("Eye_for_Women", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Womanizer1 ]]--

function Upgrade_Womanizer1_impl (context)
		return  char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 2 and context:character():in_settlement() and context:character():faction():is_human() and context:character():model():turn_number() > 2 and context:character():model():turn_number() % 5 == 0  and context:character():has_trait("Womanizer")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Womanizer1_impl(context) then
		effect.trait("Womanizer", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Foulmouthed1 ]]--

function Foulmouthed1_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 10 and context:character():in_port() and context:character():faction():is_human() and context:character():model():turn_number() > 1 and context:character():model():turn_number() % 5 == 0 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Foulmouthed1_impl(context) then
		effect.trait("Foulmouthed", "agent", 1, 8, context)
		effect.trait("Eye_for_Women", "agent", 1, 9, context)
		effect.trait("Drinker", "agent", 1, 5, context)
		effect.trait("Epicurean", "agent", 1, 4, context)
		effect.trait("Gambler", "agent", 1, 6, context)
		effect.trait("Slothful", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Foulmouthed2 ]]--

function Foulmouthed2_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 10 and context:character():in_settlement() and context:character():model():turn_number() > 1 and context:character():model():turn_number() % 5 == 0 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Foulmouthed2_impl(context) then
		effect.trait("Foulmouthed", "agent", 1, 3, context)
		effect.trait("Eye_for_Women", "agent", 1, 9, context)
		effect.trait("Drinker", "agent", 1, 5, context)
		effect.trait("Epicurean", "agent", 1, 4, context)
		effect.trait("Gambler", "agent", 1, 6, context)
		effect.trait("Slothful", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Drinker_Macedonian ]]--

function Drinker_Macedonian_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 5 and context:character():in_settlement() and context:character():model():turn_number() > 1 and context:character():model():turn_number() % 5 == 0 and context:character():faction():subculture() == "sc_rom_hellenistic" and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Drinker_Macedonian_impl(context) then
		effect.trait("Drinker", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Upgrade_Foulmouthed1 ]]--

function Upgrade_Foulmouthed1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Foulmouthed")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Foulmouthed1_impl(context) then
		effect.trait("Foulmouthed", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Friendly1 ]]--

function Friendly1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():has_trait("Pious") and context:character():age() > 50 and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Friendly1_impl(context) then
		effect.trait("Friendly", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Generous1 ]]--

function Generous1_impl (context)
		return char_is_governor(context:character()) and context:character():has_trait("Pious") and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Generous1_impl(context) then
		effect.trait("Generous", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Generous1 ]]--

function Upgrade_Generous1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Generous")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Generous1_impl(context) then
		effect.trait("Generous", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Good_Ambusher1 ]]--

function Good_Ambusher1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:pending_battle():ambush_battle() and context:character():number_of_traits() <10
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Good_Ambusher1_impl(context) then
		effect.trait("Good_Ambusher", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Good_Ambusher1 ]]--

function Upgrade_Good_Ambusher1_impl (context)
		return  char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:pending_battle():ambush_battle() and context:character():has_trait("Good_Ambusher")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Good_Ambusher1_impl(context) then
		effect.trait("Good_Ambusher", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Good_Attacker1 ]]--

function Good_Attacker1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Good_Attacker1_impl(context) then
		effect.trait("Good_Attacker", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Good_Attacker2 ]]--

function Good_Attacker2_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and (context:pending_battle():attacker_battle_result() == "decisive_victory" or context:pending_battle():attacker_battle_result() == "heroic_victory") and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Good_Attacker2_impl(context) then
		effect.trait("Good_Attacker", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Upgrade_Good_Attacker1 ]]--

function Upgrade_Good_Attacker1_impl (context)
		return  char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:character():has_trait("Good_Attacker")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Good_Attacker1_impl(context) then
		effect.trait("Good_Attacker", "agent", 1, 15, context)
		return true
	end
	return false
end


--[[ Good_Defender1 ]]--

function Good_Defender1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() == true and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Good_Defender1_impl(context) then
		effect.trait("Good_Defender", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Good_Defender2 ]]--

function Good_Defender2_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() == true and (context:pending_battle():defender_battle_result() == "decisive_victory" or context:pending_battle():defender_battle_result() == "heroic_victory") and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Good_Defender2_impl(context) then
		effect.trait("Good_Defender", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Good_Defender1 ]]--

function Upgrade_Good_Defender1_impl (context)
		return  char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() == true and context:character():has_trait("Good_Defender")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Good_Defender1_impl(context) then
		effect.trait("Good_Defender", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ GoodPersonalSecurity1 ]]--

function GoodPersonalSecurity1_impl (context)
		return char_is_general(context:character()) and context:character():age() > 16
end 

events.CharacterFamilyRelationDied[#events.CharacterFamilyRelationDied+1] =
function (context)
	if GoodPersonalSecurity1_impl(context) then
		effect.trait("Good_Personal_Security", "agent", 1, 10, context)
		effect.trait("Insane", "agent", 1, 2, context)
		effect.trait("Inconsistent", "agent", 1, 2, context)
		effect.trait("Vengeful", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ GoodPersonalSecurity2 ]]--

function GoodPersonalSecurity2_impl (context)
		return char_is_general(context:character()) and context:character():age() > 16
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if GoodPersonalSecurity2_impl(context) then
		effect.trait("Good_Personal_Security", "agent", 1, 30, context)
		effect.trait("Insane", "agent", 1, 2, context)
		effect.trait("Inconsistent", "agent", 1, 2, context)
		effect.trait("Vengeful", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_GoodPersonalSecurity1 ]]--

function Upgrade_GoodPersonalSecurity1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Good_Personal_Security")
end 

events.CharacterFamilyRelationDied[#events.CharacterFamilyRelationDied+1] =
function (context)
	if Upgrade_GoodPersonalSecurity1_impl(context) then
		effect.trait("Good_Personal_Security", "agent", 1, 20, context)
		effect.trait("Insane", "agent", 1, 3, context)
		effect.trait("Vengeful", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ GoodPlanner1 ]]--

function GoodPlanner1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:pending_battle():has_contested_garrison() and context:character():faction():culture() ~= "rom_Barbarian"
end

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if GoodPlanner1_impl(context) then
	    effect.trait("Good_Planner", "agent", 1, 15, context)
		return true
	end
	return false
end	

--[[ Upgrade_GoodPlanner1 ]]--

function Upgrade_GoodPlanner1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Good_Planner")
end

events.CharacterBesiegesSettlement[#events.CharacterBesiegesSettlement+1] =
function (context)
    if Upgrade_GoodPlanner1_impl(context) then
	    effect.trait("Good_Planner", "agent", 1, 25, context)
		return true
	end
	return false
end	

--[[ GoodSiegeDefender1 ]]--

function GoodSiegeDefender1_impl (context)
        return char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() == true and context:pending_battle():has_contested_garrison() and context:character():age() > 16
end

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if GoodSiegeDefender1_impl(context) then
	    effect.trait("Good_Siege_Defender", "agent", 1, 40, context)
		return true
	end
	return false
end	

--[[ Upgrade_GoodSiegeDefender1 ]]--

function Upgrade_GoodSiegeDefender1_impl (context)
        return char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() == true and context:pending_battle():has_contested_garrison() and context:character():has_trait("Good_Siege_Defender")
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_GoodSiegeDefender1_impl(context) then
	    effect.trait("Good_Siege_Defender", "agent", 1, 6, context)
		return true
	end
	return false
end	

--[[ Good_Trader1 ]]--

function Good_Trader1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():faction():trade_value_percent() > 25 and context:character():model():turn_number() > 5 and context:character():number_of_traits() <10 and context:character():age() > 16
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Good_Trader1_impl(context) then
		effect.trait("Good_Trader", "agent", 1, 5, context)
		effect.trait("Gourmand", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Good_Trader2 ]]--

function Good_Trader2_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "rom_sch_port_generic" or context:building():superchain() == "rom_sch_roads") and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Good_Trader2_impl(context) then
		effect.trait("Good_Trader", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Gourmand1 ]]--

function Upgrade_Gourmand1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Gourmand")
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Gourmand1_impl(context) then
		effect.trait("Gourmand", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Grass_Crown1 ]]--

function Grass_Crown1_impl (context)
		return ((char_is_general_with_army(context:character()) and context:pending_battle():ambush_battle() and context:pending_battle():defender() == context:character() and context:character():won_battle() == true) and context:character():faction():culture() == "rom_Roman")
end 

events.CharacterParticipatedAsSecondaryGeneralInBattle[#events.CharacterParticipatedAsSecondaryGeneralInBattle+1] =
function (context)
	if Grass_Crown1_impl(context) then
		effect.trait("Grass_Crown", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ HatesBarbarians1 ]]--

function HatesBarbarians1_impl (context)
		return ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():culture() == "rom_Barbarian") or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():culture() == "rom_Barbarian")) and context:character():faction():culture() ~= "rom_Barbarian")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if HatesBarbarians1_impl(context) then
		effect.trait("Hates_Barbarians", "agent", 1, 10, context)
		effect.trait("Vengeful", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ HatesCarthaginians1 ]]--

function HatesCarthaginians1_impl (context)
		return ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():subculture() == "sc_rom_carthaginian" ) or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():subculture() == "sc_rom_carthaginian")) and context:character():faction():subculture() ~= "sc_rom_carthaginian") 
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if HatesCarthaginians1_impl(context) then
		effect.trait("Hates_Carthaginians", "agent", 1, 10, context)
		effect.trait("Vengeful", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ HatesEasterners1 ]]--

function HatesEasterners1_impl (context)
		return ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():culture() == "rom_Eastern") or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():culture() == "rom_Eastern")) and context:character():faction():culture() ~= "rom_Eastern")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if HatesEasterners1_impl(context) then
		effect.trait("Hates_Easterners", "agent", 1, 10, context)
		effect.trait("Vengeful", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ HatesGreeks1 ]]--

function HatesGreeks1_impl (context)
		return ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():culture() == "rom_Hellenistic") or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():culture() == "rom_Hellenistic")) and context:character():faction():culture() ~= "rom_Hellenistic")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if HatesGreeks1_impl(context) then
		effect.trait("Hates_Greeks", "agent", 1, 10, context)
		effect.trait("Vengeful", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ HatesRomans1 ]]--

function HatesRomans1_impl (context)
		return ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():culture() == "rom_Roman") or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():culture() == "rom_Roman")) and context:character():faction():culture() ~= "rom_Roman")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if HatesRomans1_impl(context) then
		effect.trait("Hates_Romans", "agent", 1, 10, context)
		effect.trait("Vengeful", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Healthy1 ]]--

function Healthy1_impl (context)
		return char_is_agent(context:character()) and context:character():has_region() and context:character():region():squalor() < 5 and context:character():model():turn_number() > 5 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Healthy1_impl(context) then
		effect.trait("Healthy", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Healthy1 ]]--

function Upgrade_Healthy1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Healthy")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Healthy1_impl(context) then
		effect.trait("Healthy", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ HellenicHero1 ]]--

function HellenicHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and (context:character():faction():subculture() == "sc_rom_egyptian" or context:character():faction():subculture() == "sc_rom_greek" or context:character():faction():subculture() == "sc_rom_hellenistic" or context:character():faction():subculture() == "sc_rom_pontic"))
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if HellenicHero1_impl (context) then
	    effect.trait("Hellenic_Hero", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ Upgrade_HellenicHero1 ]]--

function Upgrade_HellenicHero1_impl (context)
        return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():has_trait("Hellenic_Hero")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if Upgrade_HellenicHero1_impl (context) then
	    effect.trait("Hellenic_Hero", "agent", 1, 50, context)
		return true
	end
	return false
end	

--[[ Humble1 ]]--

function Humble1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Humble1_impl(context) then
		effect.trait("Humble", "agent", 1, 8, context)
		return true
	end
	return false
end

--[[ Upgrade_Humble1 ]]--

function Upgrade_Humble1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Humble")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Humble1_impl(context) then
		effect.trait("Humble", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Ignorant1 ]]--

function Upgrade_Ignorant1_impl (context)
		return context:character():has_trait("Ignorant")
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Upgrade_Ignorant1_impl(context) then
	    effect.trait("Ignorant", "agent", 1,3, context)
		return true
	end
	return false
end	

--[[ Ill1 ]]--

function Ill1_impl (context)
		return char_is_agent(context:character()) and context:character():age() > 50 and context:character():number_of_traits() <10
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Ill1_impl(context) then
		effect.trait("Ill", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Upgrade_Ill1 ]]--

function Upgrade_Ill1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Ill")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Ill1_impl(context) then
		effect.trait("Ill", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Impassionate1 ]]--

function Impassionate1_impl (context)
		return char_is_general_with_army(context:character())
end 

events.CharacterPostBattleEnslave[#events.CharacterPostBattleEnslave+1] =
function (context)
	if Impassionate1_impl(context) then
		effect.trait("Impassionate", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Upgrade_Impassionate1 ]]--

function Upgrade_Impassionate1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Impassionate")

end 

events.CharacterPostBattleEnslave[#events.CharacterPostBattleEnslave+1] =
function (context)
	if Upgrade_Impassionate1_impl(context) then
		effect.trait("Impassionate", "agent", 1, 10, context)
		return true
	end
	return false
end


--[[ Incapable_Administrator1 ]]--

function Incapable_Administrator1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():town_wealth_growth() < 0 and  context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Incapable_Administrator1_impl(context) then
		effect.trait("Incapable_Administrator", "agent", 1, 5, context)
		effect.trait("Ineffective_Taxman", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Upgrade_Incapable_Administrator1 ]]--

function Upgrade_Incapable_Administrator1_impl (context)
		return  char_is_governor(context:character()) and context:character():region():town_wealth_growth() < 0 and  context:character():has_trait("Incapable_Administrator")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Incapable_Administrator1_impl(context) then
		effect.trait("Incapable_Administrator", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Inconsistent1 ]]--

function Inconsistent1_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Pious") and context:character():has_trait("Close_Minded")
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Inconsistent1_impl(context) then
		effect.trait("Inconsistent", "agent", 1, 1, context)
		return true
	end
	return false
end	

--[[ Inconsistent2 ]]--

function Inconsistent2_impl (context)
        return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library" and context:character():has_trait("Pious")

end

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Inconsistent2_impl(context) then
		effect.trait("Inconsistent", "agent", 1, 10, context)
		return true
	end
	return false
end	

--[[ Upgrade_Inconsistent1 ]]--

function Upgrade_Inconsistent1_impl (context)
        return context:character():has_trait("Inconsistent")

end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Inconsistent1_impl(context) then
		effect.trait("Inconsistent", "agent", 1, 4, context)
		return true
	end
	return false
end	

--[[ Upgrade_Ineffective_Taxman1 ]]--

function Upgrade_Ineffective_Taxman1_impl (context)
		return  char_is_governor(context:character()) and context:character():region():town_wealth_growth() < 0 and  context:character():has_trait("Ineffective_Taxman")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Ineffective_Taxman1_impl(context) then
		effect.trait("Ineffective_Taxman", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Indecisive1 ]]--

function Indecisive1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():action_points_remaining_percent() >= 90 and context:character():age() > 16
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Indecisive1_impl(context) then
		effect.trait("Indecisive", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Upgrade_Indecisive1 ]]--

function Upgrade_Indecisive1_impl (context)
		return  char_is_general_with_army(context:character()) and  context:character():has_trait("Indecisive") and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Indecisive1_impl(context) then
		effect.trait("Indecisive", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Industrious1 ]]--

function Industrious1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "Res_Superchain_Amber" or context:building():superchain() == "Res_Superchain_Copper" or context:building():superchain() == "Res_Superchain_Dyes" or context:building():superchain() == "Res_Superchain_Elephants" or context:building():superchain() == "Res_Superchain_Marble" or context:building():superchain() == "Res_Superchain_Silk") and context:character():number_of_traits() <10

end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Industrious1_impl(context) then
		effect.trait("Industrious", "agent", 1, 12, context)
		effect.trait("Inventive", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Upgrade_Industrious1 ]]--

function Upgrade_Industrious1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "Res_Superchain_Amber" or context:building():superchain() == "Res_Superchain_Copper" or context:building():superchain() == "Res_Superchain_Dyes" or context:building():superchain() == "Res_Superchain_Elephants" or context:building():superchain() == "Res_Superchain_Marble" or context:building():superchain() == "Res_Superchain_Silk") and context:character():has_trait("Industrious")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Upgrade_Industrious1_impl(context) then
		effect.trait("Industrious", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Upgrade_Inventive1 ]]--

function Upgrade_Inventive1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "Res_Superchain_Amber" or context:building():superchain() == "Res_Superchain_Copper" or context:building():superchain() == "Res_Superchain_Dyes" or context:building():superchain() == "Res_Superchain_Elephants" or context:building():superchain() == "Res_Superchain_Marble" or context:building():superchain() == "Res_Superchain_Silk") and  context:character():has_trait("Inventive")

end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Upgrade_Inventive1_impl(context) then
		effect.trait("Inventive", "agent", 1, 6, context)
		return true
	end
	return false
end

--[[ Infertile1 ]]--

function Infertile1_impl (context)
		return char_is_general(context:character()) and context:character():age() > 45 and context:character():model():turn_number() > 3

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Infertile1_impl(context) then
		effect.trait("Infertile", "agent", 1, 5, context)
		effect.trait("Insane", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Upgrade_Infertile1 ]]--

function Upgrade_Infertile1_impl (context)
		return   char_is_general(context:character()) and context:character():age() > 55 and context:character():has_trait("Infertile")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Infertile1_impl(context) then
		effect.trait("Infertile", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Insane1 ]]--

function Insane1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Good_Personal_Security") and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Insane1_impl(context) then
		effect.trait("Insane", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Insane2 ]]--

function Insane2_impl (context)
		return char_is_general(context:character()) and context:character():age() > 16

end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if Insane2_impl(context) then
		effect.trait("Insane", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ UpgradeInsane1 ]]--

function UpgradeInsane1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Insane") and context:character():number_of_traits() <10

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if UpgradeInsane1_impl(context) then
		effect.trait("Insane", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ UpgradeInsane2 ]]--

function UpgradeInsane2_impl (context)
		return char_is_general(context:character()) and context:character():is_faction_leader() and context:character():has_trait("Insane")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if UpgradeInsane2_impl(context) then
		effect.trait("Insane", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Irritable1 ]]--

function Irritable1_impl (context)
		return char_is_general(context:character()) and context:character():age() > 45 and context:character():model():turn_number() > 3
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Irritable1_impl(context) then
		effect.trait("Irritable", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Just1 ]]--

function Just1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >= 80.0 and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Just1_impl(context) then
		effect.trait("Just", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Upgrade_Just1 ]]--

function Upgrade_Just1_impl (context)
		return    char_is_governor(context:character()) and context:character():region():public_order() >= 80.0 and  context:character():has_trait("Just")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Just1_impl(context) then
		effect.trait("Just", "agent", 1, 10, context)
		return true
	end
	return false
end


--[[ Kind1 ]]--

function Kind1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10

end 

events.CharacterPostBattleRelease[#events.CharacterPostBattleRelease+1] =
function (context)
	if Kind1_impl(context) then
		effect.trait("Kind", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Upgrade_Kind1 ]]--

function Upgrade_Kind1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Kind")

end 

events.CharacterPostBattleRelease[#events.CharacterPostBattleRelease+1] =
function (context)
	if Upgrade_Kind1_impl(context) then
		effect.trait("Kind", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Landlubber1 ]]--

function Landlubber1_impl (context)
		return char_is_general_with_navy(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false and context:pending_battle():attacker_battle_result() == "crushing_defeat"
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Landlubber1_impl(context) then
		effect.trait("Landlubber1", "agent", 1, 50, context)
		return true
	end
	return false
end


--[[ Likes_Entertainment1 ]]--

function  Likes_Entertainment1_impl (context)
	return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_entertainment" and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16 
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  Likes_Entertainment1_impl(context) then
		effect.trait("Likes_Entertainment", "agent", 1, 2, context)
	end
	return false
end

--[[ Upgrade_Likes_Entertainment1 ]]--

function  Upgrade_Likes_Entertainment1_impl (context)
	return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_entertainment" and context:character():faction():culture() ~= "rom_Barbarian" and context:character():has_trait("Likes_Entertainment") and context:character():age() > 16 
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if  Upgrade_Likes_Entertainment1_impl(context) then
		effect.trait("Likes_Entertainment", "agent", 1, 5, context)
	end
	return false
end

--[[ Logistical_Skill1 ]]--

function  Logistical_Skill1_impl (context)
	return char_is_general(context:character()) and context:character():number_of_traits() <4 and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if  Logistical_Skill1_impl(context) then
		effect.trait("Logistical_Skill", "agent", 1,5, context)
	end
	return false
end

--[[ LogisticalSkill2 ]]--

function LogisticalSkill2_impl (context)
        return char_is_general_with_army(context:character()) and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian"
end

events.CharacterBesiegesSettlement[#events.CharacterBesiegesSettlement+1] =
function (context)
    if LogisticalSkill2_impl(context) then
	    effect.trait("Logistical_Skill", "agent", 1, 5, context)
		return true
	end
	return false
end	

--[[ LogisticalSkill3 ]]--

function LogisticalSkill3_impl (context)
        return (char_is_general(context:character()) and (context:character():has_trait("Mathematician") or context:character():has_trait("Intelligent")) and context:character():faction():culture() ~= "rom_Barbarian") and context:character():age() > 16
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if LogisticalSkill3_impl(context) then
	    effect.trait("Logistical_Skill", "agent", 1, 10, context)
		return true
	end
	return false
end	

--[[ Upgrade_LogisticalSkill1 ]]--

function LogisticalSkill1_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Logistical_Skill")

end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if LogisticalSkill1_impl(context) then
	    effect.trait("Logistical_Skill", "agent", 1, 4, context)
		return true
	end
	return false
end	

--[[ LostEagle1 ]]--

function LostEagle1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false and context:pending_battle():attacker_battle_result() == "crushing_defeat"  and context:character():faction():culture() == "rom_Roman"
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if LostEagle1_impl(context) then
	    effect.trait("Lost_Eagle", "agent", 1, 60, context)
		return true
	end
	return false
end		

--[[ LostEagle2 ]]--

function LostEagle2_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false and context:pending_battle():attacker_battle_result() == "decisive_defeat" and context:character():faction():culture() == "rom_Roman"
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if LostEagle2_impl(context) then
	    effect.trait("Lost_Eagle", "agent", 1, 40, context)
		return true
	end
	return false
end

--[[ Loyal1 ]]--

function Loyal1_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():number_of_traits() <8
end 

events.CharacterCandidateBecomesMinister[#events.CharacterCandidateBecomesMinister+1] =
function (context)
	if Loyal1_impl(context) then
		effect.trait("Loyal", "agent", 1, 8, context)
		effect.trait("Disloyal", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Loyal2 ]]--

function Loyal2_impl (context)
		return char_is_general(context:character()) and context:character():age() > 16
end 

events.DiplomaticOfferRejected[#events.DiplomaticOfferRejected+1] =
function (context)
	if Loyal2_impl(context) then
		effect.trait("Loyal", "agent", 1, 1, context)
		effect.trait("Disloyal", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Upgrade_Loyal1 ]]--

function Upgrade_Loyal1_impl (context)
		return context:character():has_trait("Loyal")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Loyal1_impl(context) then
		effect.trait("Loyal", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Mathematician1 ]]--

function Mathematician1_impl (context)
        return char_is_general(context:character()) and context:character():has_trait("Intelligent") and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16

end

events.CharacterFactionCompletesResearch[#events.CharacterFactionCompletesResearch+1] =
function (context)
    if Mathematician1_impl(context) then 
	    effect.trait("Mathematician", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Mathematician2 ]]--

function Mathematician2_impl (context)
        return char_is_general(context:character()) and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16

end

events.CharacterFactionCompletesResearch[#events.CharacterFactionCompletesResearch+1] =
function (context)
    if Mathematician2_impl(context) then 
	    effect.trait("Mathematician", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Upgrade_Motivated1 ]]--

function Upgrade_Motivated1_impl (context)
        return context:character():has_trait("Motivated")

end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Upgrade_Motivated1_impl(context) then
	    effect.trait("Motivated", "agent", 1, 6, context)
		return true
	end
	return false
end

		
--[[ Narcicisst1 ]]--

function Narcicisst1_impl (context)
		return char_is_general(context:character()) and char_rank_between(context:character(), 2, 9) and context:character():number_of_traits() <10

end 

events.CharacterRankUp[#events.CharacterRankUp+1] =
function (context)
	if Narcicisst1_impl(context) then
		effect.trait("Narcicisst", "agent", 1, 10, context)
		return true
	end
	return false
end
--[[ Upgrade_Narcicisst1 ]]--

function Upgrade_Narcicisst1_impl (context)
		return   char_is_general(context:character()) and  context:character():has_trait("Narcicisst")

end 

events.CharacterRankUp[#events.CharacterRankUp+1] =
function (context)
	if Upgrade_Narcicisst1_impl(context) then
		effect.trait("Narcicisst", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Natural_Military_Skill_Upgrade ]]--

function  Natural_Military_Skill_Upgrade_impl (context)
	return char_is_general_with_army(context:character()) and (context:pending_battle():attacker() == context:character() or context:pending_battle():defender() == context:character()) and context:character():won_battle() and context:character():has_trait("Natural_Military_Skill")
end

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if  Natural_Military_Skill_Upgrade_impl(context) then
		effect.trait("Natural_Military_Skill", "agent", 1, 40, context)
		return true
	end
	return false
end

--[[ Noble_Patron1 ]]--

function Noble_Patron1_impl (context)
        return (char_is_general(context:character()) and context:character():faction():culture() == "rom_Eastern")

end

events.CharacterEntersGarrison[#events.CharacterEntersGarrison+1] =
function (context)
    if Noble_Patron1_impl(context) then
	    effect.trait("Noble_Patron", "agent", 1, 10, context)
		return true
	end
	return false
end	

--[[ Non_Authoritarian1 ]]--

function Non_Authoritarian1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():faction():tax_level() < 20 and context:character():age() > 16

end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Non_Authoritarian1_impl(context) then
		effect.trait("Non_Authoritarian", "agent", 1, 5, context)
		effect.trait("Kind", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Non_Authoritarian2 ]]--

function Non_Authoritarian2_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >80 and context:character():age() > 16

end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Non_Authoritarian2_impl(context) then
		effect.trait("Non_Authoritarian", "agent", 1, 2, context)
		effect.trait("Kind", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Upgrade_Non_Authoritarian1 ]]--

function Upgrade_Non_Authoritarian1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():faction():tax_level() < 20 and context:character():has_trait("Non_Authoritarian")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Non_Authoritarian1_impl(context) then
		effect.trait("Non_Authoritarian", "agent", 1, 10, context)
		return true
	end
	return false
end
--[[ Upgrade_Kind1 ]]--

function Upgrade_Kind1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():faction():tax_level() < 20 and context:character():has_trait("Kind")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Kind1_impl(context) then
		effect.trait("Kind", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Nyctophilia1 ]]--

function Nyctophilia1_impl (context)
		return char_is_general_with_army(context:character()) and (context:pending_battle():attacker() == context:character() or context:pending_battle():defender() == context:character()) and context:character():won_battle() and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Nyctophilia1_impl(context) then
		effect.trait("Nyctophilia", "agent", 1, 15, context)
		return true
	end
	return false
end
--[[ Upgrade_Nyctophilia1 ]]--

function Upgrade_Nyctophilia1_impl (context)
		return char_is_general_with_army(context:character()) and (context:pending_battle():attacker() == context:character() or context:pending_battle():defender() == context:character()) and context:character():won_battle() and context:pending_battle():night_battle() and context:character():has_trait("Nyctophilia")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Nyctophilia1_impl(context) then
		effect.trait("Nyctophilia", "agent", 1, 20, context)
		return true
	end
	return false
end

--[[ Nyctophobia1 ]]--

function Nyctophobia1_impl (context)
		return char_is_general_with_army(context:character()) and (context:pending_battle():attacker() == context:character() or context:pending_battle():defender() == context:character()) and not context:character():won_battle() and context:pending_battle():night_battle() and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Nyctophobia1_impl(context) then
		effect.trait("Nyctophobia", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Open_Minded1 ]]--

function Open_Minded1_impl (context)
		return char_is_general(context:character()) and context:character():is_politician() and not context:character():faction():at_war() and context:character():faction():is_human()
end 

events.CharacterRankUp[#events.CharacterRankUp+1] =
function (context)
	if Open_Minded1_impl(context) then
	    effect.trait("Open_Minded", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Optimates1 ]]-- 

function Optimates1_impl (context)
        return char_is_general(context:character()) and context:character():number_of_traits() <10 and context:character():faction():culture() == "rom_Roman" 
end

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
    if Optimates1_impl(context) then
	    effect.trait("Optimates", "agent", 1, 12, context)
		effect.trait("Populares", "agent", 1, 9, context)
		return true
	end
	return false
end

--[[ Passive1 ]]--

function Passive1_impl (context)
        return context:character():has_trait("Passive") and context:character():action_points_remaining_percent() >= 75
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if Passive1_impl(context) then 
	    effect.trait("Passive", "agent", 1, 5, context)
		return true
	end
	return false
end	

--[[ Persecutor1 ]]--

function Persecutor1_impl (context)
        return char_is_general(context:character()) and context:character():has_region() and (context:character():region():majority_religion() ~= "rom_religion_latin") and context:character():model():turn_number() > 2 and context:character():faction():culture() == "rom_Roman" and context:character():age() > 16
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Persecutor1_impl(context) then
	    effect.trait("Persecutor", "agent", 1, 2, context)
		return true
	end
	return false
end	

--[[ Persecutor2 ]]--

function Persecutor2_impl (context)
        return char_is_general(context:character()) and context:character():has_region() and (context:character():region():majority_religion() ~= "rom_religion_hellenistic" or context:character():region():majority_religion() ~= "rom_religion_hellenic" or context:character():region():majority_religion() ~= "rom_religion_egyptian") and context:character():model():turn_number() > 2 and context:character():faction():culture() == "rom_Hellenistic" and context:character():age() > 16
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Persecutor2_impl(context) then
	    effect.trait("Persecutor", "agent", 1, 2, context)
		return true
	end
	return false
end	

--[[ Persecutor3 ]]--

function Persecutor3_impl (context)
        return char_is_general(context:character()) and context:character():has_region() and (context:character():region():majority_religion() ~= "rom_religion_eastern" or context:character():region():majority_religion() ~= "rom_religion_parthian" or context:character():region():majority_religion() ~= "rom_religion_armenian" or context:character():region():majority_religion() ~= "rom_religion_persian") and context:character():model():turn_number() > 1 and (context:character():faction():subculture() == "sc_rom_armenian" or context:character():faction():subculture() == "sc_rom_parthian" or context:character():faction():subculture() == "sc_rom_eastern") and context:character():model():turn_number() > 2 and context:character():age() > 16
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Persecutor3_impl(context) then
	    effect.trait("Persecutor", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Philanthropist1 ]]--

function Philanthropist1_impl (context)
		return char_is_general(context:character())
end 

events.CharacterPostBattleRelease[#events.CharacterPostBattleRelease+1] =
function (context)
	if Philanthropist1_impl(context) then
		effect.trait("Philanthropist", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Philanthropist2 ]]--

function Philanthropist2_impl (context)
		return context:character():has_region() and char_is_governor(context:character()) and context:character():region():public_order() >= 90.0 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Philanthropist2_impl(context) then
	    effect.trait("Philanthropist", "agent", 1, 10, context)
		return true
	end
	return false
end	

--[[ Philosopher1 ]]--

function Philosopher1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterFactionCompletesResearch[#events.CharacterFactionCompletesResearch+1] =
function (context)
	if Philosopher1_impl(context) then
		effect.trait("Philosopher", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Philosopher2 ]]--

function Philosopher2_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library" and context:character():age() > 16

end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Philosopher2_impl(context) then
		effect.trait("Philosopher", "agent", 1, 8, context)
		return true
	end
	return false
end


--[[ Upgrade_Philosopher1 ]]--

function Upgrade_Philosopher1_impl (context)
		return   char_is_general(context:character()) and context:character():has_trait("Philosopher")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Philosopher1_impl(context) then
		effect.trait("Philosopher", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Pillager1 ]]--

function Pillager1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10 and context:character():faction():culture() == "rom_Barbarian"

end 

events.CharacterLootedSettlement[#events.CharacterLootedSettlement+1] =
function (context)
	if Pillager1_impl(context) then
		effect.trait("Pillager", "agent", 1, 20, context)
		effect.trait("Savage", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Upgrade_Pillager1 ]]--

function Upgrade_Pillager1_impl (context)
		return   char_is_general(context:character()) and context:character():has_trait("Pillager")

end 

events.CharacterLootedSettlement[#events.CharacterLootedSettlement+1] =
function (context)
	if Upgrade_Pillager1_impl(context) then
		effect.trait("Pillager", "agent", 1, 20, context)
		return true
	end
	return false
end
--[[ Upgrade_Savage1 ]]--

function Upgrade_Savage1_impl (context)
		return   char_is_general(context:character()) and context:character():has_trait("Savage")

end 

events.CharacterLootedSettlement[#events.CharacterLootedSettlement+1] =
function (context)
	if Upgrade_Savage1_impl(context) then
		effect.trait("Savage", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Playwright1 ]]--

function Playwright1_impl (context)
		return  char_is_general(context:character()) and context:character():number_of_traits() <6 and context:character():faction():culture() ~= "rom_Barbarian" and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Playwright1_impl(context) then
        effect.trait("Playwright", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Political_Talent1 ]]--

function Political_Talent1_impl (context)
		return char_is_general(context:character()) and context:character():is_politician() and context:character():faction():culture() ~= "rom_Barbarian"

end 

events.CharacterRankUp[#events.CharacterRankUp+1] =
function (context)
	if Political_Talent1_impl(context) then
        effect.trait("Orator", "agent", 1, 2, context)
        effect.trait("Political_Schemer", "agent", 1, 1, context)
		effect.trait("Political_Talent", "agent", 1, 2, context)
		effect.trait("Political_Underdog", "agent", 1, 1, context)		
		effect.trait("Secretive", "agent", 1, 2, context)
		effect.trait("Witty", "agent", 1, 2, context)
		effect.trait("Unamusing", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Orator1 ]]--

function Upgrade_Orator1_impl (context)
		return   char_is_politician(context:character()) and context:character():has_trait("Orator")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Orator1_impl(context) then
		effect.trait("Orator", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Political_Schemer1 ]]--

function Upgrade_Political_Schemer1_impl (context)
		return   char_is_politician(context:character()) and context:character():has_trait("Political_Schemer")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if UUpgrade_Political_Schemer1_impl(context) then
		effect.trait("Orator", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Political_Talent1 ]]--

function Upgrade_Political_Talent1_impl (context)
		return   char_is_politician(context:character()) and context:character():has_trait("Political_Talent")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Political_Talent1_impl(context) then
		effect.trait("Political_Talent", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Secretive1 ]]--

function Upgrade_Secretive1_impl (context)
		return   char_is_politician(context:character()) and context:character():has_trait("Secretive")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Secretive1_impl(context) then
		effect.trait("Secretive", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Upgrade_Unamusive1 ]]--

function Upgrade_Unamusive1_impl (context)
		return   char_is_politician(context:character()) and context:character():has_trait("Unamusive")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Unamusive1_impl(context) then
		effect.trait("Unamusive", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Upgrade_Witty1 ]]--

function Upgrade_Witty1_impl (context)
		return   char_is_general(context:character()) and char_is_politician(context:character()) and context:character():has_trait("Witty")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Witty1_impl(context) then
		effect.trait("Witty", "agent", 1, 10, context)
		return true
	end
	return false
end


--[[ Poor_Attacker1 ]]--

function Poor_Attacker1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Poor_Attacker1_impl(context) then
		effect.trait("Poor_Attacker", "agent", 1, 30, context)
		return true
	end
	return false
end
--[[ Upgrade_Poor_Attacker1 ]]--

function Upgrade_Poor_Attacker1_impl (context)
		return   char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == false and context:character():has_trait("Poor_Attacker")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Poor_Attacker1_impl(context) then
		effect.trait("Poor_Attacker", "agent", 1, 80, context)
		return true
	end
	return false
end


--[[ Poor_Defender1 ]]--

function Poor_Defender1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() == false and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Poor_Defender1_impl(context) then
		effect.trait("Poor_Defender", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ Upgrade_Poor_Defender1 ]]--

function Upgrade_Poor_Defender1_impl (context)
		return   char_is_general_with_army(context:character()) and context:pending_battle():defender() == context:character() and context:character():won_battle() == false and context:character():has_trait("Poor_Defender")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Poor_Defender1_impl(context) then
		effect.trait("Poor_Defender", "agent", 1, 80, context)
		return true
	end
	return false
end

--[[ Poor_Trader1 ]]--

function Poor_Trader1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():faction():trade_value_percent() < 25 and context:character():model():turn_number() > 5 and context:character():number_of_traits() <10 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Poor_Trader1_impl(context) then
		effect.trait("Poor_Trader", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Upgrade_Poor_Trader1 ]]--

function Upgrade_Poor_Trader1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Poor_Trader")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Poor_Trader1_impl(context) then
		effect.trait("Poor_Trader", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Possesive1 ]]--

function Possesive1_impl (context)
		return context:character():is_faction_leader() and context:character():faction():treasury() > 25000 and context:character():faction():is_human() and context:character():model():turn_number() > 10 and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Possesive1_impl(context) then
		effect.trait("Possessive", "agent", 1, 3, context)
		return true
	end
	return false
end
--[[ Upgrade_Possesive1 ]]--

function Upgrade_Possesive1_impl (context)
		return context:character():is_faction_leader() and context:character():faction():is_human() and context:character():has_trait("Possessive")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Possesive1_impl(context) then
		effect.trait("Possessive", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Predominant1 ]]--

function Predominant1_impl (context)
		return char_is_general(context:character()) and char_rank_between(context:character(), 5, 9) and context:character():has_trait("Pious") and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Predominant1_impl(context) then
		effect.trait("Predominant", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Predominant2 ]]--

function Predominant2_impl (context)
		return char_is_general(context:character()) and (context:character():has_trait("Authoritarian") or context:character():has_trait("confrontational")) and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Predominant2_impl(context) then
		effect.trait("Predominant", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Predominant1 ]]--

function Upgrade_Predominant1_impl (context)
		return char_is_general(context:character()) and context:character():faction():is_human() and context:character():has_trait("Predominant")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Predominant1_impl(context) then
		effect.trait("Predominant", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Rash1 ]]--

function Rash1_impl (context)
		return char_is_general(context:character()) and char_rank_between(context:character(), 4, 9) and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Rash1_impl(context) then
		effect.trait("Rash", "agent", 1, 2, context)
		effect.trait("Superficial", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Rash1 ]]--

function Upgrade_Rash1_impl (context)
		return char_is_general(context:character()) and context:character():faction():is_human() and context:character():has_trait("Rash")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Rash1_impl(context) then
		effect.trait("Rash", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Rational1 ]]--

function Upgrade_Rational1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Rational")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Rational1_impl(context) then
		effect.trait("Rational", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Prudent1 ]]--

function Prudent1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():faction():culture() ~= "rom_Hellenistic" and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Prudent1_impl(context) then
		effect.trait("Prudent", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Upgrade_Prudent1 ]]--

function Upgrade_Prudent1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Prudent")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Prudent1_impl(context) then
		effect.trait("Prudent", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Superficial1 ]]--

function Upgrade_Superficial1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Superficial")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Superficial1_impl(context) then
		effect.trait("Superficial", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Religious_Mania1 ]]--

function Religious_Mania1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Pious") and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Religious_Mania1_impl(context) then
		effect.trait("Religious_Mania", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Religious_Mania1 ]]--

function Upgrade_Religious_Mania1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Religious_Mania")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Religious_Mania1_impl(context) then
		effect.trait("Religious_Mania", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ RetainedEagle1 ]]--

function RetainedEagle1_impl (context)
		return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:pending_battle():attacker_battle_result() == "decisive_victory" and context:character():has_trait("Lost_Eagle") and not context:character():faction():culture() == "rom_Roman"
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if RetainedEagle1_impl(context) then
	    effect.trait("Retained_Eagle", "agent", 3, 100, context)
		return true
	end
	return false
end		

--[[ Retiring1 ]]--

function Retiring1_impl (context)
		return char_is_general(context:character()) and context:character():age() > 59 

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Retiring1_impl(context) then
		effect.trait("Retiring", "agent", 1, 10, context)
		effect.trait("Passive", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ RomanHero1 ]]--

function RomanHero1_impl (context)
        return (char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():faction():culture() == "rom_Roman")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if RomanHero1_impl (context) then
	    effect.trait("Roman_Hero", "agent", 1, 30, context)
		return true
	end
	return false
end	

--[[ Upgrade_RomanHero1 ]]--

function Upgrade_RomanHero1_impl (context)
        return char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():has_trait("Roman_Hero")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
    if Upgrade_RomanHero1_impl (context) then
	    effect.trait("Roman_Hero", "agent", 1, 50, context)
		return true
	end
	return false
end	

--[[ Satirist1 ]]--

function Satirist1_impl (context)
		return char_is_general(context:character()) and context:character():is_politician() and context:character():faction():at_war() and context:character():age() > 16
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Satirist1_impl(context) then
		effect.trait("Satirist", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Satirist2 ]]--

function Satirist2_impl (context)
		return char_is_general(context:character()) and context:character():faction():at_war() and context:character():age() > 16 
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Satirist2_impl(context) then
		effect.trait("Satirist", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Satirist1 ]]--

function Upgrade_Satirist1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Satirist")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Satirist1_impl(context) then
		effect.trait("Satirist", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Seafarer1 ]]--

function Seafarer1_impl (context)
		return char_is_general_with_navy(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Seafarer1_impl(context) then
		effect.trait("Seafarer", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Seafarer1 ]]--

function Upgrade_Seafarer1_impl (context)
		return char_is_general_with_navy(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() == true and context:character():has_trait("Seafarer")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Seafarer1_impl(context) then
		effect.trait("Seafarer", "agent", 1, 25, context)
		return true
	end
	return false
end


--[[ Sharp1 ]]--

function Sharp1_impl (context)
		return char_is_general_with_army(context:character()) and (context:pending_battle():attacker() == context:character() or context:pending_battle():defender() == context:character()) and context:pending_battle():ambush_battle() and context:character():number_of_traits() <10

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Sharp1_impl(context) then
		effect.trait("Sharp", "agent", 1, 10, context)
		return true
	end
	return false
end
--[[ Upgrade_Sharp1 ]]--

function Upgrade_Sharp1_impl (context)
		return char_is_general_with_army(context:character()) and (context:pending_battle():attacker() == context:character() or context:pending_battle():defender() == context:character()) and context:pending_battle():ambush_battle() and context:character():has_trait("Sharp")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Sharp1_impl(context) then
		effect.trait("Sharp", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Skeptic1 ]]--

function Upgrade_Skeptic1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Skeptic")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Skeptic1_impl(context) then
		effect.trait("Skeptic", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Slothful1 ]]--

function Slothful1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():age() > 50 

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Slothful1_impl(context) then
		effect.trait("Slothful", "agent", 1, 2, context)
		effect.trait("Unhurried", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Slothful1 ]]--

function Upgrade_Slothful1_impl (context)
		return char_is_governor(context:character()) and context:character():age() > 50 and context:character():has_trait("Slothful")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Slothful1_impl(context) then
		effect.trait("Slothful", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Upgrade_Unhurried1 ]]--

function Upgrade_Unhurried1_impl (context)
		return char_is_governor(context:character()) and context:character():age() > 50 and context:character():has_trait("Unhurried")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Unhurried1_impl(context) then
		effect.trait("Unhurried", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Sober1 ]]--

function Sober1_impl (context)
		return char_is_general(context:character()) and context:character():age() > 16

end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if Sober1_impl(context) then
		effect.trait("Sober", "agent", 1, 20, context)
		return true
	end
	return false
end

--[[ Sober2 ]]--

function Sober2_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Drinker")

end 

events.CharacterLeavesGarrison[#events.CharacterLeavesGarrison+1] =
function (context)
	if Sober2_impl(context) then
		effect.trait("Sober", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Upgrade_Sober1 ]]--

function Upgrade_Sober1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Sober")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Sober1_impl(context) then
		effect.trait("Sober", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Stingy1 ]]--

function Stingy1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():is_faction_leader() and context:character():faction():treasury() > 1000000 and context:character():faction():is_human() and context:character():model():turn_number() > 10 and context:character():number_of_traits() <10 and context:character():age() > 16

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Stingy1_impl(context) then
		effect.trait("Stingy", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Upgrade_Stingy1 ]]--

function Upgrade_Stingy1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Stingy")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Stingy1_impl(context) then
		effect.trait("Stingy", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Stoic1 ]]--

function Stoic1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian"

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Stoic1_impl(context) then
		effect.trait("Stoic", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Stoic1 ]]--

function Upgrade_Stoic1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Stoic")

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Upgrade_Stoic1_impl(context) then
		effect.trait("Stoic", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Submissive1 ]]--

function Submissive1_impl (context)
		return char_is_general(context:character()) and context:character():number_of_traits() <10 and context:character():faction():culture() == "rom_Roman"
end 

events.CharacterMarriage[#events.CharacterMarriage+1] =
function (context)
	if Submissive1_impl(context) then
		effect.trait("Submissive", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Sunbaked1 ]]--

function Sunbaked1_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():faction():culture() ~= "rom_Eastern"

end 

events.CharacterEntersAttritionalArea[#events.CharacterEntersAttritionalArea+1] =
function (context)
	if Sunbaked1_impl(context) then
		effect.trait("Sunbaked", "agent", 1, 7, context)
		return true
	end
	return false
end

--[[ Sunbaked_Removed1 ]]--

function Sunbaked_Removed1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Sunbaked")

end 

events.CharacterEntersGarrison[#events.CharacterEntersGarrison+1] =
function (context)
	if Sunbaked_Removed1_impl(context) then
		effect.trait("Sunbaked_Removed", "agent", 1, 75, context)
		return true
	end
	return false
end

--[[ Traveler1 ]]--

function Traveler1_impl (context)
		return char_is_general(context:character()) and context:character():model():turn_number() > 8

end 

events.CharacterLeavesGarrison[#events.CharacterLeavesGarrison+1] =
function (context)
	if Traveler1_impl(context) then
		effect.trait("Traveler", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Trustful1 ]]--

function Trustful1_impl (context)
		return (char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 3 and context:character():in_settlement() and context:character():model():turn_number() > 2 and context:character():model():turn_number() % 5 == 0) and context:character():has_trait("Ignorant")

end 

events.CharacterEntersGarrison[#events.CharacterEntersGarrison+1] =
function (context)
	if Trustful1_impl(context) then
		effect.trait("Trustful", "agent", 1, 20, context)
		return true
	end
	return false
end

--[[ Trustful2 ]]--

function Trustful2_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 6 and context:character():in_settlement() and context:character():model():turn_number() > 3 and context:character():model():turn_number() % 5 == 0

end 

events.CharacterEntersGarrison[#events.CharacterEntersGarrison+1] =
function (context)
	if Trustful2_impl(context) then
		effect.trait("Trustful", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Trustful1 ]]--

function Upgrade_Trustful1_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 2 and context:character():in_settlement() and context:character():model():turn_number() > 1 and context:character():has_trait("Trustful")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Trustful1_impl(context) then
		effect.trait("Trustful", "agent", 1, 10, context)
		return true
	end
	return false
end

--[[ Typhoid_Fever1 ]]--

function Typhoid_Fever1_impl (context)
		return  char_is_general_with_army(context:character())

end 

events.CharacterEntersAttritionalArea[#events.CharacterEntersAttritionalArea+1] =
function (context)
	if Typhoid_Fever1_impl(context) then
		effect.trait("Typhoid_Fever", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Typhoid_Fever2 ]]--

function Typhoid_Fever2_impl (context)
		return  char_is_general_with_army(context:character())

end 

events.CharacterLeavesGarrison[#events.CharacterLeavesGarrison+1] =
function (context)
	if Typhoid_Fever2_impl(context) then
		effect.trait("Typhoid_Fever", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Typhoid_Fever_Removed1 ]]--

function Typhoid_Fever_Removed1_impl (context)
		return (char_is_general_with_army(context:character()) and context:character():has_trait("Typhoid_Fever"))

end 

events.CharacterEntersGarrison[#events.CharacterEntersGarrison+1] =
function (context)
	if Typhoid_Fever_Removed1_impl(context) then
		effect.trait("Typhoid_Fever_Removed", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Typhoid_Fever_Removed2 ]]--

function Typhoid_Fever_Removed2_impl (context)
		return (char_is_general_with_army(context:character()) and context:character():has_trait("Typhoid_Fever"))

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Typhoid_Fever_Removed2_impl(context) then
		effect.trait("Typhoid_Fever_Removed", "agent", 1, 40, context)
		return true
	end
	return false
end

--[[ Popular_Governor1 ]]--

function Popular_Governor1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >= 70.0 and context:character():age() > 16

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Popular_Governor1_impl(context) then
		effect.trait("Popular_Governor", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Popular_Governor2 ]]--

function Popular_Governor2_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >= 90.0 and context:character():age() > 16

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Popular_Governor2_impl(context) then
		effect.trait("Popular_Governor", "agent", 1, 6, context)
		return true
	end
	return false
end

--[[ Unpopular_Governor1 ]]--

function Unpopular_Governor1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() <= 1.0 and context:character():age() > 16

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Unpopular_Governor1_impl(context) then
		effect.trait("Unpopular_Governor", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Victor1 ]]--

function Victor1_impl (context)
		return char_is_general(context:character()) and context:character():won_battle() and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterPerformsOccupationDecisionOccupy[#events.CharacterPerformsOccupationDecisionOccupy+1] =
function (context)
	if Victor1_impl(context) then
		effect.trait("Victor", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ Victor2 ]]--

function Victor2_impl (context)
		return char_is_general(context:character()) and context:character():won_battle() and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterPerformsOccupationDecisionLoot[#events.CharacterPerformsOccupationDecisionLoot+1] =
function (context)
	if Victor2_impl(context) then
		effect.trait("Victor", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ Victor3 ]]--

function Victor3_impl (context)
		return char_is_general(context:character()) and context:character():won_battle() and context:character():number_of_traits() <10 and context:character():faction():culture() ~= "rom_Barbarian"
end 

events.CharacterPerformsOccupationDecisionSack[#events.CharacterPerformsOccupationDecisionSack+1] =
function (context)
	if Victor3_impl(context) then
		effect.trait("Victor", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Victor_Others1 ]]--

function Victor_Others1_impl (context)
		return char_is_general(context:character()) and context:character():won_battle() and context:character():number_of_traits() <10 and context:character():faction():culture() == "rom_Barbarian"

end 

events.CharacterPerformsOccupationDecisionOccupy[#events.CharacterPerformsOccupationDecisionOccupy+1] =
function (context)
	if Victor_Others1_impl(context) then
		effect.trait("Victor_Others", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ Victor_Others2 ]]--

function Victor_Others2_impl (context)
		return char_is_general(context:character()) and context:character():won_battle() and context:character():number_of_traits() <10 and context:character():faction():culture() == "rom_Barbarian"

end 

events.CharacterPerformsOccupationDecisionLoot[#events.CharacterPerformsOccupationDecisionLoot+1] =
function (context)
	if Victor_Others2_impl(context) then
		effect.trait("Victor_Others", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ Victor_Others3 ]]--

function Victor_Others3_impl (context)
		return char_is_general(context:character()) and context:character():won_battle() and context:character():number_of_traits() <10 and context:character():faction():culture() == "rom_Barbarian"

end 

events.CharacterPerformsOccupationDecisionSack[#events.CharacterPerformsOccupationDecisionSack+1] =
function (context)
	if Victor_Others3_impl(context) then
		effect.trait("Victor_Others", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ WarlordGenerated1 ]]--

function WarlordGenerated1_impl (context)
		return char_is_general(context:character()) and context:character():faction():culture() == "rom_Barbarian" and not context:character():has_trait("Warlord") and not (context:character():is_faction_leader() or (context:character():has_father() and context:character():father():has_trait("Chieftain"))) and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if WarlordGenerated1_impl(context) then
		effect.trait("Warlord", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ WarlordVictoryHeroic2 ]]--

function WarlordVictoryHeroic2_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Warlord") and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory" and context:character():faction():culture() == "rom_Barbarian"

end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if WarlordVictoryHeroic2_impl(context) then
		effect.trait("Warlord", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ WarlordVictoryDecisive3 ]]--

function WarlordVictoryDecisive3_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Warlord") and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "decisive_victory" and context:character():faction():culture() == "rom_Barbarian"
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if WarlordVictoryDecisive3_impl(context) then
		effect.trait("Warlord", "agent", 1, 75, context)
		return true
	end
	return false
end

--[[ WarlordVictoryClose4 ]]--

function WarlordVictoryClose4_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Warlord") and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "close_victory" and context:character():faction():culture() == "rom_Barbarian"
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if WarlordVictoryClose4_impl(context) then
		effect.trait("Warlord", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ WarlordInfluenceLow5 ]]--

function WarlordInfluenceLow5_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Warlord") and context:character():gravitas() > 50 and context:character():gravitas() < 100 and context:character():faction():culture() == "rom_Barbarian"
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if WarlordInfluenceLow5_impl(context) then
		effect.trait("Warlord", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ WarlordInfluenceAverage6 ]]--

function WarlordInfluenceAverage6_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Warlord") and context:character():gravitas() > 100 and context:character():gravitas() < 200 and context:character():faction():culture() == "rom_Barbarian"
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if WarlordInfluenceAverage6_impl(context) then
		effect.trait("Warlord", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ WarlordInfluenceHigh7 ]]--

function WarlordInfluenceHigh7_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Warlord") and context:character():gravitas() > 200 and context:character():faction():culture() == "rom_Barbarian"

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if WarlordInfluenceHigh7_impl(context) then
		effect.trait("Warlord", "agent", 1, 40, context)
		return true
	end
	return false
end

--[[ WarlordArmyLarge8 ]]--

function WarlordArmyLarge8_impl (context)
		return char_is_general_with_army(context:character()) and context:character():has_trait("Warlord") and context:character():trait_level("Warlord") == 3 and context:character():is_carrying_troops():unit_count() >= 15 and context:character():faction():culture() == "rom_Barbarian"

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if WarlordArmyLarge8_impl(context) then
		effect.trait("Warlord", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ UpgradeAnalytical1 ]]--

function UpgradeAnalytical1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Analytical") 
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if UpgradeAnalytical1_impl(context) then
		effect.trait("Analytical", "agent", 1, 3, context)
		return true
	end
	return false
end


--[[ Upgrade_Approachable1 ]]--

function Upgrade_Approachable1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Approachable")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Approachable1_impl(context) then
		effect.trait("Approachable", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Austere1 ]]--

function Austere1_impl (context)
		return char_is_general(context:character()) and context:character():faction():culture() ~= "Barbarian" and context:character():faction():culture() ~= "Eastern" and context:character():faction():culture() ~= "Roman" and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Austere1_impl(context) then
		effect.trait("Austere", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Austere1 ]]--

function Upgrade_Austere1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Austere")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Austere1_impl(context) then
		effect.trait("Austere", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Upgrade_Changeable1 ]]--

function Upgrade_Changeable1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Changeable")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Changeable1_impl(context) then
		effect.trait("Changeable", "agent", 1, 5, context)
		return true
	end
	return false
end


--[[ Upgrade_Cheeky1 ]]--

function Upgrade_Cheeky1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Cheeky")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Cheeky1_impl(context) then
		effect.trait("Cheeky", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Diplomatic1 ]]--

function Upgrade_Diplomatic1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Diplomatic")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Diplomatic1_impl(context) then
		effect.trait("Diplomatic", "agent", 1, 15, context)
		return true
	end
	return false
end

--[[ Upgrade_Disorderly1 ]]--

function Upgrade_Disorderly1_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Disorderly")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Disorderly1_impl(context) then
		effect.trait("Disorderly", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Energetic1 ]]--

function Upgrade_Energetic1_impl (context)
		return  char_is_general_with_army(context:character())  and context:character():has_trait("Energetic")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Energetic1_impl(context) then
		effect.trait("Energetic", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Fainthearted1 ]]--

function Upgrade_Fainthearted1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Fainthearted")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Fainthearted1_impl(context) then
		effect.trait("Fainthearted", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Upgrade_Fertile1 ]]--

function Upgrade_Fertile1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Fertile")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Fertile1_impl(context) then
		effect.trait("Fertile", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Friendly1 ]]--

function Upgrade_Friendly1_impl (context)
		return  char_is_general(context:character())  and context:character():has_trait("Friendly")

end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Upgrade_Friendly1_impl(context) then
		effect.trait("Friendly", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Gambler1 ]]--

function Upgrade_Gambler1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Gambler")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Gambler1_impl(context) then
		effect.trait("Gambler", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Healthy1 ]]--

function Upgrade_Healthy1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Healthy")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Healthy1_impl(context) then
		effect.trait("Healthy", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Impatient1 ]]--

function Upgrade_Impatient1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Impatient")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Impatient1_impl(context) then
		effect.trait("Impatient", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Nagger1 ]]--

function Upgrade_Nagger1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Nagger")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Nagger1_impl(context) then
		effect.trait("Nagger", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Optimist1 ]]--

function Upgrade_Optimist1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Optimist")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Optimist1_impl(context) then
		effect.trait("Optimist", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Patient1 ]]--

function Upgrade_Patient1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Patient")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Patient1_impl(context) then
		effect.trait("Patient", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Pragmatic1 ]]--

function Upgrade_Pragmatic1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Pragmatic")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Pragmatic1_impl(context) then
		effect.trait("Pragmatic", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Rational1 ]]--

function Upgrade_Rational1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Rational")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Rational1_impl(context) then
		effect.trait("Rational", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Socially_Awkward1 ]]--

function Socially_Awkward1_impl (context) 
        return char_is_general(context:character()) and context:character():is_politician()

end
		
events.CharacterCandidateBecomesMinister[#events.CharacterCandidateBecomesMinister+1] =
function (context)
    if Socially_Awkward1_impl(context) then
	    effect.trait("Socially_Awkward", "agent", 1, 5, context)
		effect.trait("Charismatic", "agent", 1, 5, context)
		return true
	end
	return false
end	

--[[ Socially_Awkward2 ]]--

function Socially_Awkward2_impl (context) 
        return char_is_governor(context:character()) and context:character():age() < 30

end
		
events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Socially_Awkward2_impl(context) then
	    effect.trait("Socially_Awkward", "agent", 1, 1, context)
		return true
	end
	return false
end	

--[[ Upgrade_Socially_Awkward1 ]]--

function Upgrade_Socially_Awkward1_impl (context) 
        return context:character():has_trait("Socially_Awkward")

end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
    if Upgrade_Socially_Awkward1_impl(context) then
	    effect.trait("Socially_Awkward", "agent", 1, 1, context)
		return true
	end
	return false
end	

--[[ Upgrade_Straightforward1 ]]--

function Upgrade_Straightforward1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Straightforward")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Straightforward1_impl(context) then
		effect.trait("Straightforward", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Submissive1 ]]--

function Upgrade_Submissive1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Submissive")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Submissive1_impl(context) then
		effect.trait("Submissive", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Upgrade_Unfriendly1 ]]--

function Upgrade_Unfriendly1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Unfriendly")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Unfriendly1_impl(context) then
		effect.trait("Unfriendly", "agent", 1, 5, context)
		return true
	end
	return false
end
--[[ Upgrade_Upright1 ]]--

function Upgrade_Upright1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Upright")

end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Upgrade_Upright1_impl(context) then
		effect.trait("Upright", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Analytical1 ]]--

function Father_HasTrait_Analytical1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Analytical")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Analytical1_impl(context) then
		effect.trait("Analytical", "agent", 1, 30, context)
		effect.trait("Analytical", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Approachable1 ]]--

function Father_HasTrait_Approachable1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Approachable")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Approachable1_impl(context) then
		effect.trait("Approachable", "agent", 1, 20, context)
		effect.trait("Approachable", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_ArabMerchant1 ]]--

function Father_HasTrait_ArabMerchant1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Arab_Merchant")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_ArabMerchant1_impl(context) then
		effect.trait("Arab_Merchant", "agent", 1, 30, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Architect1 ]]--

function Father_HasTrait_Architect1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Architect")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Architect1_impl(context) then
		effect.trait("Architect", "agent", 2, 35, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Austere1 ]]--

function Father_HasTrait_Austere1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Austere")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Austere1_impl(context) then
		effect.trait("Austere", "agent", 1, 35, context)
		effect.trait("Austere", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Authoritarian1 ]]--

function Father_HasTrait_Authoritarian1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Authoritarian")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Authoritarian1_impl(context) then
		effect.trait("Authoritarian", "agent", 1, 30, context)
		effect.trait("Authoritarian", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Cautious1 ]]--

function Father_HasTrait_Cautious1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Cautious")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Cautious1_impl(context) then
		effect.trait("Cautious", "agent", 1, 35, context)
		effect.trait("Cautious", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Cheeky1 ]]--

function Father_HasTrait_Cheeky1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Cheeky")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Cheeky1_impl(context) then
		effect.trait("Cheeky", "agent", 1, 20, context)
		effect.trait("Cheeky", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_CloseMinded1 ]]--

function Father_HasTrait_CloseMinded1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Close_Minded")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_CloseMinded1_impl(context) then
		effect.trait("Close_Minded", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_DarkHumor1 ]]--

function Father_HasTrait_DarkHumor1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Dark_Humor")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_DarkHumor1_impl(context) then
		effect.trait("Dark_Humor", "agent", 1, 25, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Devious1 ]]--

function Father_HasTrait_Devious1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Devious")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Devious1_impl(context) then
		effect.trait("Devious", "agent", 1, 15, context)
		effect.trait("Devious", "agent", 2, 15, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Disorderly1 ]]--

function Father_HasTrait_Disorderly1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Disorderly")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Disorderly1_impl(context) then
		effect.trait("Disorderly", "agent", 1, 5, context)
		effect.trait("Disorderly", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Energetic1 ]]--

function Father_HasTrait_Energetic1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Energetic")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Energetic1_impl(context) then
		effect.trait("Energetic", "agent", 1, 10, context)
		effect.trait("Energetic", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Envious1 ]]--

function Father_HasTrait_Envious1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Envious")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Envious1_impl(context) then
		effect.trait("Envious", "agent", 1, 10, context)
		effect.trait("Envious", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Epicurean1 ]]--

function Father_HasTrait_Epicurean1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Epicurean")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Epicurean1_impl(context) then
		effect.trait("Epicurean", "agent", 1, 20, context)
		effect.trait("Epicurean", "agent", 2, 12, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Friendly1 ]]--

function Father_HasTrait_Friendly1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Friendly")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Friendly1_impl(context) then
		effect.trait("Friendly", "agent", 1, 15, context)
		effect.trait("Friendly", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Harsh1 ]]--

function Father_HasTrait_Harsh1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Harsh")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Harsh1_impl(context) then
		effect.trait("Harsh", "agent", 1, 20, context)
		effect.trait("Harsh", "agent", 2, 20, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_HatesBarbarians1 ]]--

function Father_HasTrait_HatesBarbarians1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Hates_Barbarians")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_HatesBarbarians1_impl(context) then
		effect.trait("Hates_Barbarians", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_HatesCarthaginians1 ]]--

function Father_HasTrait_HatesCarthaginians1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Hates_Carthaginians")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_HatesCarthaginians1_impl(context) then
		effect.trait("Hates_Carthaginians", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_HatesEasterners1 ]]--

function Father_HasTrait_HatesEasterners1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Hates_Easterners")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_HatesEasterners1_impl(context) then
		effect.trait("Hates_Easterners", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_HatesGreeks1 ]]--

function Father_HasTrait_HatesGreeks1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Hates_Greeks")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_HatesGreeks1_impl(context) then
		effect.trait("Hates_Greeks", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_HatesRomans1 ]]--

function Father_HasTrait_HatesRomans1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Hates_Romans")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_HatesRomans1_impl(context) then
		effect.trait("Hates_Romans", "agent", 1, 50, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Ignorant1 ]]--

function Father_HasTrait_Ignorant1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Ignorant")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Ignorant1_impl(context) then
		effect.trait("Ignorant", "agent", 1, 5, context)
		effect.trait("Ignorant", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Intelligent1 ]]--

function Father_HasTrait_Intelligent1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Intelligent")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Intelligent1_impl(context) then
		effect.trait("Intelligent", "agent", 1, 50, context)
		effect.trait("Intelligent", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Just1 ]]--

function Father_HasTrait_Just1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Just")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Just1_impl(context) then
		effect.trait("Just", "agent", 1, 10, context)
		effect.trait("Just", "agent", 2, 15, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Kind1 ]]--

function Father_HasTrait_Kind1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Kind")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Kind1_impl(context) then
		effect.trait("Kind", "agent", 1, 10, context)
		effect.trait("Kind", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Optimates1 ]]--

function Father_HasTrait_Optimates1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Optimates")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Optimates1_impl(context) then
		effect.trait("Optimates", "agent", 1, 60, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Patient1 ]]--

function Father_HasTrait_Patient1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Patient")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Patient1_impl(context) then
		effect.trait("Patient", "agent", 1, 15, context)
		effect.trait("Patient", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Populares1 ]]--

function Father_HasTrait_Populares1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Populares")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Populares1_impl(context) then
		effect.trait("Populares", "agent", 1, 45, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Predominant1 ]]--

function Father_HasTrait_Predominant1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Predominant")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Predominant1_impl(context) then
		effect.trait("Predominant", "agent", 1, 15, context)
		effect.trait("Predominant", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Prudent1 ]]--

function Father_HasTrait_Prudent1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Prudent")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Prudent1_impl(context) then
		effect.trait("Prudent", "agent", 1, 15, context)
		effect.trait("Prudent", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Satirist1 ]]--

function Father_HasTrait_Satirist1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Satirist")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Satirist1_impl(context) then
		effect.trait("Satirist", "agent", 1, 10, context)
		effect.trait("Satirist", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Sharp1 ]]--

function Father_HasTrait_Sharp1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Sharp")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Sharp1_impl(context) then
		effect.trait("Sharp", "agent", 1, 20, context)
		effect.trait("Sharp", "agent", 2, 15, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Stoic1 ]]--

function Father_HasTrait_Stoic1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Stoic")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Stoic1_impl(context) then
		effect.trait("Stoic", "agent", 1, 20, context)
		effect.trait("Stoic", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Trustful1 ]]--

function Father_HasTrait_Trustful1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Trustful")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Trustful1_impl(context) then
		effect.trait("Trustful", "agent", 1, 20, context)
		effect.trait("Trustful", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Unfriendly1 ]]--

function Father_HasTrait_Unfriendly1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Unfriendly")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Unfriendly1_impl(context) then
		effect.trait("Unfriendly", "agent", 1, 10, context)
		effect.trait("Unfriendly", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Unjust1 ]]--

function Father_HasTrait_Unjust1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Unjust")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Unjust1_impl(context) then
		effect.trait("Unjust", "agent", 1, 20, context)
		effect.trait("Unjust", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Upright1 ]]--

function Father_HasTrait_Upright1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Upright")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Upright1_impl(context) then
		effect.trait("Upright", "agent", 1, 20, context)
		effect.trait("Upright", "agent", 2, 10, context)
		return true
	end
	return false
end

--[[ Father_HasTrait_Womanizer1 ]]--

function Father_HasTrait_Womanizer1_impl (context)
		return char_is_general(context:character()) and context:character():has_father() and context:character():father():has_trait("Womanizer")
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Father_HasTrait_Womanizer1_impl(context) then
		effect.trait("Womanizer", "agent", 1, 10, context)
		effect.trait("Womanizer", "agent", 2, 5, context)
		return true
	end
	return false
end

--[[ FirstInitialTraits_Diplomat ]]--

function FirstInitialTraitsDiplomat_impl (context)
		return context:character():character_type("dignitary")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if FirstInitialTraitsDiplomat_impl(context) then
		effect.trait("Diplomat_Boring_Speaker", "agent", 1, 4, context)
		effect.trait("Diplomat_Bribable", "agent", 1, 3, context)
		effect.trait("Diplomat_Brilliant_Speaker", "agent", 1, 4, context)
		effect.trait("Diplomat_Calm", "agent", 1, 5, context)
		effect.trait("Diplomat_Choleric", "agent", 1, 4, context)
		effect.trait("Diplomat_Foodie", "agent", 2, 4, context)
		effect.trait("Diplomat_Humble", "agent", 1, 2, context)
		effect.trait("Diplomat_Intelligent", "agent", 1, 4, context)
		effect.trait("Diplomat_Pompous", "agent", 1, 3, context)
		effect.trait("Diplomat_Sharp_Tounge", "agent", 1, 2, context)
		effect.trait("Diplomat_Shy", "agent", 1, 2, context)
		effect.trait("Diplomat_Steadfast", "agent", 1, 3, context)
		effect.trait("Diplomat_Tactful", "agent", 1, 4, context)
		effect.trait("Diplomat_Thoughtless", "agent", 1, 4, context)
		effect.trait("Diplomat_Underhanded", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ FirstInitialTraits_Spy ]]--

function FirstInitialTraitsSpy_impl (context)
		return context:character():character_type("spy")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if FirstInitialTraitsSpy_impl(context) then
		effect.trait("Spy_Affectionate", "agent", 1, 5, context)
		effect.trait("Spy_Cautious", "agent", 1, 5, context)
		effect.trait("Spy_Clumpsy", "agent", 1, 3, context)		
		effect.trait("Spy_Dangerous_Conspirator", "agent", 1, 3, context)
		effect.trait("Spy_Good_Liar", "agent", 1, 5, context)
		effect.trait("Spy_Grim", "agent", 1, 5, context)
		effect.trait("Spy_Hurried", "agent", 1, 4, context)
		effect.trait("Spy_Merciless", "agent", 1, 5, context)
		effect.trait("Spy_Merciless", "agent", 2, 5, context)
		effect.trait("Spy_Pathetic_Conspirator", "agent", 1, 3, context)
		effect.trait("Spy_Scheming", "agent", 1, 5, context)
		effect.trait("Spy_Solid_Connections", "agent", 1, 4, context)
		effect.trait("Spy_Swift", "agent", 1, 5, context)
		effect.trait("Spy_Undependable", "agent", 1, 5, context)
		effect.trait("Spy_Weakhearted", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Administrator1 ]]--

function Has_Ancillary_Administrator1_impl (context)
		return context:character():has_ancillary("Administrator")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Administrator1_impl(context) then
		effect.trait("Able_Administrator", "agent", 1, 4, context)
		effect.trait("Effective_Taxman", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Amanita_Muscaria1 ]]--

function Has_Ancillary_Amanita_Muscaria1_impl (context)
		return context:character():has_ancillary("Amanita_Muscaria")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Amanita_Muscaria1_impl(context) then
		effect.trait("Insane", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Ascetic_Priest1 ]]--

function Has_Ancillary_Ascetic_Priest1_impl (context)
		return context:character():has_ancillary("Ascetic_Priest")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Ascetic_Priest1_impl(context) then
		effect.trait("Austere", "agent", 1, 3, context)
		effect.trait("Cheeky", "agent", 1, 4, context)
		effect.trait("Eccentric", "agent", 1, 2, context)
		effect.trait("Prudent", "agent", 1, 4, context)
		effect.trait("Upright", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Classical_Sophist1 ]]--

function Has_Ancillary_Classical_Sophist1_impl (context)
		return context:character():has_ancillary("Classical_Sophist")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Classical_Sophist1_impl(context) then
		effect.trait("Analytical", "agent", 1, 3, context)
		effect.trait("Close_Minded", "agent", 1, 3, context)
		effect.trait("Mathematician", "agent", 1, 3, context)
		effect.trait("Orator", "agent", 1, 3, context)
		effect.trait("Philosopher", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Dice_Collection1 ]]--

function Has_Ancillary_Dice_Collection1_impl (context)
		return context:character():has_ancillary("Dice_Collection")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Dice_Collection1_impl(context) then
		effect.trait("Gambler", "agent", 1, 4, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Drinking_Horn1 ]]--

function Has_Ancillary_Drinking_Horn1_impl (context)
		return context:character():has_ancillary("Drinking_Horn")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Drinking_Horn1_impl(context) then
		effect.trait("Drinker", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Equestrian1 ]]--

function Has_Ancillary_Equestrian1_impl (context)
		return context:character():has_ancillary("Equestrian")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Equestrian1_impl(context) then
		effect.trait("Cavalryman", "agent", 1, 6, context)
		effect.trait("Scarred", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Exotic_Slave1 ]]--

function Has_Ancillary_Exotic_Slave1_impl (context)
		return context:character():has_ancillary("Exotic_Slave")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Exotic_Slave1_impl(context) then
		effect.trait("Eccentric", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Family_Patron1 ]]--

function Has_Ancillary_Family_Patron1_impl (context)
		return context:character():has_ancillary("Family_Patron")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Family_Patron1_impl(context) then
		effect.trait("Effective_Taxman", "agent", 1, 3, context)
		effect.trait("Trustful", "agent", 1, 4, context)
		effect.trait("Possessive", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Golden_Girdle1 ]]--

function Has_Ancillary_Golden_Girdle1_impl (context)
		return context:character():has_ancillary("Golden_Girdle")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Golden_Girdle1_impl(context) then
		effect.trait("Possessive", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Medicus1 ]]--

function Has_Ancillary_Medicus1_impl (context)
		return context:character():has_ancillary("Medicus")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Medicus1_impl(context) then
		effect.trait("Healthy", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Military_Theorist1 ]]--

function Has_Ancillary_Military_Theorist1_impl (context)
		return context:character():has_ancillary("Military_Theorist")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Military_Theorist1_impl(context) then
		effect.trait("Good_Attacker", "agent", 1, 2, context)
		effect.trait("Good_Defender", "agent", 1, 2, context)
		effect.trait("Good_Planner", "agent", 1, 1, context)
		effect.trait("Logistician", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Military_Theorist2 ]]--

function Has_Ancillary_Military_Theorist2_impl (context)
		return context:character():has_ancillary("Military_Theorist")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Has_Ancillary_Military_Theorist2_impl(context) then
		effect.trait("Good_Attacker", "agent", 1, 5, context)
		effect.trait("Good_Defender", "agent", 1, 3, context)
		effect.trait("Good_Planner", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Misanthropist1 ]]--

function Has_Ancillary_Misanthropist1_impl (context)
		return context:character():has_ancillary("Misanthropist")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Misanthropist1_impl(context) then
		effect.trait("Analytical", "agent", 1, 2, context)
		effect.trait("Skeptic", "agent", 1, 3, context)
		effect.trait("Sharp", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Modern_Sophist1 ]]--

function Has_Ancillary_Modern_Sophist1_impl (context)
		return context:character():has_ancillary("Modern_Sophist")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Modern_Sophist1_impl(context) then
		effect.trait("Analytical", "agent", 1, 3, context)
		effect.trait("Energetic", "agent", 1, 1, context)
		effect.trait("Inventive", "agent", 1, 2, context)
		effect.trait("Motivated", "agent", 1, 3, context)
		effect.trait("Philosopher", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Old_Veteran1 ]]--

function Has_Ancillary_Old_Veteran1_impl (context)
		return context:character():has_ancillary("Old_Veteran")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Old_Veteran1_impl(context) then
		effect.trait("Dark_Humor", "agent", 1, 2, context)
		effect.trait("Loyal", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Old_Veteran2 ]]--

function Has_Ancillary_Old_Veteran2_impl (context)
		return context:character():has_ancillary("Old_Veteran")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Has_Ancillary_Old_Veteran2_impl(context) then
		effect.trait("Courageous", "agent", 1, 8, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Philanthropic_Sophist1 ]]--

function Has_Ancillary_Philanthropic_Sophist1_impl (context)
		return context:character():has_ancillary("Philanthropic_Sophist")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Philanthropic_Sophist1_impl(context) then
		effect.trait("Humble", "agent", 1, 3, context)
		effect.trait("Just", "agent", 1, 3, context)
		effect.trait("Kind", "agent", 1, 2, context)
		effect.trait("Open_Minded", "agent", 1, 2, context)
		effect.trait("Rational", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Poet1 ]]--

function Has_Ancillary_Poet1_impl (context)
		return context:character():has_ancillary("Poet")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Poet1_impl(context) then
		effect.trait("Witty", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Political_Benefactor1 ]]--

function Has_Ancillary_Political_Benefactor1_impl (context)
		return context:character():has_ancillary("Political_Benefactor")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Political_Benefactor1_impl(context) then
		effect.trait("Authoritarian", "agent", 1, 3, context)
		effect.trait("Persuasive", "agent", 1, 1, context)
		effect.trait("Political_Talent", "agent", 1, 2, context)
		effect.trait("Political_Underdog", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Practised_Lawman1 ]]--

function Has_Ancillary_Practised_Lawman1_impl (context)
		return context:character():has_ancillary("Practised_Lawman")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Practised_Lawman1_impl(context) then
		effect.trait("Just", "agent", 1, 2, context)
		effect.trait("Liar", "agent", 1, 2, context)
		effect.trait("Pragmatic", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Religious_Scholar1 ]]--

function Has_Ancillary_Religious_Scholar1_impl (context)
		return context:character():has_ancillary("Religious_Scholar")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Religious_Scholar1_impl(context) then
		effect.trait("Just", "agent", 1, 2, context)
		effect.trait("Pious", "agent", 1, 5, context)
		effect.trait("Religious_Mania", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Retired_Diplomat1 ]]--

function Has_Ancillary_Retired_Diplomat1_impl (context)
		return context:character():has_ancillary("Retired_Diplomat")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Retired_Diplomat1_impl(context) then
		effect.trait("Confrontational", "agent", 1, 2, context)
		effect.trait("Diplomatic", "agent", 1, 3, context)
		effect.trait("Epicurean", "agent", 1, 4, context)
		effect.trait("Orator", "agent", 1, 3, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Stoic_Philosopher1 ]]--

function Has_Ancillary_Stoic_Philosopher1_impl (context)
		return context:character():has_ancillary("Stopic_Philosopher")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Stoic_Philosopher1_impl(context) then
		effect.trait("Philosopher", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Swordmaster1 ]]--

function Has_Ancillary_Swordmaster1_impl (context)
		return context:character():has_ancillary("Swordmaster")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Swordmaster1_impl(context) then
		effect.trait("Energetic", "agent", 1, 4, context)
		effect.trait("Gambler", "agent", 1, 2, context)
		effect.trait("Scarred", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Torturer1 ]]--

function Has_Ancillary_Torturer1_impl (context)
		return context:character():has_ancillary("Torturer")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Torturer1_impl(context) then
		effect.trait("Brutal", "agent", 1, 4, context)
		effect.trait("Harsh", "agent", 1, 2, context)
		return true
	end
	return false
end

--[[ Has_Ancillary_Unbalanced_Weighing_Scales1 ]]--

function Has_Ancillary_Unbalanced_Weighing_Scales1_impl (context)
		return context:character():has_ancillary("Unbalanced_Weighing_Scales")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Ancillary_Unbalanced_Weighing_Scales1_impl(context) then
		effect.trait("Corrupt_Trader", "agent", 1, 5, context)
		return true
	end
	return false
end

--[[ Has_Trait_Authoritarian1 ]]--

function Has_Trait_Authoritarian1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Authoritarian")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Authoritarian1_impl(context) then
		effect.trait("Predominant", "agent", 1, 1, context)
		return true
	end
	return false
end


--[[ Has_Trait_Infertile1 ]]--

function Has_Trait_Infertile1_impl (context)
		return char_is_general(context:character()) and context:character():has_trait("Infertile")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Infertile1_impl(context) then
		effect.trait("Satirist", "agent", 1, 1, context)
		return true
	end
	return false
end

--[[ Army_Supplied1 ]]--

function Army_Supplied1_impl (context)
		return char_is_general(context:character()) and char_in_owned_region(context:character()) and not (context:character():has_trait("Army_Supplied") or context:character():has_trait("Army_Scarce_Supplies"))
end 

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
	if Army_Supplied1_impl(context) then
		effect.trait("Army_Supplied", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ Army_Supplied2 ]]--

function Army_Supplied2_impl (context)
		return char_is_general(context:character()) and char_in_owned_region(context:character()) and not (context:character():has_trait("Army_Supplied") or context:character():has_trait("Army_Scarce_Supplies"))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Supplied2_impl(context) then
		effect.trait("Army_Supplied", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ General_Replaced_Foreign_Region1 ]]--

function General_Replaced1_impl (context)
        return char_in_foreign_region(context:character())
end

events.CharacterCreated[#events.CharacterCreated+1] =
function (context)
    if General_Replaced1_impl(context) then
	    effect.trait("Army_Scarce_Supplies", "agent", 5, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool1 ]]--

function General_Returned_Pool1_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 1
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool1_impl(context) then
	    effect.trait("Army_Supplied", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool2 ]]--

function General_Returned_Pool2_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 2
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool2_impl(context) then
	    effect.trait("Army_Supplied", "agent", 2, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool3 ]]--

function General_Returned_Pool3_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 3
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool3_impl(context) then
	    effect.trait("Army_Supplied", "agent", 3, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool4 ]]--

function General_Returned_Pool4_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 4
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool4_impl(context) then
	    effect.trait("Army_Supplied", "agent", 4, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool5 ]]--

function General_Returned_Pool5_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 5
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool5_impl(context) then
	    effect.trait("Army_Supplied", "agent", 5, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool6 ]]--

function General_Returned_Pool6_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 6
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool6_impl(context) then
	    effect.trait("Army_Supplied", "agent", 6, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool7 ]]--

function General_Returned_Pool7_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 7
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool7_impl(context) then
	    effect.trait("Army_Supplied", "agent", 7, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool8 ]]--

function General_Returned_Pool8_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 8
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool8_impl(context) then
	    effect.trait("Army_Supplied", "agent", 8, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool9 ]]--

function General_Returned_Pool9_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Scarce_Supplies") == 9
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool9_impl(context) then
	    effect.trait("Army_Supplied", "agent", 9, 100, context)
		return true
	end
	return false
end

--[[ General_Returned_Pool10 ]]--

function General_Returned_Pool10_impl (context)
        return not char_is_general_with_army(context:character()) and context:character():trait_points("Army_Supplied") == 1
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if General_Returned_Pool10_impl(context) then
	    effect.trait("Army_Scarce_Supplies", "agent", 1, 100, context)
		return true
	end
	return false
end

--[[ Army_Scarce_Supplies_Give1 ]]--

function Army_Scarce_Supplies1_impl (context)
        return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() < 1 and context:character():has_trait("Army_Supplied") and not context:character():has_trait("Army_Scarce_Supplies")
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if Army_Scarce_Supplies1_impl(context) then 
        effect.trait("Army_Scarce_Supplies", "agent", 2, 100, context)
        return true
	end
	return false
end

--[[ Army_Scarce_Supplies_Upgrade2 ]]--

function Army_Scarce_Supplies2_impl (context)
        return (char_is_general_with_army(context:character()) and (context:character():trait_level("Army_Scarce_Supplies") == 1 or context:character():trait_level("Army_Scarce_Supplies") == 2) and context:character():turns_in_own_regions() < 1) and context:character():model():turn_number() > 1
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
    if Army_Scarce_Supplies2_impl(context) then 
        effect.trait("Army_Scarce_Supplies", "agent", 1, 100, context)
        return true
	end
	return false
end

--[[ Army_Resupplied1 ]]--

function Army_Resupplied1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 1) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Army_Resupplied1_impl(context) then
		effect.trait("Army_Supplied", "agent", 2, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied2 ]]--

function Army_Resupplied2_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 2) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied2_impl(context) then
		effect.trait("Army_Supplied", "agent", 3, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied3 ]]--

function Army_Resupplied3_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 3) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied3_impl(context) then
		effect.trait("Army_Supplied", "agent", 4, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied4 ]]--

function Army_Resupplied4_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 4) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied4_impl(context) then
		effect.trait("Army_Supplied", "agent", 5, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied5 ]]--

function Army_Resupplied5_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 5) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied5_impl(context) then
		effect.trait("Army_Supplied", "agent", 6, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied6 ]]--

function Army_Resupplied6_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 6) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied6_impl(context) then
		effect.trait("Army_Supplied", "agent", 7, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied7 ]]--

function Army_Resupplied7_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 7) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied7_impl(context) then
		effect.trait("Army_Supplied", "agent", 8, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied8 ]]--

function Army_Resupplied8_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 8) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied8_impl(context) then
		effect.trait("Army_Supplied", "agent", 9, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied9 ]]--

function Army_Resupplied9_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") == 9) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied9_impl(context) then
		effect.trait("Army_Supplied", "agent", 10, 100, context)
		return true
	end
	return false
end

--[[ Army_Resupplied10 ]]--

function Army_Resupplied10_impl (context)
		return char_is_general_with_army(context:character()) and context:character():turns_in_own_regions() > 0 and (context:character():trait_points("Army_Scarce_Supplies") >= 10) and not context:character():has_trait("Army_Supplied")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Army_Resupplied10_impl(context) then
		effect.trait("Army_Supplied", "agent", 11, 100, context)
		return true
	end
	return false
end
