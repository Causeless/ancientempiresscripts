
-------------------------------------------------------
-------------------------------------------------------
--	LIBRARY SUPPORT FOR EXPORT_TRIGGERS.LUA
-------------------------------------------------------
-------------------------------------------------------

--local scripting = require "lua_scripts.EpisodicScripting"

print("lib_export_triggers.lua loaded");



scripting = nil;


function char_in_owned_region(char)
	return char:has_region() and (char:region():owning_faction():name() == char:faction():name());
end;

function char_in_foreign_region(char)
	return char:has_region() and (char:region():owning_faction():name() ~= char:faction():name());
end;

function char_is_agent(char)
	return char:character_type("champion") or char:character_type("spy") or char:character_type("dignitary")
end;

-- In Attila we have to filter as wives count as generals.
function char_is_general(char)
	return char:character_type("general") and char:is_male();
end;


-- Returns true if the character is the governor of a region.
function char_is_governor(char)
	return char:has_region() and char:region():has_governor() and char:region():governor() == char;
end


function char_is_general_with_army(char)
	return char_is_general(char) and char_has_army(char);
end;


function char_is_general_with_navy(char)
	return char_is_general(char) and char_has_navy(char);
end;

-- In Attila, Wives are just generals who are female.
function char_is_wife(char)
	return char:character_type("general") and not char:is_male();
end;


function char_has_army(char)
	return char:has_military_force() and char:military_force():is_army();
end;


function char_has_navy(char)
	return char:has_military_force() and char:military_force():is_navy();
end;


function char_rank_between(char, min, max)
	return char:rank() >= min and char:rank() <= max;
end;


function attacker_culture(pb)
	if pb:has_attacker() then
		return pb:attacker():faction():culture();
	end;
	
	return "";
end;


function defender_culture(pb)
	if pb:has_defender() then
		return pb:defender():faction():culture();
	end;
	
	return "";
end;


function attacker_faction(pb)
	if pb:has_attacker() then
		return pb:attacker():faction():name();
	end;
	
	return "";
end;


function defender_faction(pb)
	if pb:has_defender() then
		return pb:defender():faction():name();
	end;
	
	return "";
end;


function attacker_subculture(pb)
	if pb:has_attacker() then
		return pb:attacker():faction():subculture();
	end;
	
	return "";
end;


function defender_subculture(pb)
	if pb:has_defender() then
		return pb:defender():faction():subculture();
	end;
	
	return "";
end;

function fought_culture(char, pb, culture_name)
	if char_is_attacker(char) and defender_culture(pb) == culture_name then
		return true;
	elseif char_is_defender(char) and attacker_culture(pb) == culture_name then
		return true;
	end
	
	return false;
end;


function char_is_attacker(char)
	return char:model():pending_battle():has_attacker() and char:model():pending_battle():attacker():cqi() == char:cqi();
end;


function char_is_defender(char)
	return char:model():pending_battle():has_defender() and char:model():pending_battle():defender():cqi() == char:cqi()
end;


function attacker_victory(pb)
	local result = pb:attacker_battle_result();
	return result == "close_victory" or result == "decisive_victory" or result == "heroic_victory" or result == "pyrrhic_victory";
end;


function defender_victory(pb)
	local result = pb:defender_battle_result();
	return result == "close_victory" or result == "decisive_victory" or result == "heroic_victory" or result == "pyrrhic_victory";
end;


function won_battle(char, pb)
	if char_is_attacker(char) and attacker_victory(pb) then
		return true;
	elseif char_is_defender(char) and defender_victory(pb) then
		return true;
	end
	
	return false;
end;


function player_battle_result(char, pb)
	if char_is_attacker(char) then
		return pb:attacker_battle_result();
	else
		return pb:defender_battle_result();
	end;
end


function faction_contains_building(faction, key)
	local region_list = faction:region_list();
	
	for i = 0, region_list:num_items() - 1 do
		local region = region_list:item_at(i);
		
		if region:building_exists(key) then
			return true;
		end;
	end;
	
	return false;
end;


function faction_allied_with_state_list(faction, state_list)
	local faction_list = faction:model():world():faction_list();
	
	for i = 0, faction_list:num_items() - 1 do
		local current_faction = faction_list:item_at(i);
		
		if faction:name() ~= current_faction:name() and faction_name_in_state_list(current_faction:name(), state_list) and faction:allied_with(current_faction) then
			return true;
		end;		
	end;
	
	return false;
end;

function faction_allied_with_state(faction, state)

	if faction:model():world():faction_exists(state) then

		local query_faction = faction:model():world():faction_by_key(state);
	
		return faction:allied_with(query_faction);
	end;

	return false;

end;

function garrison_has_building(garrison, building_key)

	for i = 0, garrison:region():slot_list():num_items() - 1 do

		local slot = garrison:region():slot_list():item_at(i);

		if slot:has_building() and slot:building():name() == building_key then
			return true;
		end;

	end;

	return false;

end;


function garrison_has_building_superchain(garrison, superchain_key)

	for i = 0, garrison:region():slot_list():num_items() - 1 do

		local slot = garrison:region():slot_list():item_at(i);
	
		if slot:has_building() and slot:building():superchain() == superchain_key then
			return true;
		end;
			
	end;
	
	return false;
	
end;


-- Takes a list of units and a supplied category and returns the percentage of them in the unit as a fraction (i.e. 50% = 0.5)
function character_get_percentage_of_unit_class_in_army(unit_class_name, army_list)

	if army_list:num_items() == 0 then
		return 0;
	end;
	
	local num_found = 0;
	for i = 0, army_list:num_items() - 1 do
		if army_list:item_at(i):unit_class() == unit_class_name then
			num_found = num_found + 1;
		end;
	end;
	
	return num_found / army_list:num_items();
	
end;


-- Tests if any of the neighbour regions to the supplied one are different religion.
function region_has_neighbours_of_other_religion(region)

	for i = 0, region:adjacent_region_list():num_items() -1 do
		if region:majority_religion() ~= region:adjacent_region_list():item_at(i) then
			return true;
		end;
	end;
	
	return false;
	
end;


-- Returns number of spies in the target faction
function num_spies_in_faction(faction)
	
	if faction:character_list():num_items() == 0 then
		return 0;
	end;
	
	local num_found = 0;
	for i = 0, faction:character_list():num_items() - 1 do
		if faction:character_list():item_at(i):character_type("spy") then
			num_found = num_found + 1;
		end;
	end;

	return num_found;
	
end

-------------------------------------------------------
-------------------------------------------------------
--	LISTS OF STATES
-------------------------------------------------------
-------------------------------------------------------


greek_states_factions = {
	"pro_greek_cities",
	"rom_athens",
	"rom_cimmeria",
	"rom_cyprus",
	"rom_epirus",
	"rom_knossos",
	"rom_massilia",
	"rom_pergamon",
	"rom_rebel_hellenic",
	"rom_sparta",
	"rom_syracuse",
	"rom_trapezos",
	"rom_rebel_spartan",
	"rom_rhodos",
	"rom_sardes",
	"rom_ardiaei",
	"rom_daorsi"
};

successor_states_factions = {
	"rom_baktria",
	"rom_cyrenaica",
	"rom_macedon",
	"rom_seleucid",
	"rom_ptolemaics",
	"rom_rebel_egyptian"
};

eastern_states_factions = {
	"rom_arachosia",
	"rom_ardhan",
	"rom_aria",
	"rom_armenia",
	"rom_bithynia",
	"rom_cappadocia",
	"rom_colchis",
	"rom_drangiana",
	"rom_kartli",
	"rom_media",
	"rom_media_atropatene",
	"rom_parthava",
	"rom_persia",
	"rom_rebel_eastern",
	"rom_sagartia"
};

roman_states_factions = {
	"pro_lucanian",
	"pro_rebels",
	"pro_rome",
	"pro_samnites",
	"rom_etruscan",
	"rom_rebel_latin",
	"rom_rome",
	"rom_rome_civil_war",
	"rom_slave"
};

barbarian_states_factions = {
	"rom_aorsoi",
	"rom_budini",
	"rom_catiaroi",
	"rom_dahae",
	"rom_khorasmii",
	"rom_massagetae",
	"rom_rebel_nomadic",
	"rom_roxolani",
	"rom_scythia",
	"rom_siraces",
	"rom_thyssagetae",
	"rom_brigantes",
	"rom_caledones",
	"rom_demetae",
	"rom_dumnonii",
	"rom_ebdani",
	"rom_iceni",
	"rom_rebel_briton",
	"rom_arevaci",
	"rom_cantabri",
	"rom_celtici",
	"rom_cessetani",
	"rom_edetani",
	"rom_galleaci",
	"rom_lusitani",
	"rom_rebel_iberian",
	"rom_turdetani",
	"pro_gaul",
	"rom_anartes",
	"rom_bastarnae",
	"rom_biephi",
	"rom_boii",
	"rom_breuci",
	"rom_delmatae",
	"rom_eravisci",
	"rom_galatia",
	"rom_getae",
	"rom_helvetii",
	"rom_insubres",
	"rom_liguria",
	"rom_nori",
	"rom_odryssia",
	"rom_raeti",
	"rom_rebel_celt",
	"rom_rebel_dacian",
	"rom_rebel_illyrian",
	"rom_rebel_thracian",
	"rom_scordisci",
	"rom_triballi",
	"rom_tylis",
	"rom_veneti",
	"rom_aedui",
	"rom_artebartes",
	"rom_arverni",
	"rom_carnutes",
	"rom_namnetes",
	"rom_nervii",
	"rom_pictones",
	"rom_sequani",
	"rom_treverii",
	"rom_vivisci",
	"rom_volcae",
	"rom_aestii",
	"rom_cherusci",
	"rom_cimbri",
	"rom_frisii",
	"rom_gutones",
	"rom_lugii",
	"rom_marcomanni",
	"rom_rebel_german",
	"rom_rugii",
	"rom_suebi"
};

greco_roman_states_factions = {
	"pro_greek_cities",
	"rom_ardiaei",
	"rom_athens",
	"rom_cyprus",
	"rom_cyrenaica",
	"rom_daorsi",
	"rom_epirus",
	"rom_knossos",
	"rom_macedon",
	"rom_massilia",
	"rom_pergamon",
	"rom_rebel_hellenic",
	"rom_rebel_spartan",
	"rom_rhodos",
	"rom_sardes",
	"rom_seleucid",
	"rom_sparta",
	"rom_syracuse",
	"rom_trapezos",
	"pro_lucanian",
	"pro_rebels",
	"pro_rome",
	"pro_samnites",
	"rom_etruscan",
	"rom_rebel_latin",
	"rom_rome",
	"rom_rome_civil_war"
};


generic_states_factions = {
	"pro_roman_allies",
	"rom_arverni_civil_war",
	"rom_athens_civil_war",
	"rom_axum",
	"rom_blemmyes",
	"rom_carthage_civil_war",
	"rom_epirus_civil_war",
	"rom_gaetuli",
	"rom_garamantia",
	"rom_gerrhaea",
	"rom_himyar",
	"rom_iceni_civil_war",
	"rom_ma_in",
	"rom_macedon_civil_war",
	"rom_masaesyli",
	"rom_mascat",
	"rom_meroe",
	"rom_nabatea",
	"rom_nasamones",
	"rom_parthia",
	"rom_parthia_civil_war",
	"rom_pontus",
	"rom_pontus_civil_war",
	"rom_qidri",
	"rom_rebel_african",
	"rom_rebel_arabian",
	"rom_rebel_punic",
	"rom_saba",
	"rom_sparta_civil_war",
	"rom_suebi_civil_war"
};


religious_buildings_list = {
	"rom_BARBARIAN_religious_celtic_1",
	"rom_BARBARIAN_religious_celtic_Andraste_2",
	"rom_BARBARIAN_religious_celtic_Andraste_3",
	"rom_BARBARIAN_religious_celtic_Andraste_4",
	"rom_BARBARIAN_religious_celtic_arverni_5",
	"rom_BARBARIAN_religious_celtic_Britannia_2",
	"rom_BARBARIAN_religious_celtic_Britannia_3",
	"rom_BARBARIAN_religious_celtic_Britannia_4",
	"rom_BARBARIAN_religious_celtic_Cernunnos_2",
	"rom_BARBARIAN_religious_celtic_Cernunnos_3",
	"rom_BARBARIAN_religious_celtic_Cernunnos_4",
	"rom_BARBARIAN_religious_celtic_Epona_2",
	"rom_BARBARIAN_religious_celtic_Epona_3",
	"rom_BARBARIAN_religious_celtic_Epona_4",
	"rom_BARBARIAN_religious_celtic_Gobannus_2",
	"rom_BARBARIAN_religious_celtic_Gobannus_3",
	"rom_BARBARIAN_religious_celtic_Gobannus_4",
	"rom_BARBARIAN_religious_celtic_iceni_5",
	"rom_BARBARIAN_religious_celtic_Nodens_2",
	"rom_BARBARIAN_religious_celtic_Nodens_3",
	"rom_BARBARIAN_religious_celtic_Nodens_4",
	"rom_BARBARIAN_religious_celtic_Rosmerta_2",
	"rom_BARBARIAN_religious_celtic_Rosmerta_3",
	"rom_BARBARIAN_religious_celtic_Rosmerta_4",
	"rom_BARBARIAN_religious_celtic_Tuotatis_2",
	"rom_BARBARIAN_religious_celtic_Tuotatis_3",
	"rom_BARBARIAN_religious_celtic_Tuotatis_4",
	"rom_BARBARIAN_religious_germanic_1",
	"rom_BARBARIAN_religious_germanic_5",
	"rom_BARBARIAN_religious_germanic_Austro_2",
	"rom_BARBARIAN_religious_germanic_Austro_3",
	"rom_BARBARIAN_religious_germanic_Austro_4",
	"rom_BARBARIAN_religious_germanic_Fraujaz_2",
	"rom_BARBARIAN_religious_germanic_Fraujaz_3",
	"rom_BARBARIAN_religious_germanic_Fraujaz_4",
	"rom_BARBARIAN_religious_germanic_Frijjo_2",
	"rom_BARBARIAN_religious_germanic_Frijjo_3",
	"rom_BARBARIAN_religious_germanic_Frijjo_4",
	"rom_BARBARIAN_religious_germanic_Teiwaz_2",
	"rom_BARBARIAN_religious_germanic_Teiwaz_3",
	"rom_BARBARIAN_religious_germanic_Teiwaz_4",
	"rom_BARBARIAN_religious_germanic_Thunaraz_2",
	"rom_BARBARIAN_religious_germanic_Thunaraz_3",
	"rom_BARBARIAN_religious_germanic_Thunaraz_4",
	"rom_BARBARIAN_religious_germanic_Wodanaz_2",
	"rom_BARBARIAN_religious_germanic_Wodanaz_3",
	"rom_BARBARIAN_religious_germanic_Wodanaz_4"
	};
	

function faction_name_in_state_list(factionname, state_list)
	for i = 1, #state_list do
		if state_list[i] == factionname then
			return true;
		end;
	end;
	
	return false;
end;


function is_in_state_list(char, state_list)
	local factionname = char:faction():name();

	return faction_name_in_state_list(factionname, state_list);
end;


-------------------------------------------------------
-------------------------------------------------------
--	LISTS OF REGIONS
-------------------------------------------------------
-------------------------------------------------------


function region_name_in_region_list(regionname, region_list)
	for i = 1, #region_list do
		if region_list[i] == regionname then
			return true;
		end;
	end;
	
	return false;
end;


function is_in_region_list(char, region_list)
	local regionname = char:region():name();

	return region_name_in_region_list(regionname, region_list);
end;

test_regions_list = {
	"rom_baetica_baeturia",
	"rom_baetica_turdetania",
	"rom_belgica_arduenna_silva",
	"rom_belgica_silva_carbonaria"
};



function province_from_regionname(regionname)
	local first_underscore_pos = string.find(regionname, "_");
	
	if not first_underscore_pos then
		return false;
	end;
	
	local second_underscore_pos = string.find(string.sub(regionname, first_underscore_pos + 1), "_");

	if not second_underscore_pos then
		return false;
	end;
	
	return string.sub(regionname, first_underscore_pos + 1, first_underscore_pos + second_underscore_pos - 1);
end;


-- returns true if one faction owns entire province, by region
function faction_controls_province_by_region(region)
	local owning_faction_name = region:owning_faction():name();
	local province = province_from_regionname(region:name());
	local region_manager = region:model():world():region_manager():region_list();
	
	local max_regions_per_province = 4;
	local matched_regions = 0;
		
	for i = 0, region_list:num_items() - 1 do
		local curr_region = region_list:item_at(i);
		local curr_province = province_from_regionname(curr_region:name());
		
		if curr_province == province then
			if curr_region:owning_faction():name() ~= owning_faction_name then
				return false;
			end;
			
			matched_regions = matched_regions + 1;
			
			if matched_regions >= max_regions_per_province then
				break;
			end;
		end;
	end;
	
	return true;
end;



-------------------------------------------------------
-------------------------------------------------------
--	CHARACTER SELECTED MONITOR
--	keeps track of whether we have a local char
--	selected or otherwise
-------------------------------------------------------
-------------------------------------------------------

BOOL_Local_Character_Selected = false;

function OnLETCharacterSelected(context)
	if context:character():model():faction_is_local(context:character():faction():name()) then
		BOOL_Local_Character_Selected = true;
	else
		BOOL_Local_Character_Selected = false;
	end;
end;

function OnLETCharacterDeselected(context)
	BOOL_Local_Character_Selected = false;
end;


-------------------------------------------------------
-------------------------------------------------------
--	LOGGING EVENTS
-------------------------------------------------------
-------------------------------------------------------


function OnLETFactionTurnStart(context)
	local faction = context:faction();
	
	current_turn_reloads = 0;
	
	current_faction_spies_recruited_this_turn = 0;
	current_faction_dignitaries_recruited_this_turn = 0;
	current_faction_champions_recruited_this_turn = 0;
end;


function OnLETCharacterCreated(context)
	local char = context:character();

	if char:character_type("spy") then
		current_faction_spies_recruited_this_turn = current_faction_spies_recruited_this_turn + 1;
	elseif char:character_type("dignitary") then
		current_faction_dignitaries_recruited_this_turn = current_faction_dignitaries_recruited_this_turn + 1;
	elseif char:character_type("champion") then
		current_faction_champions_recruited_this_turn = current_faction_champions_recruited_this_turn + 1;
	end;
end;





-------------------------------------------------------
-------------------------------------------------------
--	LOADING/SAVING VALUES
-------------------------------------------------------
-------------------------------------------------------

current_faction_spies_recruited_this_turn = 0;
current_faction_dignitaries_recruited_this_turn = 0;
current_faction_champions_recruited_this_turn = 0;
current_turn_reloads = 0;

function OnLETSavingGame(context)
	scripting.game_interface:save_named_value("current_faction_spies_recruited_this_turn", current_faction_spies_recruited_this_turn, context);
	scripting.game_interface:save_named_value("current_faction_dignitaries_recruited_this_turn", current_faction_dignitaries_recruited_this_turn, context);
	scripting.game_interface:save_named_value("current_faction_champions_recruited_this_turn", current_faction_champions_recruited_this_turn, context);
	scripting.game_interface:save_named_value("current_turn_reloads", current_turn_reloads, context);
end;


function OnLETLoadingGame(context)
	current_faction_spies_recruited_this_turn = scripting.game_interface:load_named_value("current_faction_spies_recruited_this_turn", 0, context);
	current_faction_dignitaries_recruited_this_turn = scripting.game_interface:load_named_value("current_faction_dignitaries_recruited_this_turn", 0, context);
	current_faction_champions_recruited_this_turn = scripting.game_interface:load_named_value("current_faction_champions_recruited_this_turn", 0, context);
	current_turn_reloads = scripting.game_interface:load_named_value("current_turn_reloads", 0, context) + 1;
end;





function initialise_let(s)
	print("initialise_let() called, s is " .. tostring(s));
	scripting = s;
	
	-- LET = lib_export_triggers
	scripting.AddEventCallBack("SavingGame", OnLETSavingGame);
	scripting.AddEventCallBack("LoadingGame", OnLETLoadingGame);
	scripting.AddEventCallBack("FactionTurnStart", OnLETFactionTurnStart);
	scripting.AddEventCallBack("CharacterCreated", OnLETCharacterCreated);
	scripting.AddEventCallBack("CharacterSelected", OnLETCharacterSelected);
	scripting.AddEventCallBack("CharacterDeselected", OnLETCharacterDeselected);
end;



----------------------------------------------------------------
----------------------------------------------------------------
---- UNIT CLASSES
----------------------------------------------------------------
----------------------------------------------------------------

dogs_list = {
	"Cel_Savage_Dogs",
	"Rom_War_Dogs" 
};

pigs_list = {
	"Rom_War_Pigs"
};

elephants_class_list = {
	"elph" 
};

dogs_and_pigs_class_list = {
	"spcl"
};

siege_equipment_class_list = {
	"art_siege"
};

artillery_class_list = {
	"art_fix",
	"art_fld"
};


function unit_is_in_unit_list(unit_string, unit_list)
	for i = 1, #unit_list do
		if unit_string == unit_list[i] then
			return true;
		end;
	end;
	return false; 
end;

function character_has_unit_in_list_by_class(character, unit_list)
	if character:has_military_force() then
		local force = character:military_force();
		for i = 0, force:unit_list():num_items() - 1 do
			local unit = force:unit_list():item_at(i);
			if unit_is_in_unit_list(unit:unit_class(),  unit_list) then
				return true;
			end;
		end;
	end;
	return false;
end;

function character_has_unit_in_list_by_name(character, unit_list)
	if character:has_military_force() then
		local force = character:military_force();
		for i = 0, force:unit_list():num_items() - 1 do
			local unit = force:unit_list():item_at(i);
			if unit_is_in_unit_list(unit:unit_key(),  unit_list) then
				return true;
			end;
		end;
	end;
	return false;
end;