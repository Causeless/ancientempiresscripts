--[[
Automatically generated via export from C:/Users/Stelios.Avramidis\DaVE_local\branches/attila/slavs/rome2/raw_data/db
Edit manually at your own risk
--]]

module(..., package.seeall)

events = get_events()
-- Ancillary Declarations

--[[ Administrator1 ]]--

function Administrator1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():has_ancillary("Administrator") and context:character():age() > 16
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Administrator1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Administrator", 5,  context)
		end
		return true
	end
	return false
end

--[[ Administrator2 ]]--

function Administrator2_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():has_ancillary("Administrator") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Administrator2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Administrator", 1,  context)
		end
		return true
	end
	return false
end

--[[ Agriculturalist1 ]]--

function Agriculturalist1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_land" and not context:character():has_ancillary("Agriculturalist")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Agriculturalist1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Agriculturalist", 5,  context)
		end
		return true
	end
	return false
end

--[[ Amanita_Muscaria1 ]]--

function Amanita_Muscaria1_impl (context)
		return  (char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee())))  and not context:character():has_ancillary("Amanita_Muscaria") and context:character():faction():culture() == "rom_Barbarian"
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Amanita_Muscaria1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Amanita_Muscaria", 10,  context)
		end
		return true
	end
	return false
end

--[[ Animal_Trader1 ]]--

function Animal_Trader1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():has_ancillary("Animal_Trader")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Animal_Trader1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Animal_Trader", 5,  context)
		end
		return true
	end
	return false
end
--[[ Artefact_from_Jerusalem1 ]]--

function Artefact_from_Jerusalem1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():name() == "att_reg_palaestinea_aelia_capitolina" and not context:character():has_ancillary("Artefact_from_Jerusalem")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Artefact_from_Jerusalem1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Artefact_from_Jerusalem", 5,  context)
		end
		return true
	end
	return false
end

--[[ Ascetic_Priest1 ]]--

function Ascetic_Priest1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():has_ancillary("Ascetic_Priest") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Ascetic_Priest1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Ascetic_Priest", 1,  context)
		end
		return true
	end
	return false
end

--[[ Ascetic_Priest2 ]]--

function Ascetic_Priest2_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_temple" and not context:character():has_ancillary("Ascetic_Priest")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Ascetic_Priest2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Ascetic_Priest", 5,  context)
		end
		return true
	end
	return false
end

--[[ Bear_Fur_Coat1 ]]--

function Bear_Fur_Coat1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():faction():culture() == "rom_Barbarian" and not context:character():has_ancillary("Bear_Fur_Coat")
end

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Bear_Fur_Coat1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Bear_Fur_Coat", 2,  context)
		end
		return true
	end
	return false
end


--[[ Beastmaster1 ]]--

function Beastmaster1_impl (context)
		return char_is_general_with_army(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and context:character():has_military_force() and character_get_percentage_of_unit_class_in_army("elph", context:character():military_force():unit_list()) >= 0.10 and not context:character():has_ancillary("Beastmaster")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Beastmaster1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Beastmaster", 10,  context)
		end
		return true
	end
	return false
end

--[[ Berserker1 ]]--

function Berserker1_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():won_battle() == false and (context:pending_battle():attacker() == context:character() and context:pending_battle():percentage_of_defender_killed() >= 70) or (context:pending_battle():defender() == context:character() and context:pending_battle():percentage_of_attacker_killed() >= 70) and context:character():faction():culture() == "rom_Barbarian"
 and not context:character():has_ancillary("Berserker")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Berserker1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Berserker", 10,  context)
		end
		return true
	end
	return false
end

--[[ Brave_Warrior1 ]]--

function Brave_Warrior1_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():won_battle() == false and ((context:pending_battle():attacker() == context:character() and context:pending_battle():percentage_of_defender_killed() >= 70) or (context:pending_battle():defender() == context:character() and context:pending_battle():percentage_of_attacker_killed() >= 70)) and not context:character():has_ancillary("Brave_Warrior")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Brave_Warrior1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Brave_Warrior", 10,  context)
		end
		return true
	end
	return false
end

--[[ Brilliant_Inventor1 ]]--

function Brilliant_Inventor1_impl (context)
		return char_is_general(context:character()) and not context:character():has_ancillary("Brilliant_Inventor")
end 

events.CharacterFactionCompletesResearch[#events.CharacterFactionCompletesResearch+1] =
function (context)
	if Brilliant_Inventor1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Brilliant_Inventor", 1,  context)
		end
		return true
	end
	return false
end

--[[ Captain_of_the_Guard1 ]]--

function Captain_of_the_Guard1_impl (context)
		return  char_is_general(context:character()) and not context:character():has_ancillary("Captain_of_the_Guard")
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if Captain_of_the_Guard1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Captain_of_the_Guard", 50,  context)
		end
		return true
	end
	return false
end

--[[ Captain_of_the_Guard2 ]]--

function Captain_of_the_Guard2_impl (context)
		return  context:character():has_trait("Good_Personal_Security") and context:character():faction():culture() ~= "rom_Roman" and not context:character():has_ancillary("Captain_of_the_Guard")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Captain_of_the_Guard2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Captain_of_the_Guard", 3,  context)
		end
		return true
	end
	return false
end

--[[ Captain_of_the_Guard2 ]]--

function Captain_of_the_Guard2_impl (context)
		return  char_is_general_with_army(context:character()) and (context:character():won_battle() == true and context:pending_battle():attacker() == context:character() and context:pending_battle():percentage_of_attacker_killed() >= 65) or (context:character():won_battle() == true and context:pending_battle():defender() == context:character() and context:pending_battle():percentage_of_defender_killed() >= 65) and not context:character():has_ancillary("Captain_of_the_Guard")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Captain_of_the_Guard2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Captain_of_the_Guard", 25,  context)
		end
		return true
	end
	return false
end

--[[ Caretaker1 ]]--

function Caretaker1_impl (context)
		return  char_is_general(context:character()) and not context:character():has_ancillary("Caretaker")
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if Caretaker1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Caretaker", 20,  context)
		end
		return true
	end
	return false
end

--[[ Caretaker2 ]]--

function Caretaker2_impl (context)
		return  char_is_general(context:character()) and context:character():has_trait("Ill") and not context:character():has_ancillary("Caretaker")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Caretaker2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Caretaker", 2,  context)
		end
		return true
	end
	return false
end

--[[ Cavalry_Veteran1 ]]--

function Cavalry_Veteran1_impl (context)
		return  (char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and context:character():has_military_force() and character_get_percentage_of_unit_class_in_army("cav_mel", context:character():military_force():unit_list()) >= 0.25) and not context:character():has_ancillary("Cavalry_Veteran")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Cavalry_Veteran1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Cavalry_Veteran", 10,  context)
		end
		return true
	end
	return false
end

--[[ Chalcidian_Helmet1 ]]--

function Chalcidian_Helmet1_impl (context)
		return  ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():culture() == "rom_Hellenistic") or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():culture() == "rom_Hellenistic")) ) and not context:character():has_ancillary("Chalcidian_Helmet")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Chalcidian_Helmet1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Chalcidian_Helmet", 4,  context)
		end
		return true
	end
	return false
end

--[[ Corinthian_Helmet1 ]]--

function Corinthian_Helmet1_impl (context)
		return  ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():culture() == "rom_Hellenistic") or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():culture() == "rom_Hellenistic")) ) and not context:character():has_ancillary("Corinthian_Helmet")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Corinthian_Helmet1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Corinthian_Helmet", 4,  context)
		end
		return true
	end
	return false
end

--[[ Classical_Sophist1 ]]--

function Classical_Sophist1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) or context:character():is_politician() and not context:character():has_ancillary("Classical_Sophist") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Classical_Sophist1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Classical_Sophist", 2,  context)
		end
		return true
	end
	return false
end

--[[ Consummate_Politician1 ]]--

function Consummate_Politician1_impl (context)
		return char_is_general(context:character()) and context:character():is_politician() and not context:character():has_ancillary("Consummate_Politician")
end 

events.CharacterRankUp[#events.CharacterRankUp+1] =
function (context)
	if Consummate_Politician1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Consummate_Politician", 3,  context)
		end
		return true
	end
	return false
end

--[[ Decorated_Falx1 ]]--

function Decorated_Falx1_impl (context)
		return  ((char_is_general(context:character()) and (context:pending_battle():attacker() == context:character() and context:pending_battle():defender():faction():culture() == "rom_Barbarian") or (context:pending_battle():defender() == context:character() and context:pending_battle():attacker():faction():culture() == "rom_Barbarian")) and context:character():model():campaign_name("cha_attila")) and not context:character():has_ancillary("Decorated_Falx")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Decorated_Falx1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Decorated_Falx", 50,  context)
		end
		return true
	end
	return false
end
--[[ Decorative_Armour1 ]]--

function Decorative_Armour1_impl (context)
		return  (char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee())))  and not context:character():has_ancillary("Decorative_Armour")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Decorative_Armour1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Decorative_Armour", 5,  context)
		end
		return true
	end
	return false
end

--[[ Diplomat1 ]]--

function Diplomat1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():has_ancillary("Diplomat")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Diplomat1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Diplomat", 5,  context)
		end
		return true
	end
	return false
end

--[[ Disfigured_Veteran1 ]]--

function Disfigured_Veteran1_impl (context)
		return  (char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee())))  and not context:character():has_ancillary("Disfigured_Veteran")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Disfigured_Veteran1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Disfigured_Veteran", 5,  context)
		end
		return true
	end
	return false
end

--[[ Drinking_Horn1 ]]--

function Drinking_Horn1_impl (context)
		return  (char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee())))  and not context:character():has_ancillary("Drinking_Horn")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Drinking_Horn1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Drinking_Horn", 5,  context)
		end
		return true
	end
	return false
end

--[[ Dutiful_Steward1 ]]--

function Dutiful_Steward1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():has_ancillary("Dutiful_Steward")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Dutiful_Steward1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Dutiful_Steward", 1,  context)
		end
		return true
	end
	return false
end

--[[ Equestrian1 ]]--

function Equestrian1_impl (context)
		return  (char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and context:character():has_military_force() and character_get_percentage_of_unit_class_in_army("cav_mel", context:character():military_force():unit_list()) >= 0.25) and not context:character():has_ancillary("Equestrian") and context:character():age() < 30
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Equestrian1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Equestrian", 10,  context)
		end
		return true
	end
	return false
end

--[[ Exotic_Slave1 ]]--

function Exotic_Slave1_impl (context)
		return  char_is_general(context:character()) and not context:character():has_ancillary("Exotic_Slave")
end 

events.CharacterPostBattleEnslave[#events.CharacterPostBattleEnslave+1] =
function (context)
	if Exotic_Slave1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Exotic_Slave", 4,  context)
		end
		return true
	end
	return false
end

--[[ Expert_Armourer1 ]]--

function Expert_Armourer1_impl (context)
		return  char_is_general(context:character()) and not context:character():has_ancillary("Expert_Armourer")
end 

events.CharacterPostBattleEnslave[#events.CharacterPostBattleEnslave+1] =
function (context)
	if Expert_Armourer1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Expert_Armourer", 4,  context)
		end
		return true
	end
	return false
end

--[[ Expert_Armourer2 ]]--

function Expert_Armourer2_impl (context)
		return  char_is_general(context:character()) and not context:character():has_ancillary("Expert_Armourer")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Expert_Armourer2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Expert_Armourer", 2,  context)
		end
		return true
	end
	return false
end

--[[ Expert_Guide1 ]]--

function Expert_Guide1_impl (context)
		return  char_is_general(context:character()) and not context:character():has_ancillary("Expert_Guide")
end 

events.CharacterPostBattleEnslave[#events.CharacterPostBattleEnslave+1] =
function (context)
	if Expert_Guide1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Expert_Guide", 10,  context)
		end
		return true
	end
	return false
end

--[[ Expert_Guide2 ]]--

function Expert_Guide2_impl (context)
		return  char_is_general(context:character()) and context:character():has_skill("Character_Skill_Master_of_Terrain") and not context:character():has_ancillary("Expert_Guide")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Expert_Guide2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Expert_Guide", 5,  context)
		end
		return true
	end
	return false
end

--[[ False_Relic1 ]]--

function False_Relic1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_temple_tomb_macedon_hellenistic" and not context:character():has_ancillary("False_Relic")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if False_Relic1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("False_Relic", 1,  context)
		end
		return true
	end
	return false
end

--[[ Famous_Orator1 ]]--

function Famous_Orator1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_admin" and context:character():region():public_order() >60  and not context:character():has_ancillary("Famous_Orator")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Famous_Orator1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Famous_Orator", 10,  context)
		end
		return true
	end
	return false
end


--[[ Family_Patron1 ]]--

function Family_Patron1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Family_Patron") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Family_Patron1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Family_Patron", 3,  context)
		end
		return true
	end
	return false
end

--[[ Famous_Healer1 ]]--

function Famous_Healer1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library" and not context:character():has_ancillary("Famous_Healer")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Famous_Healer1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Famous_Healer", 1,  context)
		end
		return true
	end
	return false
end

--[[ Famous_Historian1 ]]--

function Famous_Historian1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Famous_Historian")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Famous_Historian1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Famous_Historian", 1,  context)
		end
		return true
	end
	return false
end

--[[ Feared_Manhunter1 ]]--

function Feared_Manhunter1_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():won_battle() == false and ((context:pending_battle():attacker() == context:character() and context:pending_battle():percentage_of_defender_killed() >= 70) or (context:pending_battle():defender() == context:character() and context:pending_battle():percentage_of_attacker_killed() >= 70)) and not context:character():has_ancillary("Feared_Manhunter")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Feared_Manhunter1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Feared_Manhunter", 10,  context)
		end
		return true
	end
	return false
end

--[[ Financial_Advisor1 ]]--

function Financial_Advisor1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Financial_Advisor")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Financial_Advisor1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Financial_Advisor", 1,  context)
		end
		return true
	end
	return false
end

--[[ Golden_Girdle1 ]]--

function Golden_Girdle1_impl (context)
		return  char_is_general(context:character()) and context:character():won_battle() and player_battle_result(context:character(), context:pending_battle()) == "heroic_victory" and not context:character():has_ancillary("Golden_Girdle")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Golden_Girdle1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Golden_Girdle", 5,  context)
		end
		return true
	end
	return false
end
--[[ Golden_Helmet1 ]]--

function Golden_Helmet1_impl (context)
		return  char_is_general(context:character()) and context:character():won_battle() and player_battle_result(context:character(), context:pending_battle()) == "heroic_victory" and not context:character():has_ancillary("Golden_Helmet")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Golden_Helmet1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Golden_Helmet", 10,  context)
		end
		return true
	end
	return false
end

--[[ Golden_Torc1 ]]--

function Golden_Torc1_impl (context)
		return  char_is_general(context:character()) and context:character():won_battle() and player_battle_result(context:character(), context:pending_battle()) == "heroic_victory" and not context:character():has_ancillary("Golden_Torc")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Golden_Torc1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Golden_Torc", 8,  context)
		end
		return true
	end
	return false
end

--[[ Great_Architect1 ]]--

function Great_Architect1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and not context:character():has_ancillary("Great_Architect")
end

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Great_Architect1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Great_Architect", 4,  context)
		end
		return true
	end
	return false
end

--[[ Great_Artist1 ]]--

function Great_Artist1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Great_Artist")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Great_Artist1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Great_Artist", 1,  context)
		end
		return true
	end
	return false
end

--[[ Greedy_Merchant1 ]]--

function Greedy_Merchant1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Greedy_Merchant")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Greedy_Merchant1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Greedy_Merchant", 1,  context)
		end
		return true
	end
	return false
end

--[[ Herald1 ]]--

function Herald1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_admin" and not context:character():has_ancillary("Herald")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Herald1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Herald", 1,  context)
		end
		return true
	end
	return false
end

--[[ Herald2 ]]--

function Herald2_impl (context)
		return char_is_general_with_army(context:character()) and not context:character():has_ancillary("Herald")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Herald2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Herald", 1,  context)
		end
		return true
	end
	return false
end

--[[ Intelligent_Spymaster1 ]]--

function Intelligent_Spymaster1_impl (context)
		return  char_is_general(context:character()) and num_spies_in_faction(context:character():faction()) > 3 and not context:character():has_ancillary("Intelligent_Spymaster")
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Intelligent_Spymaster1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Intelligent_Spymaster", 1,  context)
		end
		return true
	end
	return false
end

--[[ Laurel_Wreath1 ]]--

function Laurel_Wreath1_impl (context)
		return  char_is_general(context:character()) and char_rank_between(context:character(), 5, 9) and not context:character():has_ancillary("Laurel_Wreath")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Laurel_Wreath1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Laurel_Wreath", 1,  context)
		end
		return true
	end
	return false
end

--[[ Magistrate1 ]]--

function Magistrate_1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_admin" and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Magistrate_1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Magistrate", 1,  context)
		end
		return true
	end
	return false
end

--[[ Master_Trader1 ]]--

function Master_Trader1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >80  and not context:character():has_ancillary("Master_Trader")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Master_Trader1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Master_Trader", 1,  context)
		end
		return true
	end
	return false
end

--[[ Mastersmith_Armour1 ]]--

function Mastersmith_Armour1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_military_shop" and not context:character():has_ancillary("Mastersmith_Armour")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Mastersmith_Armour1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Mastersmith_Armour", 1,  context)
		end
		return true
	end
	return false
end

--[[ Mastersmith_Cataphract_Armour1 ]]--

function Mastersmith_Cataphract_Armour1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_military_shop" and not context:character():has_ancillary("Mastersmith_Cataphract_Armour")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Mastersmith_Cataphract_Armour1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Mastersmith_Cataphract_Armour", 3,  context)
		end
		return true
	end
	return false
end

--[[ Mastersmith_Horse_Armour1 ]]--

function Mastersmith_Horse_Armour1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_military_shop" and not context:character():has_ancillary("Mastersmith_Horse_Armour")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Mastersmith_Horse_Armour1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Mastersmith_Horse_Armour", 3,  context)
		end
		return true
	end
	return false
end

--[[ Mastersmith_Sword1 ]]--

function Mastersmith_Sword1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_military_shop" and not context:character():has_ancillary("Mastersmith_Sword")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Mastersmith_Sword1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Mastersmith_Sword", 2,  context)
		end
		return true
	end
	return false
end

--[[ Medicus1 ]]--

function Medicus1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library"  and not context:character():has_ancillary("Medicus")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Medicus1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Medicus", 1,  context)
		end
		return true
	end
	return false
end

--[[ Mercenary_Commander1 ]]--

function Mercenary_Commander1_impl (context)
		return  (char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and context:character():military_force():contains_mercenaries() and player_battle_result(context:character(), context:pending_battle()) == "decisive_victory")  and not context:character():has_ancillary("Mercenary_Commander")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Mercenary_Commander1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Mercenary_Commander", 12,  context)
		end
		return true
	end
	return false
end

--[[ Military_Theorist1 ]]--

function Military_Theorist1_impl (context)
		return  (char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and player_battle_result(context:character(), context:pending_battle()) == "heroic_victory")  and not context:character():has_ancillary("Military_Theorist") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Military_Theorist1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Military_Theorist", 10,  context)
		end
		return true
	end
	return false
end

--[[ Misanthropist1 ]]--

function Misanthropist1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() <20  and not context:character():has_ancillary("Misanthropist") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Misanthropist1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Misanthropist", 2,  context)
		end
		return true
	end
	return false
end

--[[ Modern_Sophist1 ]]--

function Modern_Sophist1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Modern_Sophist") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Modern_Sophist1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Modern_Sophist", 1,  context)
		end
		return true
	end
	return false
end

--[[ Modern_Sophist2 ]]--

function Modern_Sophist2_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library" and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Modern_Sophist2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Modern_Sophist", 10,  context)
		end
		return true
	end
	return false
end

--[[ Mysterious_Ancient_Scrolls1 ]]--

function Mysterious_Ancient_Scrolls1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and (context:character():region():name() == "att_reg_palaestinea_aelia_capitolina" or context:character():region():name() == "att_reg_persis_siraf" or context:character():region():name() == "att_reg_macedonia_thessalnoica" or context:character():region():name() == "att_reg_narbonensis_narbo" or context:character():region():name() == "att_reg_syria_antiochia") and not context:character():has_ancillary("Mysterious_Ancient_Scrolls")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Mysterious_Ancient_Scrolls1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Mysterious_Ancient_Scrolls", 1,  context)
		end
		return true
	end
	return false
end

--[[ Old_Veteran1 ]]--

function Old_Veteran1_impl (context)
		return  char_is_general(context:character()) and context:character():won_battle() and not context:character():has_ancillary("Old_Veteran") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Old_Veteran1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Old_Veteran", 5,  context)
		end
		return true
	end
	return false
end

--[[ Nisean_Stallion1 ]]--

function Nisean_Stallion1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:character():region():name() == "att_reg_media_atropatene_ecbatana" or context:character():region():name() == "att_reg_media_atropatene_ganzaga" or context:character():region():name() == "att_reg_media_atropatene_rhaga") and (context:building():superchain() == "rom_sch_land" or context:building():superchain() == "rom_sch_land_arab" or context:building():superchain() == "rom_sch_land_arab_bedouin" or context:building():superchain() == "rom_sch_land_arab_foreign" or context:building():superchain() == "rom_sch_land_berber_home" or context:building():superchain() == "rom_sch_berber_foreign" or context:building():superchain() == "rom_sch_land_carthage" or context:building():superchain() == "rom_sch_land_greek" or context:building():superchain() == "rom_sch_land_hellenistic" or context:building():superchain() == "rom_sch_land_iberian_home" or context:building():superchain() == "rom_sch_land_iberian_foreign" or context:building():superchain() == "rom_sch_land_native" or context:building():superchain() == "rom_sch_land_pontus" or context:building():superchain() == "rom_sch_military_field") and not context:character():has_ancillary("Nisean_Stallion")
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Nisean_Stallion1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Nisean_Stallion", 2,  context)
		end
		return true
	end
	return false
end

--[[ Other_Horse1 ]]--

function Other_Horse1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_military_field" and not (context:character():has_ancillary("Old_Bay") or context:character():has_ancillary("Gelding") or context:character():has_ancillary("Large_Chestnut"))
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Other_Horse1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Old_Bay", 2,  context)
			effect.ancillary("Gelding", 3,  context)
			effect.ancillary("Large_Chestnut", 2,  context)
		end
		return true
	end
	return false
end

--[[ Oracle1 ]]--

function Oracle1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_temple" and context:character():region():public_order() >60  and not context:character():has_ancillary("Oracle")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Oracle1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Oracle", 1,  context)
		end
		return true
	end
	return false
end

--[[ Oracle2 ]]--

function Oracle2_impl (context)
		return (char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and player_battle_result(context:character(), context:pending_battle()) == "heroic_victory") and not context:character():has_ancillary("Oracle")
end

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Oracle2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Oracle", 4,  context)
		end
		return true
	end
	return false
end

--[[ Ornate_Toga1 ]]--

function Ornate_Toga1_impl (context)
		return char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Ornate_Toga")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Ornate_Toga1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Ornate_Toga", 1,  context)
		end
		return true
	end
	return false
end

--[[ Parade_Armour1 ]]--

function Parade_Armour1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Parade_Armour")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Parade_Armour1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Parade_Armour", 1,  context)
		end
		return true
	end
	return false
end

--[[ Pet_Monkey1 ]]--

function Pet_Monkey1_impl (context)
		return  char_is_governor(context:character()) and (context:character():region():name() == "att_reg_aegyptus_alexandria" or context:character():region():name() == "att_reg_asorstan_ctesiphon") and not context:character():has_ancillary("Pet_Monkey")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Pet_Monkey1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Pet_Monkey", 3,  context)
		end
		return true
	end
	return false
end

--[[ Pet_Tiger1 ]]--

function Pet_Tiger1_impl (context)
		return  char_is_governor(context:character()) and (context:character():region():name() == "att_reg_aegyptus_alexandria" or context:character():region():name() == "att_reg_asorstan_ctesiphon") and not context:character():has_ancillary("Pet_Tiger")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Pet_Tiger1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Pet_Tiger", 3,  context)
		end
		return true
	end
	return false
end

--[[ Philanthropic_Sophist1 ]]--

function Philanthropic_Sophist1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "rom_sch_temple" or context:building():superchain() == "rom_sch_entertainment" or context:building():superchain() == "rom_sch_water") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Philanthropic_Sophist1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Philanthropic_Sophist", 3,  context)
		end
		return true
	end
	return false
end

--[[ Philosopher1 ]]--

function Philosopher1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library"  and not (context:character():has_ancillary("Philosopher") or context:character():has_ancillary("Great_Philosopher"))
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Philosopher1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Philosopher", 2,  context)
			effect.ancillary("Great_Philosopher", 1,  context)
		end
		return true
	end
	return false
end

--[[ Poet1 ]]--

function Poet1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >60  and not context:character():has_ancillary("Poet")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Poet1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Poet", 1,  context)
		end
		return true
	end
	return false
end

--[[ Political_Benefactor1 ]]--

function Political_Benefactor1_impl (context)
		return  char_is_general(context:character()) and (char_is_governor(context:character()) or context:character():is_politician()) and not context:character():has_ancillary("Political_Benefactor") and context:character():age() > 16 and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Political_Benefactor1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Political_Benefactor", 2,  context)
		end
		return true
	end
	return false
end

--[[ Practised_Lawman1 ]]--

function Practised_Lawman1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "rom_sch_admin" or context:building():superchain() == "rom_sch_military_field") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Practised_Lawman1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Practised_Lawman", 2,  context)
		end
		return true
	end
	return false
end

--[[ Purple_Clothing1 ]]--

function Purple_Clothing1_impl (context)
		return  (char_is_governor(context:character()) and char_rank_between(context:character(), 3, 7)) and not context:character():has_ancillary("Purple_Clothing")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Purple_Clothing1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Purple_Clothing", 1,  context)
		end
		return true
	end
	return false
end

--[[ Purple_Silk_Clothing1 ]]--

function Purple_Silk_Clothing1_impl (context)
		return  (char_is_general(context:character()) and char_is_governor(context:character()) and (context:character():has_trait("Epicurean") or context:character():has_trait("Possessive")) and char_rank_between(context:character(), 4, 8)) and not context:character():has_ancillary("Purple_Silk_Clothing")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Purple_Silk_Clothing1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Purple_Silk_Clothing", 2,  context)
		end
		return true
	end
	return false
end

--[[ Religious_Fanatic1 ]]--

function Religious_Fanatic1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_temple" and not context:character():has_ancillary("Religious_Fanatic")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Religious_Fanatic1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Religious_Fanatic", 1,  context)
		end
		return true
	end
	return false
end
--[[ Religious_Scholar1 ]]--

function Religious_Scholar1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >80  and not context:character():has_ancillary("Religious_Scholar") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Religious_Scholar1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Religious_Scholar", 1,  context)
		end
		return true
	end
	return false
end

--[[ Religious_Scholar2 ]]--

function Religious_Scholar2_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_temple" and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Religious_Scholar2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Religious_Scholar", 1,  context)
		end
		return true
	end
	return false
end

--[[ Retired_Diplomat1 ]]--

function Retired_Diplomat1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >80  and not context:character():has_ancillary("Retired_Diplomat") and context:character():age() < 30 and context:character():age() > 16
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Retired_Diplomat1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Retired_Diplomat", 1,  context)
		end
		return true
	end
	return false
end

--[[ Shrewd_Informer1 ]]--

function Shrewd_Informer1_impl (context)
		return  context:character():is_faction_leader() and num_spies_in_faction(context:character():faction()) > 3 and not context:character():has_ancillary("Shrewd_Informer")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Shrewd_Informer1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Shrewd_Informer", 1,  context)
		end
		return true
	end
	return false
end

--[[ Siege_Engineer1 ]]--

function Siege_Engineer1_impl (context)
		return  char_is_general_with_army(context:character()) and context:pending_battle():attacker() == context:character() and context:character():won_battle() and context:pending_battle():has_contested_garrison() and not context:character():has_ancillary("Siege_Engineer")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Siege_Engineer1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Siege_Engineer", 8,  context)
		end
		return true
	end
	return false
end

--[[ Silk_Clothing1 ]]--

function Silk_Clothing1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and (context:character():has_trait("Epicurean") or context:character():has_trait("Possessive")) and not context:character():has_ancillary("Silk_Clothing")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Silk_Clothing1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Silk_Clothing", 5,  context)
		end
		return true
	end
	return false
end

--[[ Skilled_Administrator1 ]]--

function Skilled_Administrator1_impl (context)
		return  char_is_general(context:character()) and char_is_governor(context:character()) and context:character():region():public_order() >80 and context:character():faction():tax_level() > 35 and not context:character():has_ancillary("Skilled_Administrator")
end 

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Skilled_Administrator1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Skilled_Administrator", 3,  context)
		end
		return true
	end
	return false
end

--[[ Slave_Trader1 ]]--

function Slave_Trader1_impl (context)
		return  char_is_general(context:character()) and not context:character():has_ancillary("Slave_Trader")
end 

events.CharacterPostBattleEnslave[#events.CharacterPostBattleEnslave+1] =
function (context)
	if Slave_Trader1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Slave_Trader", 6,  context)
		end
		return true
	end
	return false
end

--[[ Statuette_of_Deity1 ]]--

function Statuette_of_Deity1_impl (context)
		return char_is_general(context:character()) and not context:character():has_ancillary("Statuette_of_Deity")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Statuette_of_Deity1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Statuette_of_Deity", 1,  context)
		end
		return true
	end
	return false
end

--[[ Stern_Drillmaster1 ]]--

function Stern_Drillmaster1_impl (context)
		return  char_is_general_with_army(context:character()) and not context:character():has_ancillary("Stern_Drillmaster")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Stern_Drillmaster1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Stern_Drillmaster", 25,  context)
		end
		return true
	end
	return false
end

--[[ Stern_Drillmaster2 ]]--

function Stern_Drillmaster2_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():action_points_remaining_percent() <= 10 and not context:character():has_ancillary("Stern_Drillmaster")
end

events.CharacterTurnEnd[#events.CharacterTurnEnd+1] =
function (context)
	if Stern_Drillmaster2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Stern_Drillmaster", 3,  context)
		end
		return true
	end
	return false
end

--[[ Stoic_Philosopher1 ]]--

function Stoic_Philosopher1_impl (context)
		return  context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library" and context:character():age() > 16 and not context:character():has_ancillary("Stoic_Philosopher")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Stoic_Philosopher1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Stoic_Philosopher", 12,  context)
		end
		return true
	end
	return false
end

--[[ Stuffed_Lion1 ]]--

function Stuffed_Lion1_impl (context)
		return  (char_is_general(context:character()) and char_is_governor(context:character()) and char_rank_between(context:character(), 3, 8) and context:character():has_trait("Epicurean")) and not context:character():has_ancillary("Stuffed_Lion")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Stuffed_Lion1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Stuffed_Lion", 10,  context)
		end
		return true
	end
	return false
end

--[[ Sturdy_Armour1 ]]--

function Sturdy_Armour1_impl (context)
		return  (char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee()))) and not context:character():has_ancillary("Sturdy_Armour")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Sturdy_Armour1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Sturdy_Armour", 5,  context)
		end
		return true
	end
	return false
end

--[[ Swordmaster1 ]]--

function Swordmaster1_impl (context)
		return  (char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee())))  and not context:character():has_ancillary("Swordmaster") and context:character():age() < 30
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Swordmaster1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Swordmaster", 5,  context)
		end
		return true
	end
	return false
end


--[[ Tender_of_the_Arse1 ]]--

function Tender_of_the_Arse1_impl (context)
		return  char_is_general(context:character()) and (context:character():has_trait("Epicurean") or context:character():has_trait("Gourmand") or context:character():has_trait("Possessive") or context:character():has_trait("Slothful")) and not context:character():has_ancillary("Tender_of_the_Arse")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Tender_of_the_Arse1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Tender_of_the_Arse", 2,  context)
		end
		return true
	end
	return false
end

--[[ Tender_of_the_Arse2 ]]--

function Tender_of_the_Arse2_impl (context)
		return  char_is_general_with_army(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 10 and context:character():in_settlement()  and context:character():model():turn_number() % 5 == 0 and context:character():age() > 16 and not context:character():has_ancillary("Tender_of_the_Arse")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Tender_of_the_Arse2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Tender_of_the_Arse", 4,  context)
		end
		return true
	end
	return false
end

--[[ Torturer1 ]]--

function Torturer1_impl (context)
		return  char_is_governor(context:character()) and context:character():region():public_order() < 10 and not context:character():has_ancillary("Torturer")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Torturer1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Torturer", 3,  context)
		end
		return true
	end
	return false
end

--[[ Torturer2 ]]--

function Torturer2_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_garrison" and not context:character():has_ancillary("Torturer")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Torturer2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Torturer", 2,  context)
		end
		return true
	end
	return false
end

--[[ Unbalanced_Weighing_Scales1 ]]--

function Unbalanced_Weighing_Scales1_impl (context)
		return  char_is_governor(context:character()) and context:character():region():public_order() < 20 and not context:character():has_ancillary("Unbalanced_Weighing_Scales")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Unbalanced_Weighing_Scales1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Unbalanced_Weighing_Scales", 1,  context)
		end
		return true
	end
	return false
end

--[[ Undefeated_Champion1 ]]--

function Undefeated_Champion1_impl (context)
		return  char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee()) or (context:pending_battle():defender() == context:character() and context:pending_battle():defender_commander_fought_in_melee())) and context:character():won_battle() and context:pending_battle():attacker_battle_result() == "heroic_victory"  and not context:character():has_ancillary("Undefeated_Champion")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Undefeated_Champion1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Undefeated_Champion", 5,  context)
		end
		return true
	end
	return false
end

--[[ Wise_Old_Hermit1 ]]--

function Wise_Old_Hermit1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_temple" and context:character():age() > 16 and not context:character():has_ancillary("Wise_Old_Hermit")
end

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Wise_Old_Hermit1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Wise_Old_Hermit", 10,  context)
		end
		return true
	end
	return false
end

--[[ Wolf_Fur_Coat1 ]]--

function Wolf_Fur_Coat1_impl (context)
		return  (char_is_general_with_army(context:character()) and ((context:pending_battle():attacker() == context:character() and context:pending_battle():attacker_commander_fought_in_melee() and context:pending_battle():defender():faction():subculture() == "rom_Barbarian"))) and not context:character():has_ancillary("Wolf_Fur_Coat")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Wolf_Fur_Coat1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Wolf_Fur_Coat", 5,  context)
		end
		return true
	end
	return false
end

--[[ Worn_Family_Heirloom1 ]]--

function Worn_Family_Heirloom1_impl (context)
		return  char_is_general(context:character())
end 

events.CharacterComesOfAge[#events.CharacterComesOfAge+1] =
function (context)
	if Worn_Family_Heirloom1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Worn_Family_Heirloom", 3,  context)
		end
		return true
	end
	return false
end

--[[ Worn_Family_Heirloom2 ]]--

function Worn_Family_Heirloom2_impl (context)
		return  char_is_general(context:character()) and context:character():age() > 16
end 

events.CharacterFamilyRelationDied[#events.CharacterFamilyRelationDied+1] =
function (context)
	if Worn_Family_Heirloom2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Worn_Family_Heirloom", 1,  context)
		end
		return true
	end
	return false
end

--[[ Actor1 ]]--

function Actor1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_entertainment" and context:character():age() > 16 and not context:character():has_ancillary("Actor")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Actor1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Actor", 2,  context)
		end
		return true
	end
	return false
end

--[[ Astrologer1 ]]--

function Astrologer1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library" and context:character():age() > 16 and not context:character():has_ancillary("Astrologer")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Astrologer1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Astrologer", 2,  context)
		end
		return true
	end
	return false
end

--[[ Celtic_Mercenary_Commander1 ]]--

function Celtic_Mercenary_Commander1_impl (context)
		return (char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and context:character():military_force():contains_mercenaries() and player_battle_result(context:character(), context:pending_battle()) == "decisive_victory") and not context:character():has_ancillary("Celtic_Mercenary_Captain")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Celtic_Mercenary_Commander1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Celtic_Mercenary_Captain", 3,  context)
		end
		return true
	end
	return false
end

--[[ Iberian_Mercenary_Commander1 ]]--

function Iberian_Mercenary_Commander1_impl (context)
		return (char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and context:character():military_force():contains_mercenaries() and player_battle_result(context:character(), context:pending_battle()) == "decisive_victory") and not context:character():has_ancillary("Iberian_Mercenary_Captain")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Iberian_Mercenary_Commander1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Iberian_Mercenary_Captain", 2,  context)
		end
		return true
	end
	return false
end

--[[ Cilician_Pirate ]]--

function Cilician_Pirate1_impl (context)
		return  char_is_general(context:character()) and context:character():won_battle() and context:pending_battle():naval_battle() and player_battle_result(context:character(), context:pending_battle()) == "decisive_victory" and not context:character():has_ancillary("Cilician_Pirate")
end 

events.CharacterPostBattleEnslave[#events.CharacterPostBattleEnslave+1] =
function (context)
	if Cilician_Pirate1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Cilician_Pirate", 7,  context)
		end
		return true
	end
	return false
end

--[[ Famous_Courtesan1 ]]--

function Famous_Courtesan1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "rom_sch_land" or context:building():superchain() == "rom_sch_land_arab" or context:building():superchain() == "rom_sch_land_arab_bedouin" or context:building():superchain() == "rom_sch_land_arab_foreign" or context:building():superchain() == "rom_sch_land_berber_home" or context:building():superchain() == "rom_sch_berber_foreign" or context:building():superchain() == "rom_sch_land_carthage" or context:building():superchain() == "rom_sch_land_greek" or context:building():superchain() == "rom_sch_land_hellenistic" or context:building():superchain() == "rom_sch_land_iberian_home" or context:building():superchain() == "rom_sch_land_iberian_foreign" or context:building():superchain() == "rom_sch_land_native" or context:building():superchain() == "rom_sch_land_pontus") and not context:character():has_ancillary("Famous_Courtesan") and context:character():age() > 16
end

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Famous_Courtesan1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Famous_Courtesan", 1,  context)
		end
		return true
	end
	return false
end

--[[ Magistrate1 ]]--

function Magistrate1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "rom_sch_admin" or context:building():superchain() == "rom_sch_city_center") and not context:character():has_ancillary("Magistrate")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Magistrate1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Magistrate", 3,  context)
		end
		return true
	end
	return false
end

--[[ Mistress1 ]]--

function Mistress1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and (context:building():superchain() == "rom_sch_land" or context:building():superchain() == "rom_sch_land_arab" or context:building():superchain() == "rom_sch_land_arab_bedouin" or context:building():superchain() == "rom_sch_land_arab_foreign" or context:building():superchain() == "rom_sch_land_berber_home" or context:building():superchain() == "rom_sch_berber_foreign" or context:building():superchain() == "rom_sch_land_carthage" or context:building():superchain() == "rom_sch_land_greek" or context:building():superchain() == "rom_sch_land_hellenistic" or context:building():superchain() == "rom_sch_land_iberian_home" or context:building():superchain() == "rom_sch_land_iberian_foreign" or context:building():superchain() == "rom_sch_land_native" or context:building():superchain() == "rom_sch_land_pontus") and not context:character():has_ancillary("Mistress")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Mistress1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Mistress", 2,  context)
		end
		return true
	end
	return false
end

--[[ Praetorian_Guardsman1 ]]--

function Praetorian_Guardsman1_impl (context)
		return  char_is_general(context:character()) and context:character():faction():culture() == "rom_Roman" and not context:character():has_ancillary("Praetorian_Guardsman")
end 

events.CharacterWoundedInAssassinationAttempt[#events.CharacterWoundedInAssassinationAttempt+1] =
function (context)
	if Praetorian_Guardsman1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Praetorian_Guardsman", 50,  context)
		end
		return true
	end
	return false
end

--[[ Praetorian_Guardsman2 ]]--

function Praetorian_Guardsman2_impl (context)
		return  context:character():has_trait("Good_Personal_Security") and context:character():faction():culture() == "rom_Roman" and not context:character():has_ancillary("Praetorian_Guardsman")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Praetorian_Guardsman2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Praetorian_Guardsman", 3,  context)
		end
		return true
	end
	return false
end

--[[ Quartermaster1 ]]--

function Quartermaster1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_military_field" and not context:character():has_ancillary("Quartermaster")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Quartermaster1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Quartermaster", 4,  context)
		end
		return true
	end
	return false
end

--[[ Roman_Legate1 ]]--

function Roman_Legate1_impl (context)
		return  char_is_general(context:character()) and context:character():won_battle() and not context:pending_battle():naval_battle() and player_battle_result(context:character(), context:pending_battle()) == "decisive_victory" and context:character():faction():culture() == "rom_Roman" and not context:character():has_ancillary("Roman_Legate")
end 

events.CharacterCompletedBattle[#events.CharacterCompletedBattle+1] =
function (context)
	if Roman_Legate1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Roman_Legate", 3,  context)
		end
		return true
	end
	return false
end

--[[ Scribe1 ]]--

function Scribe1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_library" and not context:character():has_ancillary("Scribe")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Scribe1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Scribe", 3,  context)
		end
		return true
	end
	return false
end

--[[ Seductress1 ]]--

function Seductress1_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 8 and context:character():in_port() and context:character():faction():is_human() and context:character():model():turn_number() > 1 and context:character():model():turn_number() % 5 == 0 and context:character():age() > 16 and not context:character():has_ancillary("Seductress")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Seductress1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Seductress", 8,  context)
		end
		return true
	end
	return false
end

--[[ Seductress2 ]]--

function Seductress2_impl (context)
		return char_is_general(context:character()) and context:character():has_region() and context:character():turns_in_own_regions() >= 10 and context:character():in_settlement() and context:character():model():turn_number() > 1 and context:character():model():turn_number() % 5 == 0 and context:character():age() > 16 and not context:character():has_ancillary("Seductress")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Seductress2_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Seductress", 8,  context)
		end
		return true
	end
	return false
end

--[[ Vestal_Virgin1 ]]--

function Vestal_Virgin1_impl (context)
		return context:character():has_region() and context:building():region():has_governor() and context:building():region():governor() == context:character() and context:building():superchain() == "rom_sch_temple" and not context:character():has_ancillary("Vestal_Virgin")
end 

events.CharacterBuildingCompleted[#events.CharacterBuildingCompleted+1] =
function (context)
	if Vestal_Virgin1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Vestal_Virgin", 5,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Austere ]]--

function Has_Trait_Austere1_impl (context)
		return context:character():has_trait("Austere") or context:character():has_trait("Prudent")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Austere1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Master_Trader", 1,  context)
			effect.ancillary("Skilled_Administrator", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Authoritarian ]]--

function Has_Trait_Authoritarian1_impl (context)
		return context:character():has_trait("Authoritarian")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Authoritarian1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Stern_Drillmaster", 2,  context)
			effect.ancillary("Captain_of_the_Guard", 1,  context)
			effect.ancillary("Dutiful_Steward", 1,  context)
			effect.ancillary("Herald", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Brutal ]]--

function Has_Trait_Brutal1_impl (context)
		return context:character():has_trait("Brutal")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Brutal1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Feared_Manhunter", 1,  context)
			effect.ancillary("Tender_of_the_Arse", 1,  context)
			effect.ancillary("Torturer", 2,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Cheeky ]]--

function Has_Trait_Cheeky1_impl (context)
		return context:character():has_trait("Cheeky")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Cheeky1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Herald", 1,  context)
			effect.ancillary("Diplomat", 2,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Corrupt ]]--

function Has_Trait_Corrupt1_impl (context)
		return context:character():has_trait("Corrupt")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Corrupt1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Greedy_Merchant", 2,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Disciplinarian ]]--

function Has_Trait_Disciplinarian1_impl (context)
		return context:character():has_trait("Disciplinarian")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Disciplinarian1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Stern_Drillmaster", 2,  context)
			effect.ancillary("Expert_Guide", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Eccentric ]]--

function Has_Trait_Eccentric1_impl (context)
		return  context:character():has_trait("Eccentric")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Eccentric1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Wise_Old_Hermit", 2,  context)
			effect.ancillary("Beastmaster", 1,  context)
			effect.ancillary("Torturer", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Gambler ]]--

function Has_Trait_Gambler1_impl (context)
		return  context:character():has_trait("Gambler")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Gambler1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Dice_Collection", 2,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_GoodPersonalSecurity ]]--

function Has_Trait_GoodPersonalSecurity1_impl (context)
		return context:character():has_trait("Good_Personal_Secruity")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_GoodPersonalSecurity1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Shrewd_Informer", 2,  context)
			effect.ancillary("Intelligent_Spymaster", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Handsome ]]--

function Has_Trait_Handsome1_impl (context)
		return  context:character():has_trait("Handsome")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Handsome1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Great_Artist", 1,  context)
			effect.ancillary("Poet", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Humble ]]--

function Has_Trait_Humble1_impl (context)
		return  context:character():has_trait("Humble")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Humble1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Diplomat", 1,  context)
			effect.ancillary("Poet", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Harsh ]]--

function Has_Trait_Harsh1_impl (context)
		return context:character():has_trait("Harsh")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Harsh1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Feared_Manhunter", 1,  context)
			effect.ancillary("Tender_of_the_Arse", 1,  context)
			effect.ancillary("Torturer", 2,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Ignorant ]]--

function Has_Trait_Ignorant1_impl (context)
		return context:character():has_trait("Ignorant")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Ignorant1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Wise_Old_Hermit", 1,  context)
			effect.ancillary("Religious_Fanati", 1,  context)
			effect.ancillary("Torturer", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Leper ]]--

function Has_Trait_Leper1_impl (context)
		return  context:character():has_trait("Leper") or context:character():has_trait("Gout")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Leper1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Famous_Healer", 1,  context)
			effect.ancillary("Dutiful_Steward", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Inventive ]]--

function Has_Trait_Inventive1_impl (context)
		return context:character():has_trait("Inventive")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Inventive1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Siege_Engineer", 1,  context)
			effect.ancillary("Great_Architect", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Industrious ]]--

function Has_Trait_Industrious1_impl (context)
		return context:character():has_trait("Industrious")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Industrious1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Great_Architect", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Persecutor ]]--

function Has_Trait_Persecutor1_impl (context)
		return context:character():has_trait("Persecutor")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Persecutor1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Feared_Manhunter", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Philosopher ]]--

function Has_Trait_Philosopher1_impl (context)
		return context:character():has_trait("Philosopher")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Philosopher1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Poet", 1,  context)
			effect.ancillary("Philosopher", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Pious ]]--

function Has_Trait_Pious1_impl (context)
		return context:character():has_trait("Pious")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Pious1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Oracle", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Playwright ]]--

function Has_Trait_Playwright1_impl (context)
		return context:character():has_trait("Playwright")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Playwright1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Poet", 1,  context)
			effect.ancillary("Great_Artist", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Religious_Mania ]]--

function Has_Trait_Religious_Mania1_impl (context)
		return context:character():has_trait("Religious_Mania")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Religious_Mania1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Religious_Fanatic", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Sharp ]]--

function Has_Trait_Sharp1_impl (context)
		return context:character():has_trait("Sharp")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Sharp1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Intelligent_Spymaster", 1,  context)
			effect.ancillary("Master_Trader", 1,  context)
			effect.ancillary("Consummate_Politician", 1,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Secretive ]]--

function Has_Trait_Secretive1_impl (context)
		return context:character():has_trait("Secretive")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Secretive1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Shrewd_Informer", 2,  context)
		end
		return true
	end
	return false
end

--[[ Has_Trait_Vengeful ]]--

function Has_Trait_Vengeful1_impl (context)
		return context:character():has_trait("Vengeful")
end 

events.CharacterTurnStart[#events.CharacterTurnStart+1] =
function (context)
	if Has_Trait_Vengeful1_impl(context) then
		if conditions.DateInRange(-10000, 10000, context) then 
			effect.ancillary("Torturer", 1,  context)
			effect.ancillary("Disfigured_Veteran", 1,  context)
		end
		return true
	end
	return false
end