-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Tables and Lists
world_effects = {
	[0] = -1,	--spring
	[1] = 0,	--summer
	[2] = 1,	--autumn
	[3] = -1,	--winter
	["in_own_region"] = 1,
	["in_enemy_region"] = 0,
	["at_sea"] = 0
}

army_stance_effects = {
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_AMBUSH"] = -2,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_ASSEMBLE_FLEET"] = "refill",
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_DEFAULT"] = -2,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_DOUBLE_TIME"] = -2,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_LAND_RAID"] = -1,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_MARCH"] = -2,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_MUSTER"] = "refill",
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_PATROL"] = -2,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_SEA_RAID"] = -1,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_SETTLE"] = -2,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_SET_CAMP"] = 0,
	["MILITARY_FORCE_SITUATIONAL_STANCE_BLOCKADE"] = 1,
	["MILITARY_FORCE_SITUATIONAL_STANCE_DOCK"] = 4,
	["MILITARY_FORCE_SITUATIONAL_STANCE_GARRISON"] = 4,
	["MILITARY_FORCE_SITUATIONAL_STANCE_LAY_SIEGE"] =  -2,
	["MILITARY_FORCE_SITUATIONAL_STANCE_UNDER_SIEGE"] =  0
}

navy_stance_effects = {
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_AMBUSH"] = -4,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_ASSEMBLE_FLEET"] = "refill",
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_DEFAULT"] = -4,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_DOUBLE_TIME"] = -4,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_LAND_RAID"] = -3,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_MARCH"] = -4,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_MUSTER"] = "refill",
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_PATROL"] = -4,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_SEA_RAID"] = -3,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_SETTLE"] = -4,
	["MILITARY_FORCE_ACTIVE_STANCE_TYPE_SET_CAMP"] = -2,
	["MILITARY_FORCE_SITUATIONAL_STANCE_BLOCKADE"] = -1,
	["MILITARY_FORCE_SITUATIONAL_STANCE_DOCK"] = 6,
	["MILITARY_FORCE_SITUATIONAL_STANCE_GARRISON"] = 6,
	["MILITARY_FORCE_SITUATIONAL_STANCE_LAY_SIEGE"] =  0,
	["MILITARY_FORCE_SITUATIONAL_STANCE_UNDER_SIEGE"] =  0
}

battle_effects = {
	["heroic_victory"] = 1,
	["decisive_victory"] = 2,
	["close_victory"] = 0,
	["pyrrhic_victory"] = 0,
	["valiant_defeat"] = 0,
	["close_defeat"] = -1,
	["decisive_defeat"] = -2,
	["crushing_defeat"] = -3
}

event_effects = {
	["CharacterPerformsOccupationDecisionSack"] = 8,
	["CharacterPerformsOccupationDecisionRaze"] = 8,
	["CharacterPerformsOccupationDecisionLoot"] = "refill",
	["CharacterPerformsOccupationDecisionOccupy"] = "refill",
	["CharacterPerformsOccupationDecisionResettle"] = "refill",
	["CharacterEmbarksNavy"] = 2
}