--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- lib_manpower_static_entities.lua
-- Defines data for population classes and cultures

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

local GROWTH_RATE = 1.25
local STATIC_GROWTH = 30 -- Static growth added to manpower so it can still grow back up if it hits 0

cultures = {
    roman = {
        displayName = "Roman",
        
        classes = {
            rich = {
                displayName = "Patricians",
                defaultPopulation = 250,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH -- Static growth added to manpower so it can still grow back up if it hits 0
            },
            
            med = {
                displayName = "Plebeians",
                defaultPopulation = 700,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            },
            
            poor = {
                displayName = "Prolitarii",
                defaultPopulation = 2000,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            },
            
            slave = {
                displayName = "Slaves",
                defaultPopulation = 1250,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            }
        }
    },

    barbarian = {
        displayName = "Barbarian",
        
        classes = {
            rich = {
                displayName = "Nobles",
                defaultPopulation = 250,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            },
            
            med = {
                displayName = "Warriors",
                defaultPopulation = 700,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            },
            
            poor = {
                displayName = "Commoners",
                defaultPopulation = 2000,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            }
        }
    },

    other = {
        displayName = "Other",
        
        classes = {
            rich = {
                displayName = "Royals",
                defaultPopulation = 250,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            },
            
            med = {
                displayName = "Citizens",
                defaultPopulation = 700,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            },
            
            poor = {
                displayName = "Peasants",
                defaultPopulation = 2000,
                growthRate = GROWTH_RATE,
                staticGrowth = STATIC_GROWTH
            }
        }
    }
}