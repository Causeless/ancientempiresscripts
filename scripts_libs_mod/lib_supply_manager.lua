-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Tables and Lists
supply_effects_army = {
	"bundle_army_scarce_supplies_3",
	"bundle_army_scarce_supplies_3",
	"bundle_army_scarce_supplies_3",
	"bundle_army_scarce_supplies_3",
	"bundle_army_scarce_supplies_2",
	"bundle_army_scarce_supplies_2",
	"bundle_army_scarce_supplies_2",
	"bundle_army_scarce_supplies_2",
	"bundle_army_scarce_supplies_1",
	"bundle_army_scarce_supplies_1",
	"bundle_army_scarce_supplies_1",
	"bundle_army_scarce_supplies_1",
	"bundle_army_well_supplied",
	"bundle_army_well_supplied",
	"bundle_army_well_supplied",
	"bundle_army_well_supplied",
	"bundle_army_fully_supplied",
	"bundle_army_fully_supplied"
}

supply_effects_navy = {
	"bundle_navy_scarce_supplies_3",
	"bundle_navy_scarce_supplies_3",
	"bundle_navy_scarce_supplies_3",
	"bundle_navy_scarce_supplies_3",
	"bundle_navy_scarce_supplies_2",
	"bundle_navy_scarce_supplies_2",
	"bundle_navy_scarce_supplies_2",
	"bundle_navy_scarce_supplies_2",
	"bundle_navy_scarce_supplies_1",
	"bundle_navy_scarce_supplies_1",
	"bundle_navy_scarce_supplies_1",
	"bundle_navy_scarce_supplies_1",
	"bundle_navy_well_supplied",
	"bundle_navy_well_supplied",
	"bundle_navy_well_supplied",
	"bundle_navy_well_supplied",
	"bundle_navy_fully_supplied",
	"bundle_navy_fully_supplied"
}